﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisiblityTest : MonoBehaviour {

    public Renderer PlayerRenderer;
    public Shader TransparencyShader;
    Shader m_originalShader = null;
    Color m_originalColor = Color.white;
    Texture m_origTexture = null;
    public Texture BlackTexture;
    public Color TransparencyColor;

    public float AlphaValue = 0.1f;
    bool m_transparent = false;


    public int RingCount = 2;
    public int RingVertices = 5;
    public float RingSpacing = 0.4f;

	// Use this for initialization
	void Start () {
        m_originalShader = PlayerRenderer.material.shader;
        m_originalColor = PlayerRenderer.material.GetColor("_Color");
        m_origTexture = PlayerRenderer.material.GetTexture("_MainTex");

        //renderer.material.shader = shad;
        UpdateTransparency();
    }
	
    void UpdateTransparency()
    {
        PlayerRenderer.material.shader = m_transparent ? TransparencyShader : m_originalShader;
        PlayerRenderer.material.shader = m_transparent ? TransparencyShader : m_originalShader;
        PlayerRenderer.material.SetColor("_Color", m_transparent ?
            new Color(TransparencyColor.r, TransparencyColor.g, TransparencyColor.b, AlphaValue) :
            m_originalColor);
        PlayerRenderer.material.SetTexture("_MainTex", m_transparent ? Texture2D.whiteTexture : m_origTexture);
    }
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Q))
        {
            m_transparent = !m_transparent;
            UpdateTransparency();
        }

        /*float spacing = 0;

        float angleSpacing = 360f / (float)RingVertices;
        for (int r = 0; r < RingCount; r++)
        {
            spacing += RingSpacing;
            float curAngle = 0f;
            for (int v = 0; v < RingVertices; v++)
            {
                Vector3 angle = Vector3.up;
                angle = Quaternion.AngleAxis(curAngle, Vector3.forward) * angle;
                Vector3 startPos = Camera.main.transform.position + angle * spacing;
                Debug.DrawLine(startPos, startPos + Camera.main.transform.forward * 5f);
                curAngle += angleSpacing;
            }
        }*/

            //elapsed += Time.deltaTime;
            //Color curCol = renderer.material.GetColor("_Color");
            //renderer.material.SetColor("_Color", new Color(curCol.r, curCol.g, curCol.b, fade));
            //    renderer.material.color.r,
            //    renderer.material.color.g,
            //   renderer.material.color.b,
            //    fade);
    }
}
