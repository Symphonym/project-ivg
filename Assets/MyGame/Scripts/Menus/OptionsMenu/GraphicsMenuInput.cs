﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GraphicsMenuInput : MonoBehaviour {

    public Toggle FullscreenToggle;
    public Text ResolutionText;
    public Dropdown TextureResolutionDropdown;
    public Dropdown ShadowResolutionDropdown;

    int m_selectedResolution = 0;

    OptionsControl.Options m_tentativeOptions;
    Resolution[] m_resolutions;

    void OnEnable()
    {
        ResetTentative();
        InitializeMenu();
    }
    void InitializeMenu()
    {
        m_resolutions = Screen.resolutions;

        // Select current resolution
        Resolution currentRes = new Resolution();
        currentRes.width = Screen.fullScreen ? OptionsControl.options.resWidth : Screen.width;
        currentRes.height = Screen.fullScreen ? OptionsControl.options.resHeight : Screen.height;

        for (int i = 0; i < m_resolutions.Length; i++)
        {
            if (m_resolutions[i].width == currentRes.width && m_resolutions[i].height == currentRes.height)
            {
                m_selectedResolution = i;
                break;
            }
        }

        // Load fullscreen option
        FullscreenToggle.isOn = OptionsControl.options.fullscreen;

        // Load texture resolution option
        TextureResolutionDropdown.value = OptionsControl.options.textureResolution;
        TextureResolutionDropdown.RefreshShownValue();

        // Load shadow resolution option
        ShadowResolution shadowRes = OptionsControl.options.shadowResolution;
        if (shadowRes == ShadowResolution.VeryHigh)
            ShadowResolutionDropdown.value = 3;
        else if (shadowRes == ShadowResolution.High)
            ShadowResolutionDropdown.value = 2;
        else if (shadowRes == ShadowResolution.Medium)
            ShadowResolutionDropdown.value = 1;
        else if (shadowRes == ShadowResolution.Low)
            ShadowResolutionDropdown.value = 0;
        ShadowResolutionDropdown.RefreshShownValue();

        UpdateSelectedResolution();
    }

    void ResetTentative()
    {
        m_tentativeOptions = new OptionsControl.Options();
    }
    void UpdateSelectedResolution()
    {
        m_tentativeOptions.resWidth = m_resolutions[m_selectedResolution].width;
        m_tentativeOptions.resHeight = m_resolutions[m_selectedResolution].height;
        ResolutionText.text = Screen.resolutions[m_selectedResolution].width + "x" + Screen.resolutions[m_selectedResolution].height;
    }
	
    public void OnChangeResolutionLeftPresseed()
    {
        --m_selectedResolution;
        if (m_selectedResolution < 0)
            m_selectedResolution = m_resolutions.Length - 1;
        else if (m_selectedResolution >= m_resolutions.Length)
            m_selectedResolution = 0;
        UpdateSelectedResolution();
    }
    public void OnChangeResolutionRightPresseed()
    {
        ++m_selectedResolution;
        if (m_selectedResolution < 0)
            m_selectedResolution = m_resolutions.Length - 1;
        else if (m_selectedResolution >= m_resolutions.Length)
            m_selectedResolution = 0;

        UpdateSelectedResolution();
    }
    public void OnFullscreenChanged(bool fullscreen)
    {
        m_tentativeOptions.fullscreen = fullscreen;
    }
    public void OnTextureResolutionChanged(int textureResolution)
    {
        m_tentativeOptions.textureResolution = textureResolution;
    }
    public void OnShadowResolutionChanged(int shadowResolution)
    {
        if (ShadowResolutionDropdown.value == 0)
            m_tentativeOptions.shadowResolution = ShadowResolution.Low;
        else if (ShadowResolutionDropdown.value == 1)
            m_tentativeOptions.shadowResolution = ShadowResolution.Medium;
        else if (ShadowResolutionDropdown.value == 2)
            m_tentativeOptions.shadowResolution = ShadowResolution.High;
        else if (ShadowResolutionDropdown.value == 3)
            m_tentativeOptions.shadowResolution = ShadowResolution.VeryHigh;
    }

    public void OnApplyPressed()
    {
        OptionsControl.options.resWidth = m_tentativeOptions.resWidth;
        OptionsControl.options.resHeight = m_tentativeOptions.resHeight;
        OptionsControl.options.fullscreen = m_tentativeOptions.fullscreen;
        OptionsControl.options.textureResolution = m_tentativeOptions.textureResolution;
        OptionsControl.options.shadowResolution = m_tentativeOptions.shadowResolution;

        OptionsControl.SaveOptions();
        OptionsControl.ApplyOptions();
    }
}
