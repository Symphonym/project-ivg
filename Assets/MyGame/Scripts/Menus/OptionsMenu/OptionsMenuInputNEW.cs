﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionsMenuInputNEW : MonoBehaviour {

    [SerializeField]
    Toggle m_xpGainToggle;

    [SerializeField]
    List<Button> m_tabButtons = new List<Button>();
    ColorBlock colors;

    public void HighlightTab(Button tabButton)
    {
        colors = tabButton.colors;

        foreach (Button b in m_tabButtons)
        {
            colors.normalColor = b == tabButton ? Color.white : (Color)(new Color32(120, 120, 120, 255));
            b.colors = colors;
        }
    }

    public void OnOpenMenu()
    {
        // Select the first options menu
        if(m_tabButtons.Count > 0)
            m_tabButtons[0].onClick.Invoke();

        m_xpGainToggle.isOn = OptionsControl.options.xpGain;
    }

    public void OnToggleXPGain(bool xpGain)
    {
        OptionsControl.options.xpGain = xpGain;

        OptionsControl.SaveOptions();
        OptionsControl.ApplyOptions();
    }
}
