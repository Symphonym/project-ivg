﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public static class OptionsControl  {

    [System.Serializable]
	public class Options
    {
        public int resWidth;
        public int resHeight;
        public bool fullscreen;

        public int textureResolution;

        public ShadowResolution shadowResolution;

        public float mouseSensitivity;

        public float masterVolume;

        public bool xpGain;


        public void InitializeDefaults()
        {
            resWidth = Screen.width;
            resHeight = Screen.height;
            fullscreen = false;
            textureResolution = QualitySettings.masterTextureLimit;
            shadowResolution = QualitySettings.shadowResolution;
            mouseSensitivity = 1f;
            masterVolume = 0.5f;
            xpGain = true;
        }
    }

    static Options m_options = new Options();
    public static Options options
    {
        get
        {
            return m_options;
        }
    }
    const string optionsSaveFile = "options.json";

    public static void SaveOptions()
    {
        string saveFilePath = Path.Combine(Application.persistentDataPath, optionsSaveFile);
        File.WriteAllText(saveFilePath, JsonUtility.ToJson(m_options, true));
    }
    public static void LoadOptions()
    {
        m_options = new Options();
        m_options.InitializeDefaults();

        string saveFilePath = Path.Combine(Application.persistentDataPath, optionsSaveFile);
        if(File.Exists(saveFilePath))
        {
            string savedJson = File.ReadAllText(saveFilePath);
            JsonUtility.FromJsonOverwrite(savedJson, m_options);
        }
        ApplyOptions();
    }
    public static void ApplyOptions()
    {
        Screen.SetResolution(
            m_options.resWidth, 
            m_options.resHeight,
            m_options.fullscreen);

        QualitySettings.masterTextureLimit = m_options.textureResolution;
        QualitySettings.shadowResolution = m_options.shadowResolution;

        AudioListener.volume = m_options.masterVolume;
    }
}
