﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioMenuInput : MonoBehaviour {

    public Slider MasterVolumeSlider;
    public Text MasterVolumeText;

	void OnEnable()
    {
        MasterVolumeSlider.value = OptionsControl.options.masterVolume;
        UpdateMasterVolumeText();
    }

    void UpdateMasterVolumeText()
    {
        MasterVolumeText.text = "Master volume (" + Mathf.RoundToInt(MasterVolumeSlider.value * 100f) + "%)";
    }
    public void OnMasterVolumeChanged()
    {
        UpdateMasterVolumeText();
    }

    public void FinishChangingVolume()
    {
        OptionsControl.options.masterVolume = MasterVolumeSlider.value;

        OptionsControl.SaveOptions();
        OptionsControl.ApplyOptions();
    }
}
