﻿using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;
using System;
using System.Xml;
using System.Xml.Schema;

public static class KeybindControl  {

    [System.Serializable]
    public class Keybind
    {
        [SerializeField]
        string name;
        [SerializeField]
        KeyCode key;

        [SerializeField]
        bool isMouseBind = false;
        [SerializeField]
        int mouseButton = 0;


        public Keybind()
        {
            name = string.Empty;
            key = KeyCode.W;
            mouseButton = 0;
            isMouseBind = false;
        }
        public Keybind(string name, KeyCode key)
        {
            this.name = name;
            this.key = key;
            isMouseBind = false;
        }
        public Keybind(string name, int mouseButton)
        {
            this.name = name;
            this.mouseButton = mouseButton;
            isMouseBind = true;
        }


        public string AsString()
        {
            if (isMouseBind)
            {
                switch (mouseButton)
                {
                    default:
                    case 0: return "Left Click";

                    case 1: return "Right Click";
                    case 2: return "Middle Click";
                }
            }
            else
                return key.ToString();
        }

        public bool SameBindAs(Keybind other)
        {
            bool sameType = isMouseBind == other.isMouseBind;
            if(isMouseBind)
                return mouseButton == other.mouseButton && sameType;
            else
                return key == other.key && sameType;
        }
        public bool GetDown()
        {
            if (isMouseBind)
                return Input.GetMouseButtonDown(mouseButton);
            else
                return Input.GetKeyDown(key);
        }
        public bool GetUp()
        {
            if (isMouseBind)
                return Input.GetMouseButtonUp(mouseButton);
            else
                return Input.GetKeyUp(key);
        }
        public bool Get()
        {
            if (isMouseBind)
                return Input.GetMouseButton(mouseButton);
            else
                return Input.GetKey(key);
        }
        public string GetName()
        {
            return name;
        }
        public void CopyFrom(Keybind other)
        {
            name = other.name;
            key = other.key;

            isMouseBind = other.isMouseBind;
            mouseButton = other.mouseButton;
        }
    }

    [System.Serializable]
    class KeybindStorage
    {
        public List<Keybind> storage;

        public KeybindStorage(Dictionary<string, Keybind> keybinds)
        {
            storage = new List<Keybind>(keybinds.Values);
        }

        public Dictionary<string, Keybind> GetDictionary()
        {
            Dictionary<string, Keybind> keybinds = new Dictionary<string, Keybind>(storage.Count);
            foreach (Keybind keybind in storage)
                keybinds.Add(keybind.GetName(), keybind);

            return keybinds;
        }
    }

    static Dictionary<string, Keybind> m_keybinds = new Dictionary<string, Keybind>();
    private const string keyBindSaveFile = "keybindings.json";

    // Returns true if there are other keybinds with the same configuration as 'keybind'
	public static bool SetKeybind(Keybind keybind)
    {
        // If a keybind exists at this name, just update it's internal data so any
        // references to it are updated as well
        Keybind curBind;
        if (m_keybinds.TryGetValue(keybind.GetName(), out curBind))
            curBind.CopyFrom(keybind);
        else
        {
            m_keybinds[keybind.GetName()] = keybind;
            curBind = keybind;
        }

        foreach (KeyValuePair<string, Keybind> p in m_keybinds)
        {
            // Ignore self
            if (p.Value.Equals(curBind))
                continue;

            // If there is a keybind 'p' that has the same keybinding, and is not unbinded, then unbind it due to conflicting keybinds
            if(curBind.SameBindAs(p.Value))
                return true;
        }
        return false;
    }

    public static Keybind GetKeybind(string name)
    {
        return m_keybinds[name];
    }

    public static void SaveKeybinds()
    {
        string saveFilePath = Path.Combine(Application.persistentDataPath, keyBindSaveFile);

        KeybindStorage storage = new KeybindStorage(m_keybinds);
        File.WriteAllText(saveFilePath, JsonUtility.ToJson(storage, true));
    }
    public static void LoadKeybinds()
    {
        // Load default keybinds
        SetKeybind(new KeybindControl.Keybind("Move Forward", KeyCode.W));
        SetKeybind(new KeybindControl.Keybind("Move Backward", KeyCode.S));
        SetKeybind(new KeybindControl.Keybind("Move Right", KeyCode.D));
        SetKeybind(new KeybindControl.Keybind("Move Left", KeyCode.A));
        SetKeybind(new KeybindControl.Keybind("Jump", KeyCode.Space));
        SetKeybind(new KeybindControl.Keybind("Crouch", KeyCode.LeftControl));
        SetKeybind(new KeybindControl.Keybind("Pickup Item", 0));
        SetKeybind(new KeybindControl.Keybind("Drop Item", 0));
        SetKeybind(new KeybindControl.Keybind("Throw Item", 1));
        SetKeybind(new KeybindControl.Keybind("Shoot Gun", 0));
        SetKeybind(new KeybindControl.Keybind("Flashlight", KeyCode.F));
        SetKeybind(new KeybindControl.Keybind("Interact", KeyCode.E));
        SetKeybind(new KeybindControl.Keybind("In-Game Ping", KeyCode.G));
        SetKeybind(new KeybindControl.Keybind("In-Game Position Ping", KeyCode.T));
        SetKeybind(new KeybindControl.Keybind("Camera Down", KeyCode.LeftControl));
        SetKeybind(new KeybindControl.Keybind("Camera Sprint", KeyCode.LeftShift));
        SetKeybind(new KeybindControl.Keybind("Punch", 1));
        SetKeybind(new KeybindControl.Keybind("Taunt", KeyCode.H));

        string saveFilePath = Path.Combine(Application.persistentDataPath, keyBindSaveFile);
        if (File.Exists(saveFilePath))
        {
            KeybindStorage storage = new KeybindStorage(m_keybinds);

            string savedJson = File.ReadAllText(saveFilePath);
            JsonUtility.FromJsonOverwrite(savedJson, storage);
            m_keybinds = storage.GetDictionary();
        }
    }

    public static Dictionary<string, Keybind> GetKeybinds()
    {
        return m_keybinds;
    }
}
