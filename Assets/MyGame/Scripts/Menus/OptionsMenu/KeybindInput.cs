﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeybindInput : MonoBehaviour {

    public Text ButtonText;
    public Button KeybindButton;
    public Text KeybindText;

    bool m_bindingKey = false;

    void Start()
    {
        enabled = false;
    }

    void OnGUI()
    {
        Event curEvent = Event.current;

        // Listen for keybind input
        if(m_bindingKey && (curEvent.type == EventType.MouseDown || curEvent.type == EventType.KeyDown))
        {
            if(curEvent.isMouse)
            {
                if (KeybindControl.SetKeybind(new KeybindControl.Keybind(KeybindText.text, curEvent.button)))
                    MessagePopupControl.instance.ShowPopup("Please be aware there is at least one other keybind with this same key configuration", "Warning");
                KeybindControl.SaveKeybinds();

                ButtonText.text = KeybindControl.GetKeybind(KeybindText.text).AsString();

                SetBindable(false);
            }
            else if(curEvent.isKey)
            {
                // Escape cancels the keybind
                if(curEvent.keyCode == KeyCode.Escape)
                {
                    SetBindable(false);
                    return;
                }

                if (KeybindControl.SetKeybind(new KeybindControl.Keybind(KeybindText.text, curEvent.keyCode)))
                    MessagePopupControl.instance.ShowPopup("Please be aware there is at least one other keybind with this same key configuration", "Warning");
                KeybindControl.SaveKeybinds();

                ButtonText.text = KeybindControl.GetKeybind(KeybindText.text).AsString();
                SetBindable(false);
            }
        }
    }

    void SetBindable(bool bindable)
    {
        m_bindingKey = bindable;
        enabled = bindable;
        KeybindButton.interactable = !bindable;

        ButtonText.text = bindable ? "(Waiting for input)" : KeybindControl.GetKeybind(KeybindText.text).AsString();

    }

    public void OnBindKey()
    {
        if(!m_bindingKey)
            SetBindable(true);
    }
}
