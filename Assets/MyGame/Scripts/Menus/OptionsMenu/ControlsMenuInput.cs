﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlsMenuInput : MonoBehaviour {

    public Transform KeybindContent;
    public KeybindInput KeybindPrefab;

    public Slider MouseSenseSlider;
    public InputField MouseSenseInputField;

	// Use this for initialization
	void Start () {
        foreach (KeyValuePair<string, KeybindControl.Keybind> p in KeybindControl.GetKeybinds())
        {
            KeybindInput keybindObj = Instantiate(KeybindPrefab, KeybindContent.transform, false);
            keybindObj.KeybindText.text = p.Key;
            keybindObj.ButtonText.text = p.Value.AsString();
        }
    }

    void OnEnable()
    {
        MouseSenseSlider.value = OptionsControl.options.mouseSensitivity;
        UpdateMouseSensitivityText();
    }
	
    public void FinishChangingMouseSensitivity()
    {
        OptionsControl.options.mouseSensitivity = MouseSenseSlider.value;

        OptionsControl.SaveOptions();
        OptionsControl.ApplyOptions();
    }
    void UpdateMouseSensitivityText()
    {
        MouseSenseInputField.text = System.Math.Round(MouseSenseSlider.value, 2).ToString();
    }
    public void OnMouseSensitivityTextChanged()
    {
        float mouseSense;
        if (float.TryParse(MouseSenseInputField.text, out mouseSense))
        {
            MouseSenseSlider.value = mouseSense;
            FinishChangingMouseSensitivity();
        }
        UpdateMouseSensitivityText();
    }
	public void OnMouseSensitivityChanged()
    {
        UpdateMouseSensitivityText();
    }
}
