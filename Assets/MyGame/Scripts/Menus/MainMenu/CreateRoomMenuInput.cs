﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateRoomMenuInput : MonoBehaviour {

    public InputField RoomNameInputField;
    public Dropdown StartingMapDropdown;
    public Slider MaxPlayersSlider;
    public Text MaxPlayersText;
    public Toggle PublicRoomToggle;
    public Image LevelImage;

    bool m_createdRoom = false;

    [SerializeField]
    MenuSwitcher m_menuSwitcher;

    // Use this for initialization
    void Start () {


        StartingMapDropdown.ClearOptions();
        foreach (Levels.LevelData level in Levels.instance.All)
            StartingMapDropdown.options.Add(new Dropdown.OptionData(level.Name));
        StartingMapDropdown.RefreshShownValue();

        // Update preview image
        OnLevelChanged();

	}

    void OnEnable()
    {
        m_createdRoom = false;
    }
	
    public void OnLevelChanged()
    {
        int levelIndex = Levels.instance.GetLevelIndex(StartingMapDropdown.options[StartingMapDropdown.value].text);
        if (levelIndex < 0)
            return;

        LevelImage.sprite = Levels.instance.All[levelIndex].PreviewSprite;
    }
    public void OnMaxPlayersChanged()
    {
        MaxPlayersText.text = "Max players (" + MaxPlayersSlider.value + ")";
    }
    public void OnPublicRoomChanged(bool publicRoom)
    {
        RoomNameInputField.interactable = publicRoom;

        RoomNameInputField.text = publicRoom ? "" : "~Private Room Name~";
    }
    public void OnCreateRoomPressed()
    {
        if (PhotonNetwork.inRoom || PhotonNetwork.connectionStateDetailed == ClientState.Joining || m_createdRoom)
            return;

        if (!PhotonNetwork.connectedAndReady)
        {
            MessagePopupControl.instance.ShowPopup("Connection to region not yet established", "Error");
            return;
        }
        else if(string.IsNullOrEmpty(RoomNameInputField.text))
        {
            MessagePopupControl.instance.ShowPopup("Please enter a room name", "Error");
            return;
        }

        int levelIndex = Levels.instance.GetLevelIndex(StartingMapDropdown.options[StartingMapDropdown.value].text);
        if (levelIndex < 0)
            return;

        // Hardcoded max player limit, if someone fiddles with the memory of the slider
        if (MaxPlayersSlider.value > 6)
            MaxPlayersSlider.value = 6;

        m_createdRoom = true;

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsVisible = PublicRoomToggle.isOn;
        roomOptions.MaxPlayers = (byte)MaxPlayersSlider.value;

        // C0 - (int) Level index, C1 - (int) Whether or not game is at lobby menu 
        roomOptions.CustomRoomProperties = new ExitGames.Client.Photon.Hashtable() {
            { "C0", levelIndex },
            { "C1", 1 },
            { ConstantVars.RoomGameState_Key, (int)GameControl.GameStates.Lobby },
            { ConstantVars.RoomSetting_ItemsRequired_Key, 0.8f },
            { ConstantVars.RoomSetting_ItemDensity_Key, (int)GameControl.ItemDensities.High },
            { ConstantVars.RoomSetting_GameDuration_Key, 300 },
            { ConstantVars.RoomSetting_XPGain_Key, false }
        };
        roomOptions.CustomRoomPropertiesForLobby = new string[] { "C0", "C1" }; // Make props available for lobby
        TypedLobby sqlLobby = new TypedLobby(ConstantVars.MatchMaking_SqlLobby_Name, LobbyType.SqlLobby);

        m_menuSwitcher.ChangeMenu("Connection_Menu");
        PhotonNetwork.CreateRoom(PublicRoomToggle.isOn ? RoomNameInputField.text : null, roomOptions, sqlLobby);
    }
}
