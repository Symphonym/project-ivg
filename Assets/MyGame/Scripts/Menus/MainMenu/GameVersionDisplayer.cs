﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameVersionDisplayer : MonoBehaviour {

    [SerializeField]
    Text m_versionText;

	void Start ()
    {
        m_versionText.text = "Version " + ConstantVars.GameVersion.ToString();
	}
}
