﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class ConnectionMenuInput : MonoBehaviour
{
    [SerializeField]
    MenuSwitcher m_menuSwitcher;

    private void OnEnable()
    {
        MessagePopupControl.instance.ForceBlocker(true);
    }

    private void OnDisable()
    {
        MessagePopupControl.instance.ForceBlocker(false);
    }

    void OnJoinedRoom()
    {
        // Player set as spectator when first joining a room, and ready status is reset
        Utility.InitializePhotonPlayerProps(PhotonNetwork.player);

        // When a room is joined, retrieve its level and load into the level
        int levelIndex = Utility.GetPhotonRoomProp<int>("C0", 0);
        ConsistentSceneLoader.instance.LoadScene(Levels.instance.All[levelIndex].SceneName);
    }

    void HandleErrorMessage(short errorCode)
    {
        switch (errorCode)
        {
            default:
                MessagePopupControl.instance.ShowPopup("An error occurred while connecting. Please try again.", "Error");
                break;
            case ErrorCode.GameClosed:
            case ErrorCode.GameDoesNotExist:
                MessagePopupControl.instance.ShowPopup("Room does not exist!", "Error");
                break;
            case ErrorCode.GameFull:
                MessagePopupControl.instance.ShowPopup("The room is full!", "Error");
                break;
            case ErrorCode.GameIdAlreadyExists:
                MessagePopupControl.instance.ShowPopup("That room name is already taken!", "Error");
                break;
            case ErrorCode.NoRandomMatchFound:
                MessagePopupControl.instance.ShowPopup("A room matching your filters could not be found!", "Error");
                break;
            case ErrorCode.Ok: break;
        }
    }

    void OnPhotonCreateRoomFailed(object[] codeAndMsg)
    {
        HandleErrorMessage((short)codeAndMsg[0]);
        m_menuSwitcher.GoBack();
    }

    void OnPhotonJoinRoomFailed(object[] codeAndMsg)
    {
        HandleErrorMessage((short)codeAndMsg[0]);
        m_menuSwitcher.GoBack();
    }
    void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        HandleErrorMessage((short)codeAndMsg[0]);
        m_menuSwitcher.GoBack();
    }

    void OnDisconnectedFromPhoton()
    {
        MessagePopupControl.instance.ShowPopup("Disconnected from region.", "Error");
        m_menuSwitcher.GoBack();
    }

}
