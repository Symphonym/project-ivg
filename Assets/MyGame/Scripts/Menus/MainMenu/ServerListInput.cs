﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerListInput : MonoBehaviour {

    public ServerListItem ServerListItemPrefab;

    public GameObject ServerListContent;
    public Text ServerListPageText;
    public Text ServerListPageSubText;

    public Dropdown RegionDropDown;
    public Text RegionText;

    public List<ServerListItem> CurrentPage = new List<ServerListItem>();

    class CachedRoomInfo
    {
        public string RoomText;
        public string MapText;
        public string InLobbyText;
        public string PlayerText;
    }

    List<CachedRoomInfo> m_serverList = new List<CachedRoomInfo>();
    int m_pageIndex = 0;

    bool m_awaitingInitialRoomUpdate = true;

    enum SortingModes
    {
        Name,
        Map,
        Lobby,
        Player
    };

    SortingModes m_sortingMode = SortingModes.Name;
    bool m_inverseSorting = false;

    ServerListItem m_selectedServer = null;

    bool m_joinedServer = false;
    [SerializeField]
    MenuSwitcher m_menuSwitcher;

    void Awake()
    {
        // Make sure to load filters once so we don't start with an empty server list
        ServerFilterInput.InitializeFilters();

        
        foreach (ServerListItem listItem in CurrentPage)
        {
            listItem.ItemButton.onClick.AddListener(() =>
            {
                if (m_selectedServer != null)
                    m_selectedServer.SetSelected(false);
                listItem.SetSelected(true);
                m_selectedServer = listItem;

                if (listItem.CheckDoubleClick())
                    OnJoinPressed();
            });
        }
    }

    void OnEnable()
    {
        // Update server selection
        List<string> regions = new List<string>(PhotonConnectionControl.instance.SortedRegionList);
        PhotonConnectionControl.instance.SetIgnoreCloudChanges(true); // Prevent dropdown callback from triggering a reconnect
        RegionDropDown.ClearOptions();
        RegionDropDown.AddOptions(regions);
        for (int i = 0; i < regions.Count; i++)
        {
            if (regions[i].Equals(Utility.PhotonRegionToString(PhotonConnectionControl.instance.GetCurrentCloudRegion())))
            {
                RegionDropDown.value = i;
                break;
            }
        }
        PhotonConnectionControl.instance.SetIgnoreCloudChanges(false);

        m_joinedServer = false;
        UpdateServerList();
    }
    void FixedUpdate()
    {
        UpdateRegionText();
    }

    public void OnChangePageLeft()
    {
        ChangeServerListPage(m_pageIndex - 1);
    }
    public void OnChangePageRight()
    {
        ChangeServerListPage(m_pageIndex + 1);
    }

    public void OnRefreshPressed()
    {
        UpdateServerList();
    }


    public void OnRoomHeaderPressed()
    {
        if (m_sortingMode == SortingModes.Name)
            m_inverseSorting = !m_inverseSorting;

        m_sortingMode = SortingModes.Name;
        UpdateServerList();
    }
    public void OnMapHeaderPressed()
    {
        if (m_sortingMode == SortingModes.Map)
            m_inverseSorting = !m_inverseSorting;

        m_sortingMode = SortingModes.Map;
        UpdateServerList();
    }
    public void OnLobbyHeaderPressed()
    {
        if (m_sortingMode == SortingModes.Lobby)
            m_inverseSorting = !m_inverseSorting;

        m_sortingMode = SortingModes.Lobby;
        UpdateServerList();
    }
    public void OnPlayerHeaderPressed()
    {
        if (m_sortingMode == SortingModes.Player)
            m_inverseSorting = !m_inverseSorting;

        m_sortingMode = SortingModes.Player;
        UpdateServerList();
    }

    public void OnRegionChanged()
    {
        CloudRegionCode newRegion = Utility.StringToPhotonRegion(RegionDropDown.options[RegionDropDown.value].text);
        PlayerPrefs.SetInt(ConstantVars.PlayerPref_PreferredRegion_Key, (int)newRegion);

        PhotonConnectionControl.instance.ConnectToPhotonRegion(newRegion);
        //ConnectToPhotonRegion();
    }

    public void OnJoinPressed()
    {
        if (PhotonNetwork.inRoom || PhotonNetwork.connectionStateDetailed == ClientState.Joining || m_joinedServer)
            return;

        if (m_selectedServer == null)
        {
            MessagePopupControl.instance.ShowPopup("Select a server to join first", "Error");
            return;
        }
        if (!PhotonNetwork.connectedAndReady)
        {
            MessagePopupControl.instance.ShowPopup("Connection to region not yet established", "Error");
            return;
        }

        m_joinedServer = true;

        m_menuSwitcher.ChangeMenu("Connection_Menu");
        PhotonNetwork.JoinRoom(m_selectedServer.RoomName.text);
    }
    public void OnQuickJoinPressed()
    {
        if (PhotonNetwork.inRoom || PhotonNetwork.connectionStateDetailed == ClientState.Joining || m_joinedServer)
            return;

        if (!PhotonNetwork.connectedAndReady)
        {
            MessagePopupControl.instance.ShowPopup("Connection to region not yet established", "Error");
            return;
        }

        m_joinedServer = true;

        TypedLobby sqlLobby = new TypedLobby(ConstantVars.MatchMaking_SqlLobby_Name, LobbyType.SqlLobby);

        // Create an SQL filter to filter based on map and whether or not the game is at the lobby or not
        // C0 - (int) Level index
        // C1 - (int) Whether or not game is at lobby menu  (0 = Not in loby, 1 = In lobby)
        // Filter syntax "C0 IN ({levelIndicesFromFilter})" with an optional " AND C1 = 1" to check if in lobby
        int index = 0;
        string sqlLobbyFilter = "C0 IN (";
        foreach (int i in ServerFilterInput.AllowedMaps)
        {
            sqlLobbyFilter += i.ToString();
            if (index < ServerFilterInput.AllowedMaps.Count - 1)
                sqlLobbyFilter += ", ";

            ++index;
        }
        sqlLobbyFilter += ")";
        if (ServerFilterInput.InLobby)
            sqlLobbyFilter = "(" + sqlLobbyFilter + ") AND C1 = 1";

        m_menuSwitcher.ChangeMenu("Connection_Menu");
        PhotonNetwork.JoinRandomRoom(null, 0, MatchmakingMode.FillRoom, sqlLobby, sqlLobbyFilter);
    }

    void ChangeServerListPage(int page)
    {
        m_pageIndex = page;
        int pageCount = Mathf.CeilToInt((float)m_serverList.Count / (float)CurrentPage.Count);
        if (pageCount <= 0)
            pageCount = 1;
        
        // Clamp page index
        if (m_pageIndex < 0)
            m_pageIndex = 0;
        else if (m_pageIndex >= pageCount)
            m_pageIndex = pageCount - 1;

        ServerListPageText.text = (m_pageIndex + 1) + "/" + pageCount;


        // Iterate over a subsection of the serverlist, from firstPageIndex to (lastPageIndex-1)
        int firstPageIndex = m_pageIndex * CurrentPage.Count;
        int lastPageIndex = firstPageIndex + CurrentPage.Count;

        foreach (ServerListItem item in CurrentPage)
            item.gameObject.SetActive(false);

        int index = 0;
        for (int i = firstPageIndex; i < lastPageIndex && i < m_serverList.Count; i++)
        {
            CachedRoomInfo roomInfo = m_serverList[i];
            ServerListItem listItem = CurrentPage[index];
            listItem.RoomName.text = roomInfo.RoomText;
            listItem.MapName.text = roomInfo.MapText;
            listItem.LobbyText.text = roomInfo.InLobbyText;
            listItem.PlayerText.text = roomInfo.PlayerText;
            
            listItem.gameObject.SetActive(true);
            listItem.transform.SetAsLastSibling();
            ++index;
        }

        if(m_selectedServer != null)
        {
            m_selectedServer.SetSelected(false);
            m_selectedServer = null;
        }
    }

    // Apply room filtering
    bool IsRoomAllowed(RoomInfo room)
    {
        bool mapAllowed = false;
        if (room.CustomProperties.ContainsKey("C0"))
        {
            foreach (int allowedLevelIndex in ServerFilterInput.AllowedMaps)
            {
                int levelIndex = Utility.GetPhotonRoomProp<int>(room, "C0", 0);

                // Room map is in the allowed 
                if (allowedLevelIndex == levelIndex)
                {
                    mapAllowed = true;
                    break;
                }
            }
        }
        else
            mapAllowed = true;

        bool roomInLobby = !(Utility.GetPhotonRoomProp<int>(room, "C1", 0) == 0);
        if (!roomInLobby && ServerFilterInput.InLobby)
            return false;

        if (!string.IsNullOrEmpty(ServerFilterInput.RoomNameFilter) &&
            !room.Name.ToLower().Contains(ServerFilterInput.RoomNameFilter.ToLower()))
            return false;

        if (room.PlayerCount >= room.MaxPlayers && !ServerFilterInput.AllowFullRooms)
            return false;

        return mapAllowed;
    }
    void UpdateServerList()
    {
        // Clear server list
        m_serverList.Clear();

        if (!PhotonNetwork.insideLobby)
            return;

        // Reload server list
        RoomInfo[] rooms = PhotonNetwork.GetRoomList();
        
        foreach(RoomInfo room in rooms)
        {
            // Apply filters
            if (!IsRoomAllowed(room))
                continue;

            CachedRoomInfo cache = new CachedRoomInfo();
            cache.RoomText = room.Name;


            if (room.CustomProperties.ContainsKey("C0"))
            {
                int levelIndex = Utility.GetPhotonRoomProp<int>(room, "C0", 0);
                cache.MapText = Levels.instance.All[levelIndex].Name;
            }
            else
                cache.MapText = "???";

            cache.InLobbyText = Utility.GetPhotonRoomProp<int>(room, "C1", 0) == 0 ? "No" : "Yes";
            cache.PlayerText = room.PlayerCount + "/" + room.MaxPlayers;

            m_serverList.Add(cache);
        }

        // Sort list according to sorting mode
        m_serverList.Sort((CachedRoomInfo a, CachedRoomInfo b) =>
        {
            CachedRoomInfo itemA = m_inverseSorting ? b : a;
            CachedRoomInfo itemB = m_inverseSorting ? a : b;

            switch (m_sortingMode)
            {
                default:
                case SortingModes.Name:
                    return itemA.RoomText.CompareTo(itemB.RoomText);
                case SortingModes.Map:
                    return itemA.MapText.CompareTo(itemB.MapText);
                case SortingModes.Lobby:
                    return itemA.InLobbyText.CompareTo(itemB.InLobbyText);
                case SortingModes.Player:
                    return itemA.PlayerText.CompareTo(itemB.PlayerText);
            }
        });

        ServerListPageSubText.text = "(Showing " + m_serverList.Count + "/" + rooms.Length + ")";
        ChangeServerListPage(m_pageIndex);
    }

    void UpdateRegionText()
    {
        if (!PhotonNetwork.connected || !PhotonNetwork.insideLobby)
        {
            string connectionText = "(<color=#FF0000>Not Connected</color>)";
            if (PhotonNetwork.connecting)
                connectionText = "(<color=#FFFF00>Connecting</color>)";
            else if(!PhotonNetwork.insideLobby)
                connectionText = "(<color=#FFFF00>Retreiving data</color>)";
            RegionText.text = "Region " + connectionText;

        }
        else
        {
            int ping = PhotonNetwork.GetPing();
            int roomCount = PhotonNetwork.GetRoomList().Length;
            RegionText.text = "Region (<color=#" + Utility.GetPingColorCode(ping) + ">" + ping + "ms</color>)" +
                ", " + PhotonNetwork.countOfPlayers + " player" + (PhotonNetwork.countOfPlayers == 1 ? ", " : "s, ") + roomCount + " public room" + (roomCount == 1 ? "" : "s"); // Quality programming: 1 public room, 2 public rooms
        }
    }

 
    void OnReceivedRoomListUpdate()
    {
        if(m_awaitingInitialRoomUpdate)
        {
            m_awaitingInitialRoomUpdate = false;
            UpdateServerList();
        }
    }
}
