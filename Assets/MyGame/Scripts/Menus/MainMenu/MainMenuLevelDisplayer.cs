﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Steamworks;

public class MainMenuLevelDisplayer : MonoBehaviour {

    [SerializeField]
    Text m_levelText;

    protected Callback<UserStatsReceived_t> m_steamUserStatsReceived;

    private void Start()
    {
        if(SteamManager.Initialized)
            SteamUserStats.RequestCurrentStats();
    }

    private void OnEnable()
    {
        if (SteamManager.Initialized)
            m_steamUserStatsReceived = Callback<UserStatsReceived_t>.Create(OnSteamUserStatsReceived);
    }
    void OnSteamUserStatsReceived(UserStatsReceived_t pCallback)
    {
        if (pCallback.m_steamIDUser != SteamUser.GetSteamID())
            return;

        if (pCallback.m_eResult != EResult.k_EResultOK)
        {
            // If we failed to retrieve our stats, request them again, we need them to display the level
            SteamUserStats.RequestCurrentStats();
            return;
        }

        float currentXP;

        // Retrieve our XP
        if (SteamUserStats.GetUserStat(pCallback.m_steamIDUser, ConstantVars.SteamStats_XP_Key, out currentXP))
        {
            // Update level text with our level
            m_levelText.text = XPControl.GetColoredLevelString(XPControl.CalculateLevel(currentXP));
        }
    }
}
