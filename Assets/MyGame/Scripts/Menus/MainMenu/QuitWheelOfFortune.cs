﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class QuitWheelOfFortune : MonoBehaviour {

    public float FadeInDuration = 1f;
    public float SpinDurationMin = 2f;
    public float SpinDurationMax = 3f;
    public int SliceCount = 1;
    public bool FirstSliceIsQuit = false;
    public float RotationSpeed = 1000f;

    [SerializeField]
    Image m_wheelImage;

    [SerializeField]
    Text m_quitText;

    [SerializeField]
    Text m_playText;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SpinTheWheel()
    {
        if (gameObject.activeSelf)
            return;

        MessagePopupControl.instance.ForceBlocker(true);
        gameObject.SetActive(true);

        StartCoroutine(CR_SpinTheWheel());
    }

    IEnumerator CR_SpinTheWheel()
    {
        m_playText.color = Color.white;
        m_quitText.color = Color.white;
        m_wheelImage.transform.localRotation = Quaternion.Euler(0, 0, 0);


        float fadeElapsed = -Time.deltaTime;
        AnimationCurve fadeCurve = AnimationCurve.EaseInOut(0f, 0f, FadeInDuration, 1f);

        // Fade in
        do
        {
            fadeElapsed += Time.deltaTime;
            float ratio = fadeCurve.Evaluate(fadeElapsed);
            gameObject.transform.localScale = Vector3.Lerp(new Vector3(0.01f, 0.01f, 0.01f), new Vector3(1f, 1f, 1f), ratio);

            yield return null;
        } while (fadeElapsed < FadeInDuration);

        yield return new WaitForSeconds(0.5f);



        float spinElapsed = -Time.deltaTime;
        float spinDuration = Random.Range(SpinDurationMin, SpinDurationMax);
        AnimationCurve spinCurve = AnimationCurve.EaseInOut(0f, 0f, spinDuration, 1f);

        float rotSpeed = RotationSpeed;

        do
        {
            spinElapsed += Time.deltaTime;
            float ratio = spinCurve.Evaluate(spinElapsed);
            rotSpeed = Mathf.Lerp(RotationSpeed, 0, ratio);

            m_wheelImage.transform.Rotate(0, 0, rotSpeed * Time.deltaTime);

            yield return null;
        } while (spinElapsed < spinDuration);


        float angleIncrements = 360f / (float)SliceCount;

        bool isOnStartingSlice = Mathf.FloorToInt(m_wheelImage.transform.localEulerAngles.z / angleIncrements) % 2 == 0;

        bool quitWins = false;

        if (FirstSliceIsQuit)
            quitWins = isOnStartingSlice;
        else
            quitWins = !isOnStartingSlice;

        if (quitWins)
            m_quitText.color = Color.green;
        else
            m_playText.color = Color.green;

        yield return new WaitForSeconds(1f);

        // Fade out
        fadeElapsed = -Time.deltaTime;
        do
        {
            fadeElapsed += Time.deltaTime;
            float ratio = fadeCurve.Evaluate(fadeElapsed);
            gameObject.transform.localScale = Vector3.Lerp(new Vector3(1f, 1f, 1f), new Vector3(0.01f, 0.01f, 0.01f), ratio);

            yield return null;
        } while (fadeElapsed < FadeInDuration);

        gameObject.SetActive(false);
        MessagePopupControl.instance.ForceBlocker(false);

        if (quitWins)
            Application.Quit();

        yield break;
    }
}
