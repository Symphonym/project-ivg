﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerListItem : MonoBehaviour {

    public Text RoomName;
    public Text MapName;
    public Text LobbyText;
    public Text PlayerText;

    public float DoubleClickThreshold = 0.3f;

    public Button ItemButton;

    public Color SelectedColor;

    ColorBlock m_startColor;
    float m_lastClickTime = 0f;

    void Awake()
    {
        m_startColor = ItemButton.colors;
    }
    void Start()
    {
        m_lastClickTime = Time.time;
    }

    public void SetSelected(bool selected)
    {
        ColorBlock colors = m_startColor;

        if(selected)
        {
            colors.normalColor = SelectedColor;
            colors.highlightedColor = SelectedColor;
            colors.pressedColor = SelectedColor;
        }

        ItemButton.colors = colors;
    }
    public bool CheckDoubleClick()
    {
        float elapsed = Time.time - m_lastClickTime;
        m_lastClickTime = Time.time;

        return elapsed <= DoubleClickThreshold;
    }
}
