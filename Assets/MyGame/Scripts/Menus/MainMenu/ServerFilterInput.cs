﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ServerFilterInput : MonoBehaviour {

    public static HashSet<int> AllowedMaps = new HashSet<int>();
    public static string RoomNameFilter = "";
    public static bool InLobby = false;
    public static bool AllowFullRooms = true;


    public GameObject MapFilterContent;
    public MapFilterItem MapFilterItemPrefab;
    public Text MapFilterText;

    public Toggle InLobbyToggle;
    public Toggle FullRoomToggle;

    List<MapFilterItem> m_mapFilters = new List<MapFilterItem>();

	// Use this for initialization
	void Start ()
    {
        for(int i = 0; i < Levels.instance.All.Count; i++)
        {
            Levels.LevelData level = Levels.instance.All[i];

            MapFilterItem filterItem = Instantiate(MapFilterItemPrefab, MapFilterContent.transform, false);
            filterItem.MapName.text = level.Name;
            filterItem.ItemToggle.isOn = true;
            int levelIndex = i;

            filterItem.ItemToggle.onValueChanged.AddListener((bool allowed) =>
            {

                if (allowed)
                    AllowedMaps.Add(levelIndex);
                else
                    AllowedMaps.Remove(levelIndex);

                UpdateMapFilterText();
            });

            m_mapFilters.Add(filterItem);
        }

        InLobbyToggle.isOn = InLobby;
        FullRoomToggle.isOn = AllowFullRooms;

        UpdateMapFilterText();
    }

    void UpdateMapFilterText()
    {
        MapFilterText.text = "Map filters (" + AllowedMaps.Count + "/" + m_mapFilters.Count + ")";
    }

    public void OnRoomNameChanged(string roomNameFilter)
    {
        RoomNameFilter = roomNameFilter;
    }
    public void OnInLobbyChanged(bool inLobby)
    {
        InLobby = inLobby;
    }
    public void OnFullRoomChanged(bool allowFullRoom)
    {
        AllowFullRooms = allowFullRoom;
    }

    public void OnSelectAllPressed()
    {
        foreach (MapFilterItem filterItem in m_mapFilters)
            filterItem.ItemToggle.isOn = true;
    }
    public void OnSelectNonePressed()
    {
        foreach (MapFilterItem filterItem in m_mapFilters)
            filterItem.ItemToggle.isOn = false;
    }


    public static void InitializeFilters()
    {
        InLobby = false;
        AllowFullRooms = true;
        for (int i = 0; i < Levels.instance.All.Count; i++)
            AllowedMaps.Add(i);
    }
}
