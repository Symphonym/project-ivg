﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitButtonInput : MonoBehaviour {

    [SerializeField]
    QuitWheelOfFortune m_wheelOfFortune;

    public void QuitGame()
    {
        MessagePopupControl.instance.ShowPopup("Are you sure?", "Exit game",
            new MessagePopupControl.MessagePopupButtonInfo("YES", () => { Application.Quit(); }),
            new MessagePopupControl.MessagePopupButtonInfo("Maybe?", () => { m_wheelOfFortune.SpinTheWheel(); }),
            new MessagePopupControl.MessagePopupButtonInfo("NO", () => { }));
    }
}
