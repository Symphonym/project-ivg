﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameMenuInput : MonoBehaviour {

    public Button LobbyButton;
    public Image BackgroundImage;
    public Text PlayerListText;
    public Text GameDurationText;
    public Text ItemsLeftText;
    public Text ItemsRequiredText;

    [SerializeField]
    List<GameMenuListItem> m_playerListItems = new List<GameMenuListItem>();

    [SerializeField]
    int m_maxNameLength = 12;
	
	void OnEnable()
    {
        UpdateLobbyButton();
        UpdatePlayerList();
        UpdateGameInfoText();

        BackgroundImage.color = new Color(0, 0, 0, 0.8f);
    }

    private void Update()
    {
        UpdateGameInfoText();
    }

    private void OnDisable()
    {
        BackgroundImage.color = new Color(0, 0, 0, 0);
    }

    void UpdateGameInfoText()
    {
        GameDurationText.text = "Time left: " + Utility.GetGametimeLeftAsString();

        int itemsScored = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomItemsScored_Key, 0);
        int itemsRequired = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomItemsRequired_Key, GameControl.instance.GetItemsLeft());

        int itemsNeededToWin = itemsRequired - itemsScored;
        ItemsRequiredText.text = "Items required: " + itemsNeededToWin;
        ItemsLeftText.text = "Items left: " + GameControl.instance.GetItemsLeft().ToString();
    }

    void UpdatePlayerList()
    {
        List<GameMenuListItem> m_unusedItems = new List<GameMenuListItem>(m_playerListItems);

        if(PhotonNetwork.inRoom)
        {
            foreach (PhotonPlayer player in PhotonNetwork.playerList)
            {
                GameMenuListItem listItem = m_unusedItems[0];
                m_unusedItems.RemoveAt(0);

                string team = Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
                int ping = Utility.GetPhotonPlayerProp<int>(player, ConstantVars.PlayerPing_Key, 0);

                string idText = PhotonNetwork.isMasterClient ? "[" + player.ID + "] " : "";

                Color teamColor = ConstantVars.NeutralColor;
                if (team == ConstantVars.GuardTeamName)
                    teamColor = ConstantVars.GuardColor;
                else if (team == ConstantVars.InvisibleTeamName)
                    teamColor = ConstantVars.InvisibleColor;

                string nameStr = "<color=#" + ColorUtility.ToHtmlStringRGBA(teamColor) + ">" + idText + Utility.ClampString(Utility.RemoveRichTextTags(player.NickName), m_maxNameLength) + "</color>";
                string pingStr = "<color=#" + Utility.GetPingColorCode(ping) + ">" + ping + "ms</color>";

                listItem.NameText.text = nameStr + " (" + pingStr + ")";
                listItem.HostIcon.gameObject.SetActive(player.IsMasterClient);
                listItem.gameObject.SetActive(true);
            }
        }

        foreach(GameMenuListItem item in m_unusedItems)
        {
            if (item.gameObject.activeSelf)
                item.gameObject.SetActive(false);
        }

        PlayerListText.text = "Player list (" + PhotonNetwork.room.PlayerCount + "/" + PhotonNetwork.room.MaxPlayers + ")";
        
    }
    void UpdateLobbyButton()
    {
        LobbyButton.interactable = PhotonNetwork.isMasterClient;
    }

    public void OnResumePressed()
    {
        GameUIControl.instance.SetPauseMenuVisible(false);
        GameControl.instance.SetGameInputPreventionKey(ConstantVars.GIP_GameMenu_Key, true);
        GameControl.instance.SetMouseRequiredKey(ConstantVars.MR_GameMenu_Key, false);
    }
  
    public void OnLobbyPressed()
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        GameControl.instance.EndGame(GameControl.EndReasons.ReturnToLobby);
    }
    public void OnLeavePressed()
    {
        GameUIControl.instance.SetPauseMenuVisible(false);
        GameControl.instance.LeaveRoom();
    }

    void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        UpdateLobbyButton();
    }

    void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
    {
        UpdatePlayerList();
    }
    void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        UpdatePlayerList();
    }
    void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        UpdatePlayerList();
    }
}
