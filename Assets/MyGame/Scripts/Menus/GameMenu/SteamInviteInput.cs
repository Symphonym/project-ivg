﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamInviteInput : MonoBehaviour {

	public void OnSteamInvitePressed()
    {
        SteamInviteControl.instance.OpenInviteDialog();
    }
}
