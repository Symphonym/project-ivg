﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardInput : MonoBehaviour {

    enum SortingModes
    {
        Player,
        Throw,
        Kill,
        Team,
        Ping
    };

    class SortingData
    {
        public PhotonPlayer Player;

        public string PlayerName;
        public string TeamName;
        public int Kills;
        public int Throws;
        public int Ping;
    }

    public Text HeaderText;
    public Text SubHeaderText;

    public Sprite DefaultProfileSprite;
    public Sprite DeadProfileSprite;
    public List<ScoreboardListItem> ScoreboardItems = new List<ScoreboardListItem>();

    SortingModes m_sortingMode = SortingModes.Player;
    bool m_inverseSorting = false;

    public void OnMenuOpen()
    {
        UpdatePlayerList();
        UpdateHeaderText();

        // Refresh the level progress bar
        GameEvents.instance.Trigger("OnLevelProgressBarPrepared");
    }

    void SetSortingMode(SortingModes mode)
    {
        if (m_sortingMode == mode)
            m_inverseSorting = !m_inverseSorting;

        m_sortingMode = mode;
    }

    public void OnPlayerHeaderPressed()
    {
        SetSortingMode(SortingModes.Player);
        UpdatePlayerList();
    }
    public void OnThrowHeaderPressed()
    {
        SetSortingMode(SortingModes.Throw);
        UpdatePlayerList();
    }
    public void OnKillHeaderPressed()
    {
        SetSortingMode(SortingModes.Kill);
        UpdatePlayerList();
    }
    public void OnTeamHeaderPressed()
    {
        SetSortingMode(SortingModes.Team);
        UpdatePlayerList();
    }
    public void OnPingHeaderPressed()
    {
        SetSortingMode(SortingModes.Ping);
        UpdatePlayerList();
    }

    void UpdateHeaderText()
    {
        GameControl.EndReasons endReason = (GameControl.EndReasons)Utility.GetPhotonRoomProp<int>(ConstantVars.RoomGameEndReason_Key, (int)GameControl.EndReasons.ReturnToLobby);

        switch (endReason)
        {
            case GameControl.EndReasons.AllItemsThrown:
                HeaderText.color = ConstantVars.InvisibleColor;
                SubHeaderText.color = ConstantVars.InvisibleColor;
                HeaderText.text = "Invisible Win";
                break;
            case GameControl.EndReasons.NotEnoughItems:
            case GameControl.EndReasons.InvisibleDead:
            case GameControl.EndReasons.TimeExpired:
                HeaderText.color = ConstantVars.GuardColor;
                SubHeaderText.color = ConstantVars.GuardColor;
                HeaderText.text = "Guards Win";
                break;
            case GameControl.EndReasons.ReturnToLobby:
                HeaderText.color = ConstantVars.NeutralColor;
                SubHeaderText.color = ConstantVars.NeutralColor;
                HeaderText.text = "Game Cancelled";
                break;
        }

        switch (endReason)
        {

            case GameControl.EndReasons.AllItemsThrown:
                SubHeaderText.text = "Enough items thrown away";
                break;
            case GameControl.EndReasons.NotEnoughItems:
                SubHeaderText.text = "Too many items wasted";
                break;
            case GameControl.EndReasons.InvisibleDead:
                SubHeaderText.text = "All Invisible defeated";
                break;
            case GameControl.EndReasons.TimeExpired:
                SubHeaderText.text = "Timer expired";
                break;
            case GameControl.EndReasons.ReturnToLobby:
                SubHeaderText.text = "Host returned to Lobby";
                break;
        }
    }

    void UpdatePlayerList()
    {
        if (!PhotonNetwork.inRoom)
            return;

        // Start of by hiding all scoreboard list items
        foreach(ScoreboardListItem item in ScoreboardItems)
        {
            if (item.gameObject.activeSelf)
                item.gameObject.SetActive(false);
        }

        List<SortingData> sortingData = new List<SortingData>();
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {

            string playerTeam = Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
            int throws = Utility.GetPhotonPlayerProp<int>(player, ConstantVars.PlayerThrows_Key, 0);
            int kills = Utility.GetPhotonPlayerProp<int>(player, ConstantVars.PlayerKills_Key, 0);
            int ping = Utility.GetPhotonPlayerProp<int>(player, ConstantVars.PlayerPing_Key, 0);

            SortingData data = new SortingData();
            data.Player = player;
            data.PlayerName =  player.NickName;
            data.TeamName = playerTeam;
            data.Kills = kills;
            data.Throws = throws;
            data.Ping = ping;
            sortingData.Add(data);
        }

        // Sort list according to sorting mode
        sortingData.Sort((SortingData a, SortingData b) =>
        {
            SortingData itemA = m_inverseSorting ? b : a;
            SortingData itemB = m_inverseSorting ? a : b;

            switch (m_sortingMode)
            {
                default:
                case SortingModes.Player:
                    return itemA.PlayerName.CompareTo(itemB.PlayerName);
                case SortingModes.Throw:
                    return itemA.Throws.CompareTo(itemB.Throws);
                case SortingModes.Kill:
                    return itemA.Kills.CompareTo(itemB.Kills);
                case SortingModes.Team:
                    return itemA.TeamName.CompareTo(itemB.TeamName);
                case SortingModes.Ping:
                    return itemA.Ping.CompareTo(itemB.Ping);
            }
        });

        // Populate scoreboard from the sorted list
        int index = 0;
        foreach(SortingData data in sortingData)
        {
            if (index >= ScoreboardItems.Count)
                break;

            bool alive = Utility.GetPhotonPlayerProp<bool>(data.Player, ConstantVars.PlayerAlive_Key, true);

            string idText = PhotonNetwork.isMasterClient ? "[" + data.Player.ID + "] " : "";
            string hostText = PhotonNetwork.masterClient.Equals(data.Player) ? " (Host)" : "";

            ScoreboardListItem item = ScoreboardItems[index];
            item.PlayerText.text = idText + data.PlayerName + hostText;
            item.ThrowText.text = data.Throws.ToString();
            item.KillText.text = data.Kills.ToString();
            item.TeamText.text = data.TeamName;
            item.PingText.text = "<color=#" + Utility.GetPingColorCode(data.Ping) + ">" + data.Ping.ToString() + "ms</color>";
            item.ProfileImg.sprite = alive ? DefaultProfileSprite : DeadProfileSprite;

            item.gameObject.SetActive(true);

            ++index;
        }
    }

    void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
    {
        UpdatePlayerList();
    }
    void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        UpdatePlayerList();
    }
    void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        UpdatePlayerList();
    }
    void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        UpdatePlayerList();
    }

    void OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        UpdateHeaderText();
    }
}
