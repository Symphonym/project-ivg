﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyMenuPlayer : MonoBehaviour {

    public PlayerModel Model;

    [SerializeField]
    Text m_playerNameUI;

    public void InitializeModel(string modelTeamName)
    {
        if (!PhotonNetwork.inRoom || Model != null)
            return;

        if (modelTeamName == ConstantVars.SpectatorTeamName)
            return;


        int levelIndex = Utility.GetPhotonRoomProp<int>("C0", 0);
        if (levelIndex < 0)
            return;

        Levels.LevelData level = Levels.instance.All[levelIndex];
        Model = Instantiate(modelTeamName == ConstantVars.InvisibleTeamName ? level.InvisibleModelPrefab : level.GuardModelPrefab, transform, false);
        Model.transform.localPosition = Vector3.zero;
    }

    public void SetText(string text, PhotonPlayer player)
    {
        string hostString = "<color=#EBFF94FF>[Host]</color>";

        if (m_playerNameUI != null)
            m_playerNameUI.text = (player.IsMasterClient ? hostString : "") + " " + text;

        // To make sure the correct cosmetics are shown
        if (Model != null)
            Model.SetAssociatedPhotonPlayer(player);
    }
}
