﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardListItem : MonoBehaviour
{
    public Image ProfileImg;

    public Text PlayerText;
    public Text ThrowText;
    public Text KillText;
    public Text TeamText;
    public Text PingText;
}
