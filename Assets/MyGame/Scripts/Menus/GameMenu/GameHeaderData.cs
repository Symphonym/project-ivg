﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHeaderData : MonoBehaviour {

    public Text GameDurationText;

    public Text TeamInvisibleText;

    public Text TeamGuardText;
}
