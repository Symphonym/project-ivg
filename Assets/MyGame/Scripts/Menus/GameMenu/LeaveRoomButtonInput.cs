﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaveRoomButtonInput : MonoBehaviour {

	public void OnLeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }
}
