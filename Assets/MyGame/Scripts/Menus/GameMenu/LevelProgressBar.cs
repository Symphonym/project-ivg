﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelProgressBar : MonoBehaviour {

    [SerializeField]
    Text m_titleText;

    [SerializeField]
    Image m_levelProgressImage;

    [SerializeField]
    Text m_fromLevelText;
    [SerializeField]
    Text m_toLevelText;

    [SerializeField]
    Text m_xpText;

    bool m_hasPlayed = false;


    private void Start()
    {
        GameEvents.instance.AddListener("OnLevelProgressBarPrepared", OnLevelProgressBarPrepared);
    }

    void SetBaseState()
    {
        float curXp = SteamStatsControl.instance.GetXPForPhotonPlayer(PhotonNetwork.player);

        m_titleText.text = "Level Progress";

        m_fromLevelText.text = XPControl.GetColoredLevelString(XPControl.CalculateLevel(curXp));
        m_toLevelText.text = XPControl.GetColoredLevelString(XPControl.CalculateLevel(curXp) + 1);
        m_xpText.text = Mathf.RoundToInt(XPControl.XPPerLevel() - XPControl.CalculateXPNeeded(curXp)).ToString() + "/" + Mathf.RoundToInt(XPControl.XPPerLevel()).ToString() + "XP";

        m_levelProgressImage.fillAmount = 1f - (XPControl.CalculateXPNeeded(curXp) / XPControl.XPPerLevel());
    }

    void StartAnimation()
    {
        // We need to be active otherwise StartCoroutine won't work
        if (!gameObject.activeInHierarchy)
            return;

        float xpGained = XPControl.instance.GetProgressBarXPGain();
        if (xpGained <= 0f || m_hasPlayed || !SteamManager.Initialized)
        {
            SetBaseState();
            return;
        }

        // Make sure this animation doesn't start again, until next game
        m_hasPlayed = true;

        StartCoroutine(CR_StartAnim(XPControl.instance.GetProgressBarCurXP(), xpGained, 1f));
    }

    IEnumerator CR_StartAnim(float curXp, float xpGain, float durationPerLevel)
    {
        // Initialize the progress bar display
        float levelProgress = 1f - (XPControl.CalculateXPNeeded(curXp) / XPControl.XPPerLevel());
        m_titleText.text = Mathf.RoundToInt(xpGain).ToString() + "XP";
        m_xpText.text = Mathf.RoundToInt(levelProgress * XPControl.XPPerLevel()) + "/" + Mathf.RoundToInt(XPControl.XPPerLevel()).ToString() + "XP";
        m_levelProgressImage.fillAmount = levelProgress;

        int curLevel = XPControl.CalculateLevel(curXp);

        m_fromLevelText.text = XPControl.GetColoredLevelString(curLevel);
        m_toLevelText.text = XPControl.GetColoredLevelString(curLevel + 1);

        yield return new WaitForSeconds(2f);
        yield return CR_PopAnim(m_titleText.transform, new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
        yield return CR_ShowProgress(curXp, xpGain, durationPerLevel);
    }

    IEnumerator CR_PopAnim(Transform target, Vector3 popScale, float duration)
    {
        float elapsed = -Time.deltaTime;

        AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, duration, 1f);

        do
        {
            elapsed += Time.deltaTime;
            float ratio = curve.Evaluate(elapsed);

            target.localScale = Vector3.Lerp(
                popScale,
                Vector3.one,
                ratio);

            yield return null;
        } while (elapsed < duration);
        yield break;
    }

    IEnumerator CR_ShowProgress(float curXp, float xpGain, float durationPerLevel)
    {
        if (xpGain <= 0f)
        {
            yield return new WaitForSeconds(0.5f);
            m_titleText.text = "Level Progress";
            SetBaseState();
            yield return CR_PopAnim(m_titleText.transform, new Vector3(1.3f, 1.3f, 1.3f), 0.5f);
            yield break;
        }

        AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, durationPerLevel, 1f);

        int curLevel = XPControl.CalculateLevel(curXp);

        m_fromLevelText.text = XPControl.GetColoredLevelString(curLevel);
        m_toLevelText.text = XPControl.GetColoredLevelString(curLevel + 1);

        float xpToGive = XPControl.CalculateXPNeeded(curXp);
        if (xpGain < xpToGive)
            xpToGive = xpGain;

        float newXp = curXp;
        float elapsed = -Time.deltaTime;
        do
        {
            elapsed += Time.deltaTime;
            float ratio = curve.Evaluate(elapsed);
            newXp = curXp + xpToGive * ratio;
            float levelProgress = 1f - (XPControl.CalculateXPNeeded(newXp) / XPControl.XPPerLevel());

            // Avoid the reset in XP needed when we reach a new level
            if (XPControl.CalculateLevel(newXp) != curLevel)
                levelProgress = 1f;


            m_titleText.text = Mathf.RoundToInt(xpGain - xpToGive * ratio).ToString() + "XP";
            m_xpText.text = Mathf.RoundToInt(levelProgress * XPControl.XPPerLevel()) + "/" + Mathf.RoundToInt(XPControl.XPPerLevel()).ToString() + "XP";
            m_levelProgressImage.fillAmount = levelProgress;


            yield return null;
        } while (elapsed < durationPerLevel);

        if(XPControl.CalculateLevel(newXp) != curLevel)
        {
            m_titleText.text = "Level Up!";
            yield return CR_PopAnim(m_titleText.transform, new Vector3(1.5f, 1.5f, 1.5f), 0.5f);
        }

        yield return CR_ShowProgress(curXp + xpToGive, xpGain - xpToGive, durationPerLevel);
    }

    void OnLevelProgressBarPrepared(GameEvents.Event eventData)
    {
        StartAnimation();
    }
}
