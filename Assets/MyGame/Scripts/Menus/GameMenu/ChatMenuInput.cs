﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatMenuInput : MonoBehaviour {

    public CanvasGroup ChatCanvasGroup;

    public Text PlaceholderText;
    public Text ChannelText;
    public InputField ChatInputField;

    public List<Text> ChatMenuItems = new List<Text>();

    public enum ChatMessageTypes : int
    {
        HostAnnouncement = 0, // An announcement from the host (e.g a player connected)
        NormalMessageAll, // A normal chat message, sent to ALL players
        NormalMessageTeam, // A normal chat message, sent to players on your team
        Ping, // A ping message
        PositionPing, // A position ping message

    }

    bool m_menuAvailable = true;
    bool m_sendToAll = false;
    bool m_previousFocus = false;

    IEnumerator m_fadeCR = null;

    PhotonView m_photonView;

	// Use this for initialization
	void Start ()
    {
        m_photonView = PhotonView.Get(this);

        PlaceholderText.text = "Enter to chat. Tab to switch channel.";
        ResetFade();
        UpdateChannelText();
    }

    void UpdateChannelText()
    {
        ChannelText.text = m_sendToAll ? "<color=#F2F265>All</color>" : "<color=#00FF00>Team</color>";
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (!PhotonNetwork.inRoom)
            return;

        if (m_previousFocus != ChatInputField.isFocused)
        {
            GameControl.instance.SetGameInputPreventionKey(ConstantVars.GIP_Chat_Key, !ChatInputField.isFocused);
            ResetFade();

            m_previousFocus = ChatInputField.isFocused;
        }


        if (ChatInputField.isFocused && Input.GetKeyDown(KeyCode.Tab))
        {
            m_sendToAll = !m_sendToAll;
            UpdateChannelText();
        }

        if (Input.GetKeyDown(KeyCode.Return) && m_menuAvailable)
        {
            // Focus input field
            ChatInputField.Select();
            ChatInputField.ActivateInputField();
        }

        m_menuAvailable = true;
	}

    public void OnMouseEnter()
    {
        ResetFade();
    }

    void ResetFade()
    {
        if (m_fadeCR != null)
            StopCoroutine(m_fadeCR);

        m_fadeCR = CR_FadeOut();
        StartCoroutine(m_fadeCR);

        ChatCanvasGroup.alpha = 1f;
    }

    IEnumerator CR_FadeOut()
    {
        const float Delay = 4f;
        const float FadeDuration = 1f;

        // Initial delay
        float elapsed = -Time.deltaTime;
        do
        {
            elapsed += Time.deltaTime;
            if (ChatInputField.isFocused)
                yield break;

            yield return null;
        } while (elapsed < Delay);

        // Fadeout
        AnimationCurve curve = AnimationCurve.EaseInOut(0f, 1f, FadeDuration, 0f);
        elapsed = -Time.deltaTime;
        do
        {
            elapsed += Time.deltaTime;
            if (ChatInputField.isFocused)
            {
                ChatCanvasGroup.alpha = 1f;
                yield break;
            }

            ChatCanvasGroup.alpha = curve.Evaluate(elapsed);

            yield return null;
        } while (elapsed < FadeDuration);


        yield return null;
    }

    public void OnChatSubmit()
    {
        if (Input.GetKey(KeyCode.Return))
        {
            if (ChatInputField.text.Length > 0)
            {
                bool isCommand = ChatInputField.text[0] == '/' && PhotonNetwork.isMasterClient;

                // Chat messages preceeded by a / are commands
                if (isCommand)
                {
                    string commandText = ChatInputField.text.Substring(1);
                    string[] parameters = commandText.Split(' ');
                     ParseCommand(parameters);
                }
                else
                {
                    string message = ChatInputField.text;
                    message = message.Replace("\\", "");
                    message = Utility.RemoveRichTextTags(message);

                    if (!string.IsNullOrEmpty(message))
                    {
                        // Make sure message isn't longer than what the input field allows
                        if (message.Length > ChatInputField.characterLimit)
                            message = message.Substring(0, ChatInputField.characterLimit);

                        SendChatMessage(message, m_sendToAll ? ChatMessageTypes.NormalMessageAll : ChatMessageTypes.NormalMessageTeam);
                    }
                }

                ChatInputField.text = "";
            }

            ChatInputField.DeactivateInputField();
            m_menuAvailable = false;
        }
    }

    public void SendChatMessage(string message, ChatMessageTypes messageType)
    {
        if (string.IsNullOrEmpty(message))
            return;

        // Send the chat message to the host, who will verify the message and broadcast it
        if (m_photonView != null && PhotonNetwork.masterClient != null)
        {
            m_photonView.RPC("SendChatMessageToHostRPC", PhotonTargets.MasterClient, message, (int)messageType);
            PhotonNetwork.SendOutgoingCommands(); // Force chat RPC to be sent
        }
    }

    public void SendLocalChatMessage(string message)
    {
        if (string.IsNullOrEmpty(message))
            return;

        string chatMessage = message;

        // Shuffle chat text upwards and then use the bottom-most text item
        for(int i = 0; i < ChatMenuItems.Count - 1; i++)
            ChatMenuItems[i].text = ChatMenuItems[i + 1].text;

        ChatMenuItems[ChatMenuItems.Count - 1].text = chatMessage;

        ResetFade();
    }

    [PunRPC]
    void SendChatMessageRPC(string message, PhotonMessageInfo info)
    {
        // Only the master client can send chat messages to all players
        if (!info.sender.IsMasterClient)
            return;

        SendLocalChatMessage(message);
    }
    [PunRPC]
    void SendChatMessageToHostRPC(string message, int messageTypeValue, PhotonMessageInfo info)
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        ChatMessageTypes messageType = (ChatMessageTypes)messageTypeValue;
        
        // Validate the message string only if it's not a Host Announcement
        if(messageType != ChatMessageTypes.HostAnnouncement)
        {
            message = message.Replace("\\", "");
            message = Utility.RemoveRichTextTags(message);
            if (message.Length > ChatInputField.characterLimit)
                message = message.Substring(0, ChatInputField.characterLimit);
        }

        // Only the master client can send HostAnnouncements
        if (messageType == ChatMessageTypes.HostAnnouncement && !info.sender.IsMasterClient)
            return;

        // Message formating
        else if (messageType == ChatMessageTypes.NormalMessageAll)
            message = Utility.ParseChatPlayerName(info.sender) + ": " + message;
        else if (messageType == ChatMessageTypes.NormalMessageTeam)
            message = "<color=#00FF00>(Team)</color> " + Utility.ParseChatPlayerName(info.sender) + ": " + message;
        else if (messageType == ChatMessageTypes.Ping)
            message = Utility.ParseChatPlayerName(info.sender) + " pinged from <color=#4286F4>" + message + "</color>!";
        else if (messageType == ChatMessageTypes.PositionPing)
            message = Utility.ParseChatPlayerName(info.sender) + " pinged towards <color=#4286F4>" + message + "</color>!";

        // Team messages and Pings are only sent to teammates
        if (messageType == ChatMessageTypes.NormalMessageTeam || messageType == ChatMessageTypes.Ping || messageType == ChatMessageTypes.PositionPing)
        {
            string playerTeam = Utility.GetPhotonPlayerProp<string>(info.sender, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
            foreach (PhotonPlayer player in PhotonNetwork.playerList)
            {
                string theirTeam = Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
                if (playerTeam.Equals(theirTeam))
                    m_photonView.RPC("SendChatMessageRPC", player, message);
            }
        }

        // Everything else is broadcasted
        else
            m_photonView.RPC("SendChatMessageRPC", PhotonTargets.All, message);
    }

    void ParseCommand(string[] parameters)
    {
        if (!PhotonNetwork.isMasterClient || parameters.Length <= 0)
            return;

        string command = parameters[0].ToLower();
        if (command == "kick" && parameters.Length == 2)
        {
            int playerID;
            if (System.Int32.TryParse(parameters[1], out playerID))
            {
                PhotonPlayer kickPlayer = PhotonPlayer.Find(playerID);
                if (kickPlayer != null)
                    PhotonNetwork.CloseConnection(kickPlayer);
            }

        }
    }
}
