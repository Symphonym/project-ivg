﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LobbyMenuInputNEW : MonoBehaviour {

    [SerializeField]
    List<LobbyMenuPlayer> m_invisLobbySlots = new List<LobbyMenuPlayer>();
    [SerializeField]
    List<LobbyMenuPlayer> m_guardLobbySlots = new List<LobbyMenuPlayer>();
    [SerializeField]
    List<LobbyMenuPlayer> m_specLobySlots = new List<LobbyMenuPlayer>();

    [SerializeField]
    Button m_teamInvisButton;
    [SerializeField]
    Button m_teamGuardButton;
    [SerializeField]
    Button m_teamSpecButton;

    [SerializeField]
    Dropdown m_spawnDropDown;
    [SerializeField]
    Button m_readyButton;
    [SerializeField]
    Image m_readyIcon;
    [SerializeField]
    Sprite m_readySprite;
    [SerializeField]
    Sprite m_notReadySprite;

    [SerializeField]
    Text m_roomNameText;
    [SerializeField]
    Text m_xpGainText;
    [SerializeField]
    Text m_gameDurationText;
    [SerializeField]
    Text m_itemPercentText;
    [SerializeField]
    Text m_itemDensityText;

    [SerializeField]
    Text m_teamText;
    [SerializeField]
    Text m_readyStatusText;

    [SerializeField]
    int m_maxNameLength = 12;

    class LobbyModelSlot
    {
        LobbyMenuPlayer player;
        Transform lobbyPosition;
    }

    // Player ID mapped to a slot
    Dictionary<int, LobbyModelSlot> m_lobbyModelSlots;


    // A pool of available lobby player objects, each player in the lobby is associated with a lobby player object
    List<LobbyMenuPlayer> m_availableLobbyPlayers;

    void Start ()
    {
        foreach (LobbyMenuPlayer lobbyPlayer in m_invisLobbySlots)
            lobbyPlayer.InitializeModel(ConstantVars.InvisibleTeamName);
        foreach (LobbyMenuPlayer lobbyPlayer in m_guardLobbySlots)
            lobbyPlayer.InitializeModel(ConstantVars.GuardTeamName);

        UpdateUI();
	}

    public void OnMenuOpen()
    {
        UpdateUI();
    }
	
    public void OnReadyPressed()
    {
        if (!PhotonNetwork.inRoom || !GameControl.instance.IsGameState(GameControl.GameStates.Lobby))
            return;

        bool currentReadyStatus = Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerReady_Key, false);

        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable.Add(ConstantVars.PlayerReady_Key, !currentReadyStatus);
        PhotonNetwork.player.SetCustomProperties(hashtable);

        UpdateUI();
    }

    public void OnSpawnChanged()
    {
        if (m_spawnDropDown.options.Count <= 0)
            return;

        PlayerSpawner.SelectedInvisibleSpawn = m_spawnDropDown.options[m_spawnDropDown.value].text;
    }

    public void OnJoinTeamSpectator()
    {
        if (!PhotonNetwork.inRoom || !GameControl.instance.IsGameState(GameControl.GameStates.Lobby))
            return;

        if (Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerReady_Key, false))
        {
            MessagePopupControl.instance.ShowPopup("You can't change team when you are ready.", "You are ready");
            return;
        }

        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable.Add(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
        PhotonNetwork.player.SetCustomProperties(hashtable);

        UpdateTeamInfo();
    }
    public void OnJoinTeamInvisible()
    {
        if (!PhotonNetwork.inRoom || !GameControl.instance.IsGameState(GameControl.GameStates.Lobby))
            return;

        if (Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerReady_Key, false))
        {
            MessagePopupControl.instance.ShowPopup("You can't change team when you are ready.", "You are ready");
            return;
        }

        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable.Add(ConstantVars.PlayerTeam_Key, ConstantVars.InvisibleTeamName);
        PhotonNetwork.player.SetCustomProperties(hashtable);

        UpdateTeamInfo();
    }
    public void OnJoinTeamGuard()
    {
        if (!PhotonNetwork.inRoom || !GameControl.instance.IsGameState(GameControl.GameStates.Lobby))
            return;

        if (Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerReady_Key, false))
        {
            MessagePopupControl.instance.ShowPopup("You can't change team when you are ready.", "You are ready");
            return;
        }

        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable.Add(ConstantVars.PlayerTeam_Key, ConstantVars.GuardTeamName);
        PhotonNetwork.player.SetCustomProperties(hashtable);

        UpdateTeamInfo();
    }
    void UpdateLobbyList()
    {
        List<LobbyMenuPlayer> m_slotsToDeactivate = new List<LobbyMenuPlayer>();
        m_slotsToDeactivate.AddRange(m_invisLobbySlots);
        m_slotsToDeactivate.AddRange(m_guardLobbySlots);
        m_slotsToDeactivate.AddRange(m_specLobySlots);


        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            string team = Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
            bool ready = Utility.GetPhotonPlayerProp<bool>(player, ConstantVars.PlayerReady_Key, false);
            int ping = Utility.GetPhotonPlayerProp<int>(player, ConstantVars.PlayerPing_Key, 0);

            string idText = PhotonNetwork.isMasterClient ? "[" + player.ID + "] " : "";
            string nameStr = "";
            string nameColor = ready ? "00FF00" : "FFFFFF";


            nameStr = idText + "<color=#" + nameColor + ">" + Utility.ClampString(Utility.RemoveRichTextTags(player.NickName), m_maxNameLength) + "</color>";
            string pingStr = "<color=#" + Utility.GetPingColorCode(ping) + ">" + ping + "ms</color>";

            int slotIndex = 0;
            foreach (PhotonPlayer otherPlayer in PhotonNetwork.playerList)
            {
                if (otherPlayer == player)
                    continue;

                // Keep increasing slot index while there exists a player with a lower ID/
                // This way a player will always have the same lobby slot (until someone leaves)
                // and avoid switching slots when people move between teams.
                if (otherPlayer.ID < player.ID)
                    ++slotIndex;
            }

            LobbyMenuPlayer lobbyPlayer = null;
            if (team == ConstantVars.SpectatorTeamName)
            {
                lobbyPlayer = m_specLobySlots[slotIndex];
                nameStr += " (" + pingStr + ")";
            }
            else if (team == ConstantVars.GuardTeamName)
            {
                lobbyPlayer = m_guardLobbySlots[slotIndex];
                nameStr += "\n" + pingStr;
            }
            else if (team == ConstantVars.InvisibleTeamName)
            {
                lobbyPlayer = m_invisLobbySlots[slotIndex];
                nameStr += "\n" + pingStr;
            }

            if (lobbyPlayer == null)
                continue;

            m_slotsToDeactivate.Remove(lobbyPlayer);

            lobbyPlayer.SetText(nameStr, player);

            lobbyPlayer.gameObject.SetActive(true);
        }

        // Deactivate unused slots
        foreach (LobbyMenuPlayer lobbyPlayer in m_slotsToDeactivate)
            lobbyPlayer.gameObject.SetActive(false);
    }
    void UpdateTeamInfo()
    {
        string team = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);

        ColorBlock specColors = m_teamSpecButton.colors;
        specColors.normalColor = team == ConstantVars.SpectatorTeamName ? specColors.highlightedColor : new Color(specColors.highlightedColor.r, specColors.highlightedColor.g, specColors.highlightedColor.b, 0);
        m_teamSpecButton.colors = specColors;

        ColorBlock invisColors = m_teamInvisButton.colors;
        invisColors.normalColor = team == ConstantVars.InvisibleTeamName ? invisColors.highlightedColor : new Color(invisColors.highlightedColor.r, invisColors.highlightedColor.g, invisColors.highlightedColor.b, 0);
        m_teamInvisButton.colors = invisColors;

        ColorBlock guardColors = m_teamGuardButton.colors;
        guardColors.normalColor = team == ConstantVars.GuardTeamName ? guardColors.highlightedColor : new Color(guardColors.highlightedColor.r, guardColors.highlightedColor.g, guardColors.highlightedColor.b, 0);
        m_teamGuardButton.colors = guardColors;

        m_spawnDropDown.gameObject.SetActive(team == ConstantVars.InvisibleTeamName);
    }
    void UpdateSpawnList()
    {
        m_spawnDropDown.ClearOptions();
        List<string> spawnNames = new List<string>();

        string team = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
        GameObject[] spawns = new GameObject[0];
        if (team.Equals(ConstantVars.InvisibleTeamName))
            spawns = GameObject.FindGameObjectsWithTag("TeamInvisible_Spawn");
        //else if(team.Equals(ConstantVars.GuardTeamName))
        //     spawns = GameObject.FindGameObjectsWithTag("TeamGuard_Spawn");

        foreach (GameObject spawn in spawns)
            spawnNames.Add(spawn.gameObject.name);
        spawnNames.Sort((string a, string b) =>
        {
            return a.CompareTo(b);
        });

        m_spawnDropDown.AddOptions(spawnNames);
        m_spawnDropDown.RefreshShownValue();

        // Only invisible can choose spawns
        m_spawnDropDown.interactable = team.Equals(ConstantVars.InvisibleTeamName);
        OnSpawnChanged(); // Make sure to refresh currently selected spawn
    }

    void UpdateRoomInfo()
    {
        // Update title tex
        m_roomNameText.text = PhotonNetwork.room.IsVisible ? PhotonNetwork.room.Name : "Private room";
        m_roomNameText.text += " (" + PhotonNetwork.room.PlayerCount + "/" + PhotonNetwork.room.MaxPlayers + ")";

        // Update XP gain text
        if (XPControl.IsXPGainAllowed())
            m_xpGainText.text = "XP Gain: <color=#00FF00>YES</color>";
        else
            m_xpGainText.text = "XP Gain: <color=#FF0000>NO (Min 3 players)</color>";

        // Update game duration text
        int gameDurationSeconds = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomSetting_GameDuration_Key, 0);
        int minutes = gameDurationSeconds / 60;
        int seconds = gameDurationSeconds % 60;
        m_gameDurationText.text = "Game duration: " + minutes + "m" + (seconds <= 0 ? "" : seconds + "s");

        int itemDensity = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomSetting_ItemDensity_Key, 0);
        m_itemDensityText.text = "Item density: " + ((GameControl.ItemDensities)itemDensity).ToString();

        // Update items required text
        float itemsRequiredPercentage = Utility.GetPhotonRoomProp<float>(ConstantVars.RoomSetting_ItemsRequired_Key, 0f);

        m_itemPercentText.text = "Items required: " + Mathf.RoundToInt(itemsRequiredPercentage * 100f) + "%";
    }

    void UpdateStatusText()
    {
        int readyCount = 0;

        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            bool ready = Utility.GetPhotonPlayerProp<bool>(player, ConstantVars.PlayerReady_Key, false);

            if (ready)
                ++readyCount;
        }

        string readyColor = ColorUtility.ToHtmlStringRGBA(readyCount == PhotonNetwork.room.PlayerCount ? Color.green : Color.red);
        m_readyStatusText.text = "Ready: <color=#" + readyColor + ">[" + readyCount + "/" + PhotonNetwork.room.PlayerCount + "]</color>";

        string myTeam = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
        string myColor = ColorUtility.ToHtmlStringRGBA(ConstantVars.NeutralColor);
        if (myTeam == ConstantVars.InvisibleTeamName)
            myColor = ColorUtility.ToHtmlStringRGBA(ConstantVars.InvisibleColor);
        else if (myTeam == ConstantVars.GuardTeamName)
            myColor = ColorUtility.ToHtmlStringRGBA(ConstantVars.GuardColor);

        m_teamText.text = "Playing as <color=#" + myColor + ">" + myTeam.ToUpper() + "</color>";
    }

    void UpdateUI()
    {
        if (!PhotonNetwork.inRoom)
            return;

        // Only update lobby UI when in the lobby game state
        if (!GameControl.instance.IsGameState(GameControl.GameStates.Lobby))
            return;

        m_readyIcon.sprite = Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerReady_Key, false) ? m_readySprite : m_notReadySprite;
        m_readyIcon.color = Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerReady_Key, false) ? Color.red : Color.green;

        UpdateLobbyList();
        UpdateStatusText();
        UpdateTeamInfo();
        UpdateSpawnList();
        UpdateRoomInfo();
    }

    bool AllPlayersReady()
    {
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            bool ready = Utility.GetPhotonPlayerProp<bool>(player, ConstantVars.PlayerReady_Key, false);
            if (!ready)
                return false;
        }
        return true;
    }


    void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
    {
        UpdateUI();


        // Start game when all players ready up
        if (!PhotonNetwork.inRoom
            || !PhotonNetwork.isMasterClient
            || !AllPlayersReady()
            || !GameControl.instance.IsGameState(GameControl.GameStates.Lobby))
            return;

        int currentLevelIndex = Utility.GetPhotonRoomProp<int>("C0", 0);
        Levels.LevelData level = Levels.instance.All[currentLevelIndex];

        if (level == null)
            return;

        // TODO: Did I Make a GameControlNEW class? also roomselectedindex won't be needed in future
        GameControl.instance.StartGame(level);
    }
    void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        UpdateUI();
    }
    void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        UpdateUI();
    }
    void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        UpdateUI();
    }
}
