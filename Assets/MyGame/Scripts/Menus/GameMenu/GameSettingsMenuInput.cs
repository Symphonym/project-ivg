﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSettingsMenuInput : MonoBehaviour {

    public Text ItemsRequiredText;
    public Slider ItemsRequiredSlider;

    public Text GameDurationText;
    public Slider GameDurationSlider;

    [SerializeField]
    Dropdown m_itemDensityDropdown;

    [SerializeField]
    Dropdown m_levelDropdown;

    [SerializeField]
    Image m_levelPreviewImage;

    [SerializeField]
    Button m_changeLevelButton;

    [SerializeField]
    Button m_resetScoreButton;

    [SerializeField]
    float m_changeLevelCooldown = 15;

    static float m_lastLevelChangeTimestamp = -1;

    private void Start()
    {
        if (m_lastLevelChangeTimestamp < 0)
            m_lastLevelChangeTimestamp = Time.time - m_changeLevelCooldown;

        // Load item densities
        m_itemDensityDropdown.ClearOptions();
        m_itemDensityDropdown.AddOptions(new List<string>() { GameControl.ItemDensities.Low.ToString(), GameControl.ItemDensities.Mid.ToString(), GameControl.ItemDensities.High.ToString() });
        m_itemDensityDropdown.RefreshShownValue();


        // Load level list
        m_levelDropdown.ClearOptions();
        foreach (Levels.LevelData level in Levels.instance.All)
            m_levelDropdown.options.Add(new Dropdown.OptionData(level.Name));
        m_levelDropdown.RefreshShownValue();

        int selLevelIndex = 0;
        int curLevelIndex = Utility.GetPhotonRoomProp<int>("C0", 0);

        Levels.LevelData curLevel = curLevelIndex > 0 ? Levels.instance.All[curLevelIndex] : null;

        foreach (Dropdown.OptionData option in m_levelDropdown.options)
        {
            if (curLevel == null)
                break;

            if (option.text.Equals(curLevel.Name))
            {
                m_levelDropdown.value = selLevelIndex;
                break;
            }
            ++selLevelIndex;
        }


        // Update preview image
        OnLevelChanged();

        UpdateUI();
    }

    public void OnMenuOpen()
    {
        UpdateUI();
    }

    void UpdateUI()
    {
        bool ready = Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerReady_Key, false);
        ItemsRequiredSlider.interactable = PhotonNetwork.isMasterClient && !ready;
        GameDurationSlider.interactable = PhotonNetwork.isMasterClient && !ready;
        m_itemDensityDropdown.interactable = PhotonNetwork.isMasterClient && !ready;

        // Stuff only the HOST sees
        m_levelDropdown.interactable = PhotonNetwork.isMasterClient;
        m_levelDropdown.gameObject.SetActive(PhotonNetwork.isMasterClient);
        m_levelPreviewImage.gameObject.SetActive(PhotonNetwork.isMasterClient);
        m_changeLevelButton.interactable = PhotonNetwork.isMasterClient;
        m_changeLevelButton.gameObject.SetActive(PhotonNetwork.isMasterClient);
        m_resetScoreButton.interactable = PhotonNetwork.isMasterClient;
        m_resetScoreButton.gameObject.SetActive(PhotonNetwork.isMasterClient);

        ItemsRequiredSlider.value = Utility.GetPhotonRoomProp<float>(ConstantVars.RoomSetting_ItemsRequired_Key, ItemsRequiredSlider.minValue);
        GameDurationSlider.value = (float)Utility.GetPhotonRoomProp<int>(ConstantVars.RoomSetting_GameDuration_Key, (int)GameDurationSlider.minValue);

        GameControl.ItemDensities itemDensity = (GameControl.ItemDensities)Utility.GetPhotonRoomProp<int>(ConstantVars.RoomSetting_ItemDensity_Key, (int)GameControl.ItemDensities.High);
        int selDensityIndex = 0;
        foreach (Dropdown.OptionData option in m_itemDensityDropdown.options)
        {
            if (option.text.Equals(itemDensity.ToString()))
            {
                m_itemDensityDropdown.value = selDensityIndex;
                break;
            }
            ++selDensityIndex;
        }
    }

    public void OnItemsRequiredChanged()
    {
        ItemsRequiredText.text = "Items required (" + Mathf.RoundToInt(ItemsRequiredSlider.value * 100f) + "%)";
    }
    public void OnFinishItemsRequiredChange()
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable.Add(ConstantVars.RoomSetting_ItemsRequired_Key, ItemsRequiredSlider.value);
        PhotonNetwork.room.SetCustomProperties(hashtable);
    }

    public void OnGameDurationChanged()
    {
        int gameDurationSeconds = (int)GameDurationSlider.value;
        int minutes = gameDurationSeconds / 60;
        int seconds = gameDurationSeconds % 60;
        GameDurationText.text = "Game duration (" + minutes + "m" + (seconds <= 0 ? "" : seconds + "s") + ")";
    }
    public void OnFinishGameDurationChange()
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable.Add(ConstantVars.RoomSetting_GameDuration_Key, (int)GameDurationSlider.value);
        PhotonNetwork.room.SetCustomProperties(hashtable);
    }

    public void OnItemDensityChanged()
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        GameControl.ItemDensities itemDensity = GameControl.ItemDensities.Low;
        string sliderValue = m_itemDensityDropdown.options[m_itemDensityDropdown.value].text;

        if(sliderValue.Equals(GameControl.ItemDensities.Low.ToString()))
            itemDensity = GameControl.ItemDensities.Low;
        else if (sliderValue.Equals(GameControl.ItemDensities.Mid.ToString()))
            itemDensity = GameControl.ItemDensities.Mid;
        else if (sliderValue.Equals(GameControl.ItemDensities.High.ToString()))
            itemDensity = GameControl.ItemDensities.High;

        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable.Add(ConstantVars.RoomSetting_ItemDensity_Key, (int)itemDensity);
        PhotonNetwork.room.SetCustomProperties(hashtable);
    }

    public void OnLevelChanged()
    {
        int levelIndex = Levels.instance.GetLevelIndex(m_levelDropdown.options[m_levelDropdown.value].text);
        if (levelIndex < 0)
            return;

        m_levelPreviewImage.sprite = Levels.instance.All[levelIndex].PreviewSprite;
    }

    public void OnChangeLevelPressed()
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        if(Time.time - m_lastLevelChangeTimestamp < m_changeLevelCooldown)
        {
            float secondsLeft = m_changeLevelCooldown - (Time.time - m_lastLevelChangeTimestamp);
            MessagePopupControl.instance.ShowPopup("Please wait " + System.Math.Round(secondsLeft, 2) + "s more before changing level again.", "Please wait");
            return;
        }

        int levelIndex = Levels.instance.GetLevelIndex(m_levelDropdown.options[m_levelDropdown.value].text);
        if (levelIndex < 0)
            return;

        Levels.LevelData level = Levels.instance.All[levelIndex];
        if (level == null)
            return;

        if (level.SceneName.Equals(ConsistentSceneLoader.instance.CurrentScene))
        {
            MessagePopupControl.instance.ShowPopup("You are already on that level.", "Error");
            return;
        }

        m_lastLevelChangeTimestamp = Time.time;
        GameControl.instance.StartGame(level);
    }

    public void OnResetScorePressed()
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
            hashtable.Add(ConstantVars.PlayerKills_Key, 0);
            hashtable.Add(ConstantVars.PlayerThrows_Key, 0);
            player.SetCustomProperties(hashtable);
        }
        GameUIControl.instance.Chat.SendChatMessage("The score was reset by the host!", ChatMenuInput.ChatMessageTypes.HostAnnouncement);
    }

    void OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        UpdateUI();
    }

    void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        UpdateUI();
    }
}
