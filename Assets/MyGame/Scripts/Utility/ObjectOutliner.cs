﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectOutliner {

    GameObject m_selectedObject;

	public void RemoveOutlines()
    {
        // Remove outliens from previously selected object
        if (m_selectedObject != null)
        {
            Renderer[] previousRenderers = m_selectedObject.GetComponentsInChildren<Renderer>();
            foreach (Renderer ren in previousRenderers)
            {
                cakeslice.Outline outline = ren.GetComponent<cakeslice.Outline>();
                if (outline != null)
                    GameObject.Destroy(outline);
            }

            m_selectedObject = null;
        }

    }
    public void ChangeOutlinedObject(GameObject obj, int colorIndex = 0)
    {
        if (obj == null || m_selectedObject == obj)
            return;

        if (m_selectedObject != null)
            RemoveOutlines();

        Renderer[] renderers = obj.GetComponentsInChildren<Renderer>();

        m_selectedObject = obj;
        foreach (Renderer ren in renderers)
        {
            cakeslice.Outline outline = ren.gameObject.AddComponent<cakeslice.Outline>();
            outline.color = colorIndex;
        }
    }
}
