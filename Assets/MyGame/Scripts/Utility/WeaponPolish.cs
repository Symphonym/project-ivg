﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPolish : MonoBehaviour {

    public float MaxAmount = 0.1f;
    public float Amount = 0.05f;
    public float Smoothing = 3f;

    public float MovementSway = 1f;
    public float MovementTilt = 45f;

    public float RecoilAngle = 45f;
    public float RecoilOffset = 0.5f;
    public float RecoilResetSpeed = 8f;

    Vector3 m_startRot;
    Vector3 m_startPos;

    [SerializeField]
    PlayerMovement m_movement;

	// Use this for initialization
	void Start () {
        m_startRot = transform.localEulerAngles;
        m_startPos = transform.localPosition;
	}

    private void OnEnable()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.AddListener("OnPlayerShoot", OnPlayerShoot);
    }
    private void OnDisable()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.RemoveListener("OnPlayerShoot", OnPlayerShoot);
    }

    void OnPlayerShoot(GameEvents.Event eventData)
    {
        transform.localRotation *= Quaternion.Euler(0, 0, RecoilAngle);
        transform.localPosition -= new Vector3(0, 0, RecoilOffset);
    }

    // Update is called once per frame
    void Update () {

        if (!PhotonNetwork.inRoom || m_movement == null)
            return;

        float factorY = (Input.GetAxis("Mouse X")) * Amount * (GameControl.instance.GameInputAllowed_IgnoreState() ? 1f : 0f);
        float factorZ = (Input.GetAxis("Mouse Y")) * Amount * (GameControl.instance.GameInputAllowed_IgnoreState() ? 1f : 0f);

        factorY = Mathf.Clamp(factorY, -MaxAmount, MaxAmount);
        factorZ = Mathf.Clamp(factorZ, -MaxAmount, MaxAmount);

        float factorX = (float)m_movement.GetSideInput() * MovementSway;
        factorZ += (float)m_movement.GetForwardInput() * MovementTilt;

        Quaternion swayRot = Quaternion.Euler(m_startRot.x + factorX, m_startRot.y + factorY, m_startRot.z + factorZ);
        transform.localRotation = Quaternion.Lerp(transform.localRotation, swayRot, (Time.deltaTime * Amount) * Smoothing);

        transform.localPosition = new Vector3(
            transform.localPosition.x,
            transform.localPosition.y,
            Mathf.Lerp(transform.localPosition.z, m_startPos.z, Time.deltaTime * RecoilResetSpeed));
    }
}
