﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSway : MonoBehaviour {

    public float MaxSwayHorizontal = 5f;
    public float MaxSwayVertical = 5f;
    public float SwayInputIntensity = 5f;
    public float SwaySmoothing = 5f;

    Quaternion m_startRot = Quaternion.identity;

	// Use this for initialization
	void Awake ()
    {
        m_startRot = transform.localRotation;
	}
	
	// Update is called once per frame
	void Update () {

        if (!PhotonNetwork.inRoom)
            return;


        float factorX = (Input.GetAxis("Mouse X")) * SwayInputIntensity * (GameControl.instance.GameInputAllowed_IgnoreState() ? 1f : 0f);
        float factorY = (Input.GetAxis("Mouse Y")) * SwayInputIntensity * (GameControl.instance.GameInputAllowed_IgnoreState() ? 1f : 0f);


        factorX = Mathf.Clamp(factorX, -MaxSwayHorizontal, MaxSwayHorizontal);
        factorY = Mathf.Clamp(factorY, -MaxSwayVertical, MaxSwayVertical);

        Quaternion swayRot = m_startRot * Quaternion.Euler(-factorY, factorX, 0f);
        transform.localRotation = Quaternion.Lerp(transform.localRotation, swayRot, Time.deltaTime * SwaySmoothing);
    }
}
