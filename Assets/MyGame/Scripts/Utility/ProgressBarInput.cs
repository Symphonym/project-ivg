﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBarInput : MonoBehaviour {

    public Image ProgressBar;
    public Text ProgressText;

	public void SetProgress(float progress, string text)
    {
        ProgressBar.fillAmount = progress;
        ProgressText.text = text;
    }
}
