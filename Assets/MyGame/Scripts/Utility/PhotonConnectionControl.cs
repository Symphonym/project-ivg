﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonConnectionControl : MonoBehaviour {

    public static PhotonConnectionControl instance;

    [HideInInspector]
    public List<string> SortedRegionList = new List<string>();

    bool m_reconnect = false;
    CloudRegionCode m_targetRegion = CloudRegionCode.eu;

    bool m_ignoreRegionChanges = false;

    private void Awake()
    {
        instance = this;
        SortedRegionList = new List<string>()
        {
            Utility.PhotonRegionToString(CloudRegionCode.asia),
            Utility.PhotonRegionToString(CloudRegionCode.au),
            Utility.PhotonRegionToString(CloudRegionCode.cae),
            Utility.PhotonRegionToString(CloudRegionCode.eu),
            Utility.PhotonRegionToString(CloudRegionCode.@in),
            Utility.PhotonRegionToString(CloudRegionCode.jp),
            Utility.PhotonRegionToString(CloudRegionCode.kr),
            Utility.PhotonRegionToString(CloudRegionCode.sa),
            Utility.PhotonRegionToString(CloudRegionCode.us),
            Utility.PhotonRegionToString(CloudRegionCode.usw)
        };
        SortedRegionList.Sort();

        DontDestroyOnLoad(gameObject);
    }

    // Use this for initialization
    void Start ()
    {
        // Check if user wants a specific region
        if (PlayerPrefs.HasKey(ConstantVars.PlayerPref_PreferredRegion_Key))
        {
            CloudRegionCode preferredRegion = (CloudRegionCode)PlayerPrefs.GetInt(ConstantVars.PlayerPref_PreferredRegion_Key);
            PhotonNetwork.OverrideBestCloudServer(preferredRegion);
        }

        PhotonNetwork.ConnectToBestCloudServer(ConstantVars.GameVersion);

        StartCoroutine(CR_ConnectionCheck());
	}
	
	// Update is called once per frame
	IEnumerator CR_ConnectionCheck ()
    {
        yield return new WaitForSeconds(1f);

        while (true)
        {
            if (PhotonNetwork.connectionState == ConnectionState.Disconnected)
                PhotonNetwork.ConnectToBestCloudServer(ConstantVars.GameVersion);
            yield return new WaitForSeconds(1f);
        }
	}

    public void ConnectToPhotonRegion(CloudRegionCode regionCode)
    {
        if (m_ignoreRegionChanges)
            return;

        PhotonNetwork.OverrideBestCloudServer(regionCode);
        m_targetRegion = regionCode;

        // If a connecting is already established/being established, disconnect
        // and reconnect to the new region
        if (PhotonNetwork.connectionState != ConnectionState.Disconnected)
        {
            m_reconnect = true;
            PhotonNetwork.Disconnect();
        }
        else
            PhotonNetwork.ConnectToBestCloudServer(ConstantVars.GameVersion);
    }

    public void SetIgnoreCloudChanges(bool ignoreCloudChanges)
    {
        m_ignoreRegionChanges = ignoreCloudChanges;
    }

    public CloudRegionCode GetCurrentCloudRegion()
    {
        return PhotonNetwork.networkingPeer.CloudRegion;
    }

    void OnConnectionFail(DisconnectCause cause)
    {
        MessagePopupControl.instance.ShowPopup("A connection to the region could not be established", "Error");
    }
    void OnConnectedToMaster()
    {
        // Immediately as connection to master is established, connect to SQL lobby
        PhotonNetwork.JoinLobby(new TypedLobby(ConstantVars.MatchMaking_SqlLobby_Name, LobbyType.SqlLobby));
    }
    void OnFailedToConnectToPhoton(DisconnectCause cause)
    {
        MessagePopupControl.instance.ShowPopup("A connection to the region could not be established", "Error");
    }

    void OnDisconnectedFromPhoton()
    {
        // Disconnected in order to connect to different master server
        if (m_reconnect)
        {
            m_reconnect = false;
            ConnectToPhotonRegion(m_targetRegion);
        }
    }
}
