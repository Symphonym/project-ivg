﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Steamworks;

public class Game_Kickoff : MonoBehaviour {

    public string StartScene;

    bool m_firstUpdate = false;

	void Start ()
    {
        PhotonNetwork.automaticallySyncScene = false; // TODO: True???
        PhotonNetwork.autoJoinLobby = false;
        PhotonNetwork.autoCleanUpPlayerObjects = true;
        //PhotonNetwork.AuthValues.UserId = STEAMID // TODO: Steam integration, use user steamID as unique user id

        OptionsControl.LoadOptions();
        KeybindControl.LoadKeybinds();

        MusicControl.instance.PlaySong("MainMenu_Music");
    }

    private void Update()
    {
        if(!m_firstUpdate)
        {
            m_firstUpdate = true;

            Utility.GenerateNonSteamUsername();
            Utility.InitializePhotonPlayerProps(PhotonNetwork.player);

            ConsistentSceneLoader.instance.LoadScene(StartScene, () =>
            {
                if (!SteamManager.Initialized)
                    MessagePopupControl.instance.ShowPopup(
                        "You're not connected to Steam. You will be seen as <color=#FF0000>\"" + Utility.NonSteam_Username + "\"</color> to other players and you will be unable to receive XP and gain levels",
                        "Steam connection failure",
                        new MessagePopupControl.MessagePopupButtonInfo("I understand", () => { }),
                        new MessagePopupControl.MessagePopupButtonInfo("Quit game", () => { Application.Quit(); }));
            });
        }
    }


}
