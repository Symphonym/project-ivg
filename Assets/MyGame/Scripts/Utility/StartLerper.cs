﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartLerper : MonoBehaviour {

    Vector3 m_startPos;
    Quaternion m_startRot;

	// Use this for initialization
	void Start ()
    {
        m_startPos = transform.position;
        m_startRot = transform.rotation;
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = Vector3.Lerp(transform.position, m_startPos, Time.deltaTime * 2f);
        transform.rotation = Quaternion.Lerp(transform.rotation, m_startRot, Time.deltaTime * 2f);
    }
}
