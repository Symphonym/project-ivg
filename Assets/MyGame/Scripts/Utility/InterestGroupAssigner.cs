﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class InterestGroupAssigner : MonoBehaviour {

    PhotonView m_photonView;

    private void Awake()
    {
        m_photonView = PhotonView.Get(this);
    }

    void Start ()
    {

        if(m_photonView.isMine)
            InvokeRepeating("UpdateGroupId", 0f, 1f / PhotonNetwork.sendRateOnSerialize);
    }
	
	void UpdateGroupId ()
    {
	    m_photonView.group = InterestGroupGrid.instance.GetInterestGroupFor(transform.position);
    }
}
