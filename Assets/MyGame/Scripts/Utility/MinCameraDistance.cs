﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinCameraDistance : MonoBehaviour {

    public float MinCamDistance = 10f;

    Vector3 m_startPos;

    private void Awake()
    {
        m_startPos = transform.localPosition;
    }

    // Use this for initialization
    void Start ()
    {
        UpdatePosition();
    }

    void UpdatePosition()
    {
        if (Camera.main == null)
            return;

        Vector3 desiredWorldPos = transform.parent.TransformPoint(m_startPos);
        Vector3 dirToObject = (desiredWorldPos - Camera.main.transform.position).normalized;
        transform.localPosition = transform.parent.InverseTransformPoint(Camera.main.transform.position + dirToObject * MinCamDistance);
    }

    // Update is called once per frame
    void Update ()
    {
        UpdatePosition();
    }
}
