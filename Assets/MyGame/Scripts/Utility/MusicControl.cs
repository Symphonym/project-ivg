﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicControl : MonoBehaviour {

    public static MusicControl instance;

    [System.Serializable]
    public class Song
    {
        public string name;
        public AudioClip song;
    }

   class Transition
    {
        public IEnumerator transition;
        public bool volumeUp;
    }

    public float SongTransitionDuration = 1f;

    [SerializeField]
    List<Song> m_availableSongs = new List<Song>();

    [SerializeField]
    List<AudioSource> m_audioSources = new List<AudioSource>();
    Dictionary<int, Transition> m_audioTransitions = new Dictionary<int, Transition>();

    int m_currentSourceIndex = 0;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
        // TODO: Add loop offset support
    }

    public void PlaySongUnlessAlreadyPlaying(string songName)
    {
        Song songToPlay = null;
        foreach (Song song in m_availableSongs)
        {
            if (song.name.Equals(songName))
            {
                songToPlay = song;
                break;
            }
        }
        bool sameSong = songToPlay != null && songToPlay.song == m_audioSources[m_currentSourceIndex].clip;
        if (songToPlay == null)
            return;

        if (!sameSong || !m_audioSources[m_currentSourceIndex].isPlaying || (m_audioTransitions.ContainsKey(m_currentSourceIndex) && !m_audioTransitions[m_currentSourceIndex].volumeUp))
            PlaySong(songName);
    }
    public void PlaySong(string songName)
    {
        if (m_audioSources.Count < 2f)
            return;

        Song songToPlay = null;
        foreach(Song song in m_availableSongs)
        {
            if(song.name.Equals(songName))
            {
                songToPlay = song;
                break;
            }
        }

        if (songToPlay == null)
            return;

        AudioSource currentSong = m_audioSources[m_currentSourceIndex];
        int currentSongIndex = m_currentSourceIndex;

        ++m_currentSourceIndex;
        if (m_currentSourceIndex >= m_audioSources.Count)
            m_currentSourceIndex = 0;

        AudioSource newSong = m_audioSources[m_currentSourceIndex];
        int newSongIndex = m_currentSourceIndex;
        
        if(currentSong.isPlaying)
        {
            // Stop any previous transitions on the currently playing song
            if (m_audioTransitions.ContainsKey(currentSongIndex))
            {
                StopCoroutine(m_audioTransitions[currentSongIndex].transition);
                m_audioTransitions.Remove(currentSongIndex);
            }

            // Start the transition to lower the volume of the current song
            Transition trans = new Transition();
            trans.transition = CR_Transition(currentSong, false);
            trans.volumeUp = false;
            m_audioTransitions.Add(currentSongIndex, trans);
            StartCoroutine(m_audioTransitions[currentSongIndex].transition);
        }


        // Stop any transitions on the audio source which we wanna play our new song on
        if (m_audioTransitions.ContainsKey(newSongIndex))
        {
            StopCoroutine(m_audioTransitions[newSongIndex].transition);
            m_audioTransitions.Remove(newSongIndex);
        }

        // Reset the audio source which we wanna play our new song on
        newSong.Stop();
        newSong.volume = 0f;
        newSong.clip = songToPlay.song;
        newSong.Play();

        // Fade the new song volume up
        Transition transIn = new Transition();
        transIn.transition = CR_Transition(newSong, true);
        transIn.volumeUp = true;
        m_audioTransitions.Add(newSongIndex, transIn);
        StartCoroutine(m_audioTransitions[newSongIndex].transition);
    }

    public bool IsPlaying()
    {
        return m_audioSources[m_currentSourceIndex].isPlaying;
    }

    public void StopSong()
    {
        AudioSource currentSong = m_audioSources[m_currentSourceIndex];

        // Stop any previous transitions on the currently playing song
        if (m_audioTransitions.ContainsKey(m_currentSourceIndex))
        {
            StopCoroutine(m_audioTransitions[m_currentSourceIndex].transition);
            m_audioTransitions.Remove(m_currentSourceIndex);
        }

        // Start the transition to lower the volume of the current song
        Transition transOut = new Transition();
        transOut.transition = CR_Transition(currentSong, false);
        transOut.volumeUp = false;
        m_audioTransitions.Add(m_currentSourceIndex, transOut);
        StartCoroutine(m_audioTransitions[m_currentSourceIndex].transition);
    }

    public void UpdateVolumeFromSettings()
    {
        for (int i = 0; i < m_audioSources.Count; i++)
        {
            //if (!m_audioTransitions.ContainsKey(i))
                //m_audioSources[i].volume = volumefromoptions;
        }
    }

    IEnumerator CR_Transition(AudioSource source, bool volumeUp)
    {
        AnimationCurve volumeFader = AnimationCurve.EaseInOut(0f, source.volume, SongTransitionDuration, (volumeUp ? 1f : 0f)); // TOOD: Multiply with Music sound factor

        // Offset elapsed time so we start at 0
        float elapsed = -Time.deltaTime;

        do
        {
            elapsed += Time.deltaTime;
            source.volume = volumeFader.Evaluate(elapsed);

            yield return null;
        } while (elapsed < SongTransitionDuration);

        for (int i = 0; i < m_audioSources.Count; i++)
        {
            // Register the transition as completed
            if (m_audioSources[i] == source)
            {
                m_audioTransitions.Remove(i);
                if (!volumeUp)
                    source.Stop();
                yield break;
            }
        }

        if (!volumeUp)
            source.Stop();
    }
}
