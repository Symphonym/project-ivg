﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuInitializer : MonoBehaviour {

	// Use this for initialization
	void Start ()
    {
        MusicControl.instance.PlaySongUnlessAlreadyPlaying("MainMenu_Music");
	}

    private void Update()
    {
        Utility.SetMouseLocked(false);
    }

}
