﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleFlash : MonoBehaviour {

    public float Lifetime = 0.2f;
    public float RemoteClientMieScatter = 0.7f;

    [SerializeField]
    List<GameObject> m_flashesToRotate = new List<GameObject>();

    [SerializeField]
    HxVolumetricLight m_muzzleLight;

	void Awake ()
    {
		foreach(GameObject flash in m_flashesToRotate)
            flash.transform.Rotate(new Vector3(0, 0, Random.Range(0, 360)), Space.Self);
	}

    private void Start()
    {
        Destroy(gameObject, Lifetime);

        bool isMine = false;

        // TODO: Increase intensity of muzzle flash light on remote clients?
        PhotonView view = GetComponentInParent<PhotonView>();
        if(view != null)
        {
            if (view.isMine)
                isMine = true;
        }

        m_muzzleLight.CustomMieScatter = !isMine;
        m_muzzleLight.MieScattering = isMine ? 0f : RemoteClientMieScatter;
    }
}
