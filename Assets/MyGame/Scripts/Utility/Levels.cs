﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Levels : MonoBehaviour {

    public static Levels instance;

    [System.Serializable]
    public class LevelData
    {
        public string Name;
        public string SceneName;
        public Sprite PreviewSprite;

        public PlayerModel InvisibleModelPrefab;
        public FPSRigData InvisibleFPSRig;
        public PlayerModel GuardModelPrefab;
        public FPSRigData GuardFPSRig;
    };

    public List<LevelData> All = new List<LevelData>();

    void Awake()
    {
        instance = this;
        
        // Sort level list based on name
        All.Sort((LevelData a, LevelData b) =>
        {
            return a.Name.CompareTo(b.Name);
        });
    }

    // Use this for initialization
    void Start ()
    {
        DontDestroyOnLoad(gameObject);
	}

    public LevelData GetLevel(string levelName)
    {
        foreach(LevelData level in All)
        {
            if (level.Name.Equals(levelName))
                return level;
        }
        return null;
    }
    public int GetLevelIndex(string levelName)
    {
        for(int i = 0; i < All.Count; i++)
        {
            if (All[i].Name.Equals(levelName))
                return i;
        }
        return -1;
    }
}
