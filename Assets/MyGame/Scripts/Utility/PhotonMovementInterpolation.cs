﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class PhotonMovementInterpolation : MonoBehaviour, IPunObservable {

    public bool IsUnreliableOnChange = false;
    public bool IsRigidBody = false;

    public class Snapshot
    {
        public Vector3 position;
        public Quaternion rotation;
        public float timestamp;
    }

    List<Snapshot> m_interpBuffer = new List<Snapshot>();
    Rigidbody m_rigidBody;
    PhotonView m_photonView;

    // Use this for initialization
    void Awake ()
    {
        m_photonView = PhotonView.Get(this);
        m_rigidBody = GetComponent<Rigidbody>();
    }

    /*public static double Hermite(double value1, double tangent1, double value2, double tangent2, double amount)
    {
        // All transformed to double not to lose precission
        // Otherwise, for high numbers of param:amount the result is NaN instead of Infinity
        double v1 = value1, v2 = value2, t1 = tangent1, t2 = tangent2, s = amount, result;
        double sCubed = s * s * s;
        double sSquared = s * s;

        if (amount == 0f)
            result = value1;
        else if (amount == 1f)
            result = value2;
        else
            result = (2 * v1 - 2 * v2 + t2 + t1) * sCubed +
                (3 * v2 - 3 * v1 - 2 * t1 - t2) * sSquared +
                t1 * s +
                v1;
        return (double)result;
    }*/

    void Update ()
    {
        if (m_photonView.isMine)
            return;

        float render_timestamp = Time.time - (1f / (PhotonNetwork.sendRateOnSerialize / 2f));

        // Drop older positions
        while (m_interpBuffer.Count >= 2 && m_interpBuffer[1].timestamp <= render_timestamp)
            m_interpBuffer.RemoveAt(0);

        if(m_interpBuffer.Count >= 2 && m_interpBuffer[0].timestamp <= render_timestamp && m_interpBuffer[1].timestamp >= render_timestamp)
        {
            Snapshot from = m_interpBuffer[0];
            Snapshot to = m_interpBuffer[1];

            // If there's a larger than normal difference between two adjacent snapshots, this is likely
            // because nothing has changed and no new snapshots have been received. So just change the oldest
            // timestamp as if had been received at a regular intervall to avoid moving over a too long period.
            if (IsUnreliableOnChange && to.timestamp - from.timestamp >= (1f / (float)PhotonNetwork.sendRateOnSerialize) * 4f)
            {
                m_interpBuffer[0].timestamp = render_timestamp;//to.timestamp - (1f / (float)PhotonNetwork.sendRateOnSerialize);
                return;
            }

            float ratio = (render_timestamp - from.timestamp) / (to.timestamp - from.timestamp);

            // If it's a rigidbody we negate all rigidbody physics when we are replaying past states
            if(IsRigidBody && m_rigidBody != null)
            {
                m_rigidBody.velocity = Vector3.zero;
                m_rigidBody.angularVelocity = Vector3.zero;
            }
            transform.localPosition = Vector3.Lerp(from.position, to.position, ratio);
            transform.localRotation = Quaternion.Slerp(from.rotation, to.rotation, ratio);
        }
        else if (m_interpBuffer.Count >= 1)
        {
            return;
            /*Snapshot latest = m_interpBuffer[m_interpBuffer.Count - 1];

            const float InterpLimit = 0.25f; // Taken from "Source Multiplayer"
            float maximumAllowedInterpolationDistance = (latest.speed * InterpLimit).magnitude;

            float currentInterpDistance = Vector3.Distance(transform.localPosition, latest.position);

            // We don't extrapolate for more than 0.25s due to risk of error
            if (currentInterpDistance >= maximumAllowedInterpolationDistance)
            {
                Debug.Log("Predicted TOO LONG: " + currentInterpDistance + " >= " + maximumAllowedInterpolationDistance);
                return;
            }

            Debug.Log("Predicting with speed " + latest.speed.magnitude + " EP: " + currentInterpDistance + " >= " + maximumAllowedInterpolationDistance);
            transform.localPosition += latest.speed * Time.deltaTime;
            //Debug.Log("Predicting speed " + m_interpBuffer[0].speed.magnitude);
            //transform.localPosition += m_interpBuffer[0].speed * Time.deltaTime;
            */
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.isWriting)
        {
            stream.SendNext(transform.localPosition);
            stream.SendNext(transform.localRotation);
        }
        else
        {
            Snapshot snapshot = new Snapshot();
            snapshot.position = (Vector3)stream.ReceiveNext();
            snapshot.rotation = (Quaternion)stream.ReceiveNext();
            snapshot.timestamp = Time.time;

            m_interpBuffer.Add(snapshot);
        }
    }
}
