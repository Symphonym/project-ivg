﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class InterestGroupGrid : MonoBehaviour {

    public static InterestGroupGrid instance;

    [Header("Grid map")]
    public float CellSize = 10f;
    public int GridWidth = 5;
    public int GridHeight = 5;

    [Header("Debug (only when running)")]
    [Tooltip("Shows a small debug map in the bottom right showing current interest on the grid")]
    public bool DrawDebugMap = false;
    public int DebugMapSize = 100;

    [Header("Debug (only in editor)")]
    [Tooltip("Draws the grid")]
    public bool DrawGrid = true;

    const int FirstGroupId = 1;
    const int LastGroupId = 249;

    public class GridCoordinate
    {
        public int x;
        public int y;

        public GridCoordinate(int xCoord, int yCoord)
        {
            x = xCoord;
            y = yCoord;
        }
    }

    // Each cell in the grid is assigned a value, indicating how many objects
    // are creating an interest for it. 
    // When the number goes above 0 we subscribe to its interest group id.
    // When the number goes back to 0 we unsubscribe. 
    short[,] m_interestMap;

    Texture2D m_debugMap;

    List<int> m_enableGroups = new List<int>();
    List<int> m_disableGroups = new List<int>();

    void Awake()
    {
        instance = this;
        m_interestMap = new short[GridWidth, GridHeight];

        // Initialize debug map
        m_debugMap = new Texture2D(GridWidth, GridHeight);
        m_debugMap.filterMode = FilterMode.Point;
        m_debugMap.wrapMode = TextureWrapMode.Clamp;
    }

    void Start()
    {
        List<int> enableGroups = new List<int>();
        for (int i = FirstGroupId; i <= LastGroupId; i++)
            enableGroups.Add(i);

        // Sending is always enabled on all groups because the culling is non-symmetric
        // We don't know if the remote client is interested in our data, hence we are always
        // sending 100% of the data. The culling occurs by selecting which data we receive.
        if(PhotonNetwork.connected)
            PhotonNetwork.SetSendingEnabled(enableGroups.ToArray(), null);
    }

    void OnGUI()
    {
        if(DrawDebugMap && m_debugMap != null)
            GUI.DrawTexture(new Rect(Screen.width - DebugMapSize, Screen.height - DebugMapSize, DebugMapSize, DebugMapSize), m_debugMap);
    }

    void UpdateDebugMap()
    {
        if (!DrawDebugMap || m_interestMap == null || m_debugMap == null)
            return;

        for (int y = 0; y < GridHeight; y++)
        {
            for (int x = 0; x < GridWidth; x++)
            {
                m_debugMap.SetPixel(x, GridHeight - 1 - y, m_interestMap[x, y] > 0 ? Color.green : Color.red);
            }
        }
        m_debugMap.Apply();
    }

    /// <summary>Updates the generated interest for an interest. If the generated interest has not changed, nothing will be done.</summary>
    /// <param name="interest">The interest to update</param>
    public void UpdateInterest(InterestGroupInterest_Base interest)
    {
        m_enableGroups.Clear();
        m_enableGroups.Capacity = interest.InterestSquareSize * interest.InterestSquareSize;
        m_disableGroups.Clear();
        m_disableGroups.Capacity = interest.InterestSquareSize * interest.InterestSquareSize;

        GridCoordinate centerCoord = GetCoordinatesFor(interest.transform.position);

        int startX = Mathf.Clamp(centerCoord.x - interest.InterestSquareSize / 2, 0, GridWidth - 1);
        int startY = Mathf.Clamp(centerCoord.y - interest.InterestSquareSize / 2, 0, GridHeight - 1);

        int endX = Mathf.Clamp(startX + interest.InterestSquareSize, 0, GridWidth);
        int endY = Mathf.Clamp(startY + interest.InterestSquareSize, 0, GridHeight);

        List<int> removals = new List<int>(interest.InterestingCells.Count);
        foreach (KeyValuePair<int, GridCoordinate> p in interest.InterestingCells)
        {
            // A cell that previously was interesting, is now no longer interesting
            if (!(p.Value.x >= startX && p.Value.x < endX &&
                p.Value.y >= startY && p.Value.y < endY))
            {
                removals.Add(p.Key);

                --m_interestMap[p.Value.x, p.Value.y];

                // Cell now has no interest, so prepare to disable it
                if (m_interestMap[p.Value.x, p.Value.y] == 0)
                    m_disableGroups.Add(p.Key);
            }

        }
        foreach (int removal in removals)
            interest.InterestingCells.Remove(removal);

        // Locate the new interesting cells
        for (int y = startY; y < GridHeight && y < endY; y++)
        {
            for (int x = startX; x < GridWidth && x < endX; x++)
            {
                int interestGroupId = GetInterestGroup(x, y);

                // Check if the cell is not already in our interesting cells, otherwise add it
                if (!interest.InterestingCells.ContainsKey(interestGroupId))
                {
                    ++m_interestMap[x, y];

                    // Cell previously had no interest, so prepare to enable it
                    if (m_interestMap[x, y] == 1)
                        m_enableGroups.Add(interestGroupId);

                    interest.InterestingCells.Add(interestGroupId, new GridCoordinate(x, y));

                }
            }
        }
    
        if ((m_enableGroups.Count > 0 || m_disableGroups.Count > 0) && PhotonNetwork.connected)
        {
            PhotonNetwork.SetReceivingEnabled(
                m_enableGroups.Count > 0 ? m_enableGroups.ToArray() : null,
                m_disableGroups.Count > 0 ? m_disableGroups.ToArray() : null);
        }


        UpdateDebugMap();
    }

    /// <summary>Removes all the interest generated by an interest, effectively making the interest 'uninteresting'</summary>
    /// <param name="interest">Interest to remove interest for</param>
    public void RemoveInterest(InterestGroupInterest_Base interest)
    {
        m_disableGroups.Clear();

        foreach (KeyValuePair<int, GridCoordinate> p in interest.InterestingCells)
        {
            --m_interestMap[p.Value.x, p.Value.y];

            // Cell now has no interest, so prepare to disable it
            if (m_interestMap[p.Value.x, p.Value.y] == 0)
                m_disableGroups.Add(p.Key);
        }
        interest.InterestingCells.Clear();
        
        if(m_disableGroups.Count > 0 && PhotonNetwork.connected)
        {
            PhotonNetwork.SetReceivingEnabled(
                null,
                m_disableGroups.ToArray());
        }

        UpdateDebugMap();
    }

    /// <summary>Checks if a world space position is inside the grid</summary>
    /// <param name="position">World space position to check</param>
    /// <returns>True if inside the grid, False otherwise.</returns>
    public bool GridContains(Vector3 position)
    {
        Vector3 startPos = GetGridStart();
        return
            position.x >= startPos.x &&
            position.x <= (startPos.x + GetTotalGridWidth()) &&
            position.z <= startPos.z &&
            position.z >= startPos.z - GetTotalGridHeight();
    }


    /// <summary>Converts a world space position to an interest group id. Returns 0 (default group) if position is outside the grid.</summary>
    /// <param name="position">World space position to convert</param>
    /// <returns>Interest group id for the position</returns>
    public byte GetInterestGroupFor(Vector3 position)
    {
        if (!GridContains(position))
            return (byte)0;

        GridCoordinate coords = GetCoordinatesFor(position);
        return (byte)GetInterestGroup(coords.x, coords.y);
    }

    /// <summary>Converts a grid position to an interest group id</summary>
    /// <param name="x">X grid coordinate</param>
    /// <param name="y">Y grid coordiante</param>
    /// <returns>Interest group id</returns>
    public int GetInterestGroup(int x, int y)
    {
        return y * GridWidth + x + FirstGroupId;
    }

    /// <summary>Gets the top-left position of the grid in world space</summary>
    /// <returns>Top-left position of the grid in world space</returns>
    public Vector3 GetGridStart()
    {
        return new Vector3(
            transform.position.x - (GetTotalGridWidth() / 2f),
            transform.position.y,
            transform.position.z + (GetTotalGridHeight() / 2f));
    }

    /// <summary> Gets center position of a grid cell in world space</summary>
    /// <returns>Center position of a grid cell in world space</returns>
    public Vector3 GetCellCenter(int x, int y)
    {
        Vector3 cellPos = GetGridStart();
        cellPos.x += (float)x * CellSize + CellSize / 2f;
        cellPos.z -= (float)y * CellSize + CellSize / 2f;
        return cellPos;
    }

    /// <summary>Gets the width of the grid in world space</summary>
    /// <returns>Width of the grid in world space</returns>
    public float GetTotalGridWidth()
    {
        return (float)GridWidth * CellSize;
    }
    /// <summary>Gets the height of the grid in world space</summary>
    /// <returns>Height of the grid in world space</returns>
    public float GetTotalGridHeight()
    {
        return (float)GridHeight * CellSize;
    }

    /// <summary>Converts a position in world space to coordinates on the grid. Coordinates are clamped to edges of grid if 'position' is outside it</summary>
    /// <param name="position">Position to convert to grid coordinates</param>
    /// <returns>Grid coordinates for the position</returns>
    public GridCoordinate GetCoordinatesFor(Vector3 position)
    {
        GridCoordinate coord = new GridCoordinate(0, 0);

        Vector3 localPosition = position - GetGridStart();

        coord.x = Mathf.Clamp(Mathf.FloorToInt(localPosition.x / CellSize), 0, GridWidth - 1);
        coord.y = Mathf.Clamp(Mathf.FloorToInt(-(localPosition.z / CellSize)), 0, GridHeight - 1); // Flip the vertical grid axis to originate from bottom left

        return coord;
    }


#if UNITY_EDITOR

    void Update()
    {
        int supportedCells = (LastGroupId - FirstGroupId) + 1;
        if (FirstGroupId + (GridWidth * GridHeight) - 1 > LastGroupId)
            Debug.LogError("Too many cells created by the grid, there are not enough interest group id's for each cell. Maxmimum amount is " + supportedCells + " cells. Currently there is " + (GridWidth * GridHeight) + " cells");

        if (!DrawGrid)
            return;

        // Drawing the grid in-edtior
        for (int y = 0; y < GridHeight; y++)
        {
            for (int x = 0; x < GridWidth; x++)
            {
                DrawDebugCell(x, y, Color.red);
            }
        }

    }

    void DrawDebugCell(int x, int y, Color color)
    {
        if (Application.isEditor)
        {
            Vector3 cellPos = GetGridStart();
            cellPos.x += (float)x * CellSize;
            cellPos.z -= (float)y * CellSize;

            Vector3 topLeft = cellPos;
            Vector3 topRight = new Vector3(cellPos.x + CellSize, cellPos.y, cellPos.z);
            Vector3 bottomRight = new Vector3(cellPos.x + CellSize, cellPos.y, cellPos.z - CellSize);
            Vector3 bottomLeft = new Vector3(cellPos.x, cellPos.y, cellPos.z - CellSize);

            Debug.DrawLine(topLeft, topRight, color);
            Debug.DrawLine(topRight, bottomRight, color);
            Debug.DrawLine(bottomRight, bottomLeft, color);
            Debug.DrawLine(bottomLeft, topLeft, color);
        }
    }

#endif

}
