﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIColorByTeam : MonoBehaviour {

    [System.Serializable]
    public class UIRenderer
    {
        public Graphic renderer;

        [Range(0, 255)]
        public byte alpha = 255;
    }
    public List<UIRenderer> UIRenderers = new List<UIRenderer>();

	void OnEnable ()
    {

		foreach(UIRenderer ui in UIRenderers)
        {
            string team = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
            Color32 color = ConstantVars.NeutralColor32;

            if (team == ConstantVars.InvisibleTeamName)
                color = ConstantVars.InvisibleColor32;
            else if (team == ConstantVars.GuardTeamName)
                color = ConstantVars.GuardColor32;

            color.a = ui.alpha;
            ui.renderer.color = color;
        }
	}
}
