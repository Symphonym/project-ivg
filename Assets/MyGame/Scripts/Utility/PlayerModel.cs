﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerModel : MonoBehaviour {

    public Transform FlashlightMount;

    public Transform MuzzleFlashPos;

    public List<Renderer> ModelRenderers;
    public Animator ModelAnimator;

    [SerializeField]
    List<Text> m_levelTexts = new List<Text>();

    PhotonPlayer m_associatedPlayer = null;
    List<Material> m_origMats = new List<Material>();

    private void Start()
    {
        foreach (Renderer r in ModelRenderers)
            m_origMats.Add(r.material);

        UpdateCosmetics();
    }

    private void OnEnable()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.AddListener("OnPlayerXPChanged", OnPlayerXPChanged);
    }
    private void OnDisable()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.RemoveListener("OnPlayerXPChanged", OnPlayerXPChanged);
    }

    public void DetachIntoRagdollMode()
    {
        // Detach and move up to our parent's parent
        gameObject.transform.SetParent(gameObject.transform.parent.parent);

        // Disable the animator
        ModelAnimator.enabled = false;

        // Reset the model materials if they've been changed (such as the Invisibles being invisible)
        int index = 0;
        foreach (Renderer r in ModelRenderers)
        {
            r.material = m_origMats[index];
            ++index;
        }

        // Make sure all colliders are treated as ragdolls
        Collider[] colliders = GetComponentsInChildren<Collider>();
        foreach (Collider c in colliders)
        {
            c.isTrigger = false;
            c.gameObject.layer = LayerMask.NameToLayer("Ragdoll");
        }

        // Enable physics interactions for all the associated rigidbodies
        Rigidbody[] rigidbodies = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody r in rigidbodies)
            r.isKinematic = false;
    }

    public void SetAssociatedPhotonPlayer(PhotonPlayer player)
    {
        if(m_associatedPlayer != player)
        {
            m_associatedPlayer = player;
            UpdateCosmetics();
        }
    }

    void UpdateCosmetics()
    {
        // Retrieve player associated with the model
        PhotonView view = GetComponentInParent<PhotonView>();
        PhotonPlayer owner = null;

        if (view != null)
            owner = view.isMine ? PhotonNetwork.player : view.owner;
        else if (m_associatedPlayer != null)
            owner = m_associatedPlayer;

        if (owner == null)
            return;


        // Update Level text
        float curXp = SteamStatsControl.instance.GetXPForPhotonPlayer(owner);
        SetLevelText(XPControl.CalculateLevel(curXp));

        // Make sure any visible part is marked as our own player model (so we don't render it on our local camera)
        if (owner.IsLocal)
        {
            SetLevelTextLayer("OwnPlayerModel");

            foreach (Renderer ren in ModelRenderers)
                ren.gameObject.layer = LayerMask.NameToLayer("OwnPlayerModel");

            // Playerhitbox for own model is put in a different (non-colliding) layer since it
            // is only used by remote players.
            Collider[] colliders = GetComponentsInChildren<Collider>();
            foreach (Collider c in colliders)
            {
                if (c.gameObject.layer == LayerMask.NameToLayer("PlayerHitbox"))
                    c.gameObject.layer = LayerMask.NameToLayer("OwnPlayerModel");
            }
        }
        else
        {
            SetLevelTextLayer("Player");
            foreach (Renderer ren in ModelRenderers)
                ren.gameObject.layer = LayerMask.NameToLayer("Player");
        }

        // TODO: Check cosmetic custom player property, and compare to level, if allowed
        // instantiate cosmetic.
        // TODO: Cosmetic model needs to be added and removed from model rendered list,
        // ACTUALLY probably just make separate CosmeticORIG_Material and update that and shit
        // since original materials for invisible is set in start function.
    }

    void OnPlayerXPChanged(GameEvents.Event eventData)
    {
        UpdateCosmetics();
    }

    void SetLevelText(int level)
    {
        Color color = XPControl.GetLevelColor(level);

        foreach (Text text in m_levelTexts)
        {
            text.text = level.ToString();
            text.material.SetColor("_Specular", color);
        }
    }

    void SetLevelTextLayer(string layer)
    {
        foreach (Text text in m_levelTexts)
        {
            Canvas parentCanvas = text.gameObject.GetComponentInParent<Canvas>();
            if(parentCanvas != null)
                parentCanvas.gameObject.layer = LayerMask.NameToLayer(layer);

            text.gameObject.layer = LayerMask.NameToLayer(layer);
        }
    }
    public void SetCosmeticsInvisible(bool invisible)
    {
        foreach (Text text in m_levelTexts)
        {
            if (!text.gameObject.activeSelf != invisible)
                text.gameObject.SetActive(!invisible);
        }
    }
}
