﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFacingBillboard : MonoBehaviour {

    Camera m_camera;


    void Start()
    {
        UpdateRotation();
    }

    void Update()
    {
        UpdateRotation();
    }

    void UpdateRotation()
    {
        if (m_camera == null || m_camera != Camera.main)
        {
            if (Camera.main == null)
                return;
            else
                m_camera = Camera.main;
        }

        transform.LookAt(transform.position + m_camera.transform.rotation * Vector3.forward,
            m_camera.transform.rotation * Vector3.up);
    }
}
