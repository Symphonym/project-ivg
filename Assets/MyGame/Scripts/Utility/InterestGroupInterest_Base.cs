﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class InterestGroupInterest_Base : MonoBehaviour {

    /// <summary>
    /// Frequence and order of culling
    /// </summary>
    // True overrides the interest group id (message is sent)
    // whereas False means that interest group id given by the grid is used. This only has an
    // effect for clients who are not interested in the id of this interest, in which case the
    // amount of True values indicate how often it receives an update. In the case of
    // { true, false}, every other message is sent (50% bandwidth)
    [Tooltip("Frequence and order of culling. For every message sent either 'true' or 'false' is applied. true sends message to everyone, false only to those interested. { true, false } or { false, true} indicates 50% bandwidth if we are NOT interested in this interest.")]
    public List<bool> CullingOrder = new List<bool>(new bool[2] { true, false });

    /// <summary>
    /// Size of the square of cells that generates interest for this interest. Uneven values are recommended.
    /// </summary>
    [Tooltip("Interest is generated in a square area on the grid. This is the size (in cells) of that square. Uneven values provide a good square.")]
    public int InterestSquareSize = 3;

    /// <summary>
    /// All the cells that this interest is generating interest for (cells that it makes 'interesting')
    /// </summary>
    [HideInInspector]
    public Dictionary<int, InterestGroupGrid.GridCoordinate> InterestingCells = new Dictionary<int, InterestGroupGrid.GridCoordinate>();

    protected PhotonView m_photonView;
    bool m_previousInterest = false;
    int m_previousGroupId = -1;
    int m_cullingIndex = 0;

    bool m_requiresUpdate = false;

    protected virtual void Start ()
    {
        m_photonView = PhotonView.Get(this);
        InvokeRepeating("UpdateCulling", 0f, 1f / PhotonNetwork.sendRateOnSerialize);
	}
	
    void OnDestroy()
    {
        // Remove all our generated interest when we are destroyed
        if(InterestGroupGrid.instance != null)
            InterestGroupGrid.instance.RemoveInterest(this);
    }

    /// <summary>
    /// Checks if this interest in interesting
    /// </summary>
    /// <returns>True if this interest is interesting, false otherwise.</returns>
    public virtual bool IsInteresting()
    {
        return true;
    }

    void UpdateCulling()
    {
        if (InterestGroupGrid.instance == null)
            return;

        m_requiresUpdate = false;

        int newInterestGroup = InterestGroupGrid.instance.GetInterestGroupFor(transform.position);

        bool newInterest = IsInteresting();

        // This only does something if our group id is being culled
        // It allows for every Xth (or whatever 'order' is defined by the variable)
        // to be sent over the default '0' global group so everyone gets it.
        // So if order is 'true, false' we get 50% bandwidth if a client is not interested in our normal group id.
        if (CullingOrder[m_cullingIndex++ % CullingOrder.Count])
            newInterestGroup = 0;

        if (m_photonView.group != newInterestGroup)
        {
            // The view group is only controlled if you own the interest
            if (m_photonView.isMine)
                m_photonView.group = newInterestGroup;

            // If our 'real' interest group changed, the interest moved and an update is required
            if(newInterestGroup != 0)
            {
                if(m_previousGroupId != newInterestGroup)
                {
                    m_previousGroupId = newInterestGroup;
                    m_requiresUpdate = true;
                }
            }
        }

        // Our interest change, make sure to update us in the grid
        if(m_previousInterest != newInterest)
        {
            m_previousInterest = newInterest;
            m_requiresUpdate = true;
        }

        if(m_requiresUpdate)
        {
            // Interest is 'interesting', update the interest it generates
            if (newInterest)
                InterestGroupGrid.instance.UpdateInterest(this);

            // Interest is 'uninteresting', remove all of its generated interest
            else
                InterestGroupGrid.instance.RemoveInterest(this);
        }
    }
}
