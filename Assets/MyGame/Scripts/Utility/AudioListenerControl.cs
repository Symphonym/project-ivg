﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioListenerControl : MonoBehaviour {

    public AudioListener ListenerPrefab;
    public static AudioListenerControl instance;

    AudioListener m_listener = null;
    Camera m_curCam = null;

    void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);

        m_listener = Instantiate(ListenerPrefab, transform);
    }

    void Update()
    {
        if(m_curCam != Camera.main)
            m_curCam = Camera.main;

        if (m_curCam != null)
        {
            m_listener.transform.position = m_curCam.transform.position;
            m_listener.transform.rotation = m_curCam.transform.rotation;
        }
    }
}
