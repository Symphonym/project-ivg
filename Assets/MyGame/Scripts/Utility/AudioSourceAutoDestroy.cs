﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AudioSourceAutoDestroy : MonoBehaviour {

    AudioSource m_audioSource;
    bool m_allowDestruction = false;

	// Use this for initialization
	void Awake () {
        m_audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!m_audioSource.isPlaying && m_allowDestruction)
            Destroy(gameObject);
	}

    public void AllowDestruction()
    {
        m_allowDestruction = true;
    }
}
