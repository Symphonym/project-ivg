﻿using UnityEngine;
using Steamworks;

public static class Utility {

    public static string NonSteam_Username = "";
    public static void GenerateNonSteamUsername()
    {
        const string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        NonSteam_Username = "Non-Steam User " + Random.Range(0, 10) + alphabet[Random.Range(0, alphabet.Length)].ToString();
    }

    public static string ParseChatPlayerName(PhotonPlayer player)
    {
        string nameString = ParseChatPlayerNameNoTags(player);

        string tags = GetPlayerTags(player);

        if (tags.Length > 0)
            return tags + " " + nameString;
            
        return nameString;
    }
    public static string ParseChatPlayerNameNoTags(PhotonPlayer player)
    {
        string name = RemoveRichTextTags(player.NickName);

        string nameColor = ColorUtility.ToHtmlStringRGBA(ConstantVars.NeutralColor);
        string team = Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);

        if (team == ConstantVars.InvisibleTeamName)
            nameColor = ColorUtility.ToHtmlStringRGBA(ConstantVars.InvisibleColor);
        else if (team == ConstantVars.GuardTeamName)
            nameColor = ColorUtility.ToHtmlStringRGBA(ConstantVars.GuardColor);

        name = "<color=#" + nameColor + ">" + name + "</color>";
        return name;
    }

    public static string GetPlayerTags(PhotonPlayer player)
    {
        CSteamID playerSteamId = SteamIdentityControl.instance.GetPhotonPlayerSteamId(player);
        foreach (ConstantVars.PlayerTag playerTag in ConstantVars.GamePlayerTags)
        {
            if (playerTag.requiredSteamId == playerSteamId)
                return "<color=#" + ColorUtility.ToHtmlStringRGBA(playerTag.tagColor) + ">" + playerTag.tagName + "</color>";
        }

        return string.Empty;
    }

    public static string ClampString(string inputStr, int maxLength, string trailer = "...")
    {
        if (inputStr.Length > maxLength)
            return inputStr.Substring(0, maxLength) + trailer;
        return inputStr;
    }


    public static string ResolutionToString(Resolution res)
    {
        return res.width + "x" + res.height + " (" + res.refreshRate + "hz)";
    }

    public static string RemoveRichTextTags(string inputString)
    {
        return System.Text.RegularExpressions.Regex.Replace(inputString, "[<>]", string.Empty);
    }

    public static void SetMouseLocked(bool locked)
    {
        Cursor.lockState = locked ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !locked;
    }

    public static string GetPingColorCode(int ping)
    {
        string colorCode = "00FF00";
        if (ping > 60 && ping <= 150)
            colorCode = "FFFF00";
        else if (ping > 150)
            colorCode = "FF0000";

        return colorCode;
    }

    public static string GetPlayerTeamColorString(PhotonPlayer player)
    {
        string playerTeam = GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
        string colorString = ColorUtility.ToHtmlStringRGBA(ConstantVars.NeutralColor);

        if (playerTeam == ConstantVars.InvisibleTeamName)
            colorString = ColorUtility.ToHtmlStringRGBA(ConstantVars.InvisibleColor);
        else if (playerTeam == ConstantVars.GuardTeamName)
            colorString = ColorUtility.ToHtmlStringRGBA(ConstantVars.GuardColor);

        return colorString;
    }

    public static string GetGametimeLeftAsString()
    {
        int timeleft;
        return GetGametimeLeftAsString(out timeleft);
    }
    public static string GetGametimeLeftAsString(out int timeLeftOut)
    {
        if (!PhotonNetwork.inRoom)
        {
            timeLeftOut = 0;
            return "??:??";
        }

        int gameDurationTime = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomSetting_GameDuration_Key, 300);

        double curTime = PhotonNetwork.time;
        double startTime = Utility.GetPhotonRoomProp<double>(ConstantVars.RoomGameStartTime_Key, 0);

        double timeElapsed = (curTime - startTime);
        int timeLeft = (int)(gameDurationTime - timeElapsed);
        if (timeLeft <= 0)
            timeLeft = 0;

        int minutesLeft = timeLeft / 60;
        int secondsLeft = timeLeft % 60;

        timeLeftOut = timeLeft;
        return minutesLeft.ToString("00") + ":" + secondsLeft.ToString("00");
    }

    public static void InitializePhotonPlayerProps(PhotonPlayer player)
    {
        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable[ConstantVars.PlayerPing_Key] = Utility.GetPhotonPlayerProp<int>(player, ConstantVars.PlayerPing_Key, 0);
        hashtable[ConstantVars.PlayerTeam_Key] = ConstantVars.SpectatorTeamName;
        hashtable[ConstantVars.PlayerKills_Key] = 0;
        hashtable[ConstantVars.PlayerThrows_Key] = 0;
        hashtable[ConstantVars.PlayerLoaded_Key] = false;
        hashtable[ConstantVars.PlayerReady_Key] = false;
        hashtable[ConstantVars.PlayerAlive_Key] = true;
        if (SteamManager.Initialized)
        {
            hashtable[ConstantVars.PlayerSteamIDBackup_Key] = SteamUser.GetSteamID().m_SteamID.ToString();
            PhotonNetwork.playerName = SteamFriends.GetPersonaName();
        }
        else
            PhotonNetwork.playerName = NonSteam_Username;

        player.SetCustomProperties(hashtable);
    }

    public static T GetPhotonPlayerProp<T>(PhotonPlayer player, string key, T defaultValue)
    {
        if (player.CustomProperties.ContainsKey(key))
            return (T)player.CustomProperties[key];
        else
            return defaultValue;
    }
    public static T GetPhotonPlayerProp<T>(string key, T defaultValue)
    {
        return GetPhotonPlayerProp<T>(PhotonNetwork.player, key, defaultValue);
    }
    public static T GetPhotonRoomProp<T>(RoomInfo room, string key, T defaultValue)
    {
        try
        {
            if (room.CustomProperties.ContainsKey(key))
                return (T)room.CustomProperties[key];
            else
                return defaultValue;
        }
        catch

        {
            Debug.Log(System.Environment.StackTrace);
            return defaultValue;
        }

    }
    public static T GetPhotonRoomProp<T>(string key, T defaultValue)
    {
        return GetPhotonRoomProp<T>(PhotonNetwork.room, key, defaultValue);
    }

    public static CloudRegionCode StringToPhotonRegion(string regionString)
    {
        CloudRegionCode realRegionCode = CloudRegionCode.eu;
        switch (regionString.ToLower())
        {
            case "europe": break;
            case "usa, east": realRegionCode = CloudRegionCode.us; break;
            case "usa, west": realRegionCode = CloudRegionCode.usw; break;
            case "asia": realRegionCode = CloudRegionCode.asia; break;
            case "canada, east": realRegionCode = CloudRegionCode.cae; break;
            case "australia": realRegionCode = CloudRegionCode.au; break;
            case "japan": realRegionCode = CloudRegionCode.jp; break;
            case "south korea": realRegionCode = CloudRegionCode.kr; break;
            case "south america": realRegionCode = CloudRegionCode.sa; break;
            case "india": realRegionCode = CloudRegionCode.@in; break;
        }
        return realRegionCode;
    }
    public static string PhotonRegionToString(CloudRegionCode regionCode)
    {
        switch(regionCode)
        {
            case CloudRegionCode.us: return "USA, East";
            case CloudRegionCode.usw: return "USA, West";
            case CloudRegionCode.asia: return "Asia";
            case CloudRegionCode.au: return "Australia";
            case CloudRegionCode.cae: return "Canada, East";
            case CloudRegionCode.jp: return "Japan";
            case CloudRegionCode.kr: return "South Korea";
            case CloudRegionCode.sa: return "South America";
            case CloudRegionCode.@in: return "India";
            default:
            case CloudRegionCode.eu: return "Europe";
        }
    }

    public static PhotonPlayer GetPhotonOwner(this PhotonView photonView)
    {
        return photonView.isMine ? PhotonNetwork.player : photonView.owner;
    }
}
