﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSRigData : MonoBehaviour {

    public Animator RigAnimator;
    public Transform RigPistol;
    public Transform RigPistolMuzzlePosition;


    public Quaternion RigPistol_StartRotation = Quaternion.identity;
    public Vector3 RigPistol_StartPosition = Vector3.zero;

    private void Awake()
    {
        if(RigPistol != null)
        {
            RigPistol_StartPosition = RigPistol.transform.localPosition;
            RigPistol_StartRotation = RigPistol.transform.localRotation;
        }
    }
}
