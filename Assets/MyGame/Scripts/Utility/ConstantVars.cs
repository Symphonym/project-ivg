﻿using UnityEngine;
using Steamworks;

public static class ConstantVars{

    public class PlayerTag
    {
        public readonly CSteamID requiredSteamId = CSteamID.Nil;
        public readonly string tagName = "???";
        public readonly Color tagColor = Color.white;
        public PlayerTag(CSteamID requiredSteamId, string tagName, Color tagColor)
        {
            this.requiredSteamId = requiredSteamId;
            this.tagName = tagName;
            this.tagColor = tagColor;
        }
    }

    public static string GameVersion = "1";

    public static AppId_t GameSteamAppId = new AppId_t(664810);
    public static PlayerTag[] GamePlayerTags = new PlayerTag[]
    {
        new PlayerTag(new CSteamID(76561198034063019), "Lead-Dev", Color.red), // Jakob
        new PlayerTag(new CSteamID(76561198112408020), "Dev", Color.cyan), // Karl
        new PlayerTag(new CSteamID(76561198048845907), "Dev", Color.cyan), // Viktor
    };

    // Game - Team names
    public const string GuardTeamName = "Guard";
    public const string InvisibleTeamName = "Invisible";
    public const string SpectatorTeamName = "Spectator";

    // Game - Configuration
    public const float ItemMinStunVelocity = 9f;
    public static Color32 InvisibleColor32 = new Color32(66, 134, 244, 255);
    public static Color32 GuardColor32 = new Color32(231, 56, 56, 255);
    public static Color32 NeutralColor32 = new Color32(242, 242, 101, 255);
    public static Color InvisibleColor = InvisibleColor32;
    public static Color GuardColor = GuardColor32;
    public static Color NeutralColor = NeutralColor32;

    // Photon - Player properties
    public const string PlayerTeam_Key = "Team";
    public const string PlayerReady_Key = "Ready";
    public const string PlayerPing_Key = "Ping";
    public const string PlayerThrows_Key = "Score_Throws";
    public const string PlayerKills_Key = "Score_Kills";
    public const string PlayerLoaded_Key = "Loaded";
    public const string PlayerAlive_Key = "Alive";
    public const string PlayerObject_Key = "Object";
    public const string PlayerSteamXPBackup_Key = "SteamXPBackup";
    public const string PlayerSteamIDBackup_Key = "SteamIDBackup";

    // Photon - Room properties
    //public const string RoomSelectedLevelIndex_Key = "SelectedLevelIndex";
    public const string RoomGameState_Key = "GameState";
    public const string RoomGameStartTime_Key = "GameStartTime";
    public const string RoomItemsScored_Key = "ItemsScored";
    public const string RoomItemsRequired_Key = "ItemsRequired";
    public const string RoomGameEndReason_Key = "EndReason";


    public const string RoomSetting_GameDuration_Key = "RoomSetting_GameDuration";
    public const string RoomSetting_ItemsRequired_Key = "RoomSetting_ItemsRequired";
    public const string RoomSetting_ItemDensity_Key = "RoomSetting_ItemDensity";
    public const string RoomSetting_XPGain_Key = "RoomSetting_XPGain";

    // Photon - Matchmaking
    public const string MatchMaking_SqlLobby_Name = "IVG Game Lobby";

    // Photon - Event codes
    public const byte PhotonEventCode_SendAuthTicket = 10;
    public const byte PhotonEventCode_RequestAuthTicket = 11;

    // Game Input Prevention - Keys
    public const string GIP_Chat_Key = "Chat";
    public const string GIP_GameMenu_Key = "GameMenu";
    public const string GIP_KillCam_Key = "KillCam";

    // Mouse Requirement - Keys
    public const string MR_GameState_Key = "GameState";
    public const string ML_AppFocused_Key = "AppFocused";
    public const string MR_GameMenu_Key = "GameMenu";
    public const string MR_Chat_Key = "Chat";

    // Playerprefs
    public const string PlayerPref_PreferredRegion_Key = "PreferredRegion";

    // Steam - Lobby keys
    public const string SteamLobby_PUNServer_Key = "PUNServer";

    // Steam - Stat API keys
    public const string SteamStats_XP_Key = "PLAYER_XP";
}
