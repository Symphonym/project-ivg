﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class ConsistentSceneLoader : MonoBehaviour {

    public static ConsistentSceneLoader instance;

    public CanvasGroup LoadingScreen;
    public Text LoadingText;
    public Text InfoText;
    public float FadeDuration = 0.5f;

    Action m_targetCallback = null;
    string m_targetScene = null;

    enum LoadStates
    {
        Idle,
        Loading,
        Finished
    }
    LoadStates m_loadState = LoadStates.Idle;

	// Use this for initialization
	void Awake ()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
	}

    void Start()
    {
        LoadingScreen.alpha = 0f;
        LoadingScreen.interactable = false;
        LoadingScreen.blocksRaycasts = false;
    }

    void Update()
    {
        if(m_loadState == LoadStates.Finished)
        {

            /*PhotonNetwork.CacheSendMonoMessageTargets(typeof(ConnectionMenuInput));
            PhotonNetwork.CacheSendMonoMessageTargets(typeof(CreateRoomMenuInput));
            PhotonNetwork.CacheSendMonoMessageTargets(typeof(ServerListInput));
            PhotonNetwork.CacheSendMonoMessageTargets(typeof(ChatMenuInput));
            PhotonNetwork.CacheSendMonoMessageTargets(typeof(LobbyMenuInput));
            PhotonNetwork.CacheSendMonoMessageTargets(typeof(LobbyTopMenuInput));
            PhotonNetwork.CacheSendMonoMessageTargets(typeof(ScoreboardInput));
            PhotonNetwork.CacheSendMonoMessageTargets(typeof(GameControl));
            PhotonNetwork.CacheSendMonoMessageTargets(typeof(StartMenuInput));
            PhotonNetwork.CacheSendMonoMessageTargets(typeof(OwnershipControl));*/
            //Debug.Log("WAT");
            //foreach (GameObject g in PhotonNetwork.SendMonoMessageTargets)
            //    Debug.Log(g.name);

            m_loadState = LoadStates.Idle;
            LoadingScreen.blocksRaycasts = false;

            // Call the callback when the new scene has loaded
            if (m_targetCallback != null)
                m_targetCallback();

            m_targetScene = null;
            m_targetCallback = null;

            // Unpause network message queue when scene is loaded
            PhotonNetwork.isMessageQueueRunning = true;

        }
    }

    public void LoadScene(string sceneName, string infoText = "")
    {
        LoadScene(sceneName, null, infoText);
    }
    public void LoadScene(string sceneName, Action callback, string infoText = "")
    {
        if (m_loadState != LoadStates.Idle)
        {
            Debug.LogWarning("Can't load two scenes at once!");
            return;
        }

        if(PhotonNetwork.inRoom)
            PhotonNetwork.DestroyPlayerObjects(PhotonNetwork.player);

        m_targetCallback = callback;
        m_targetScene = sceneName;
        
        // Pause network message queue when level is loading
        PhotonNetwork.isMessageQueueRunning = false;


        LoadingScreen.blocksRaycasts = true;
        m_loadState = LoadStates.Loading;

        LoadingText.text = "Loading 0%";
        InfoText.text = infoText;

        StartCoroutine(CR_Loading());
    }

    IEnumerator CR_Fade(bool fadeIn)
    {
        // Offset elapsed time so we start at 0
        float elapsed = -Time.deltaTime;

        LoadingText.gameObject.SetActive(fadeIn);

        do
        {
            elapsed += Time.deltaTime;
            float ratio = Mathf.Clamp01(elapsed / FadeDuration);

            LoadingScreen.alpha = fadeIn ? ratio : (1f - ratio);

            yield return null;
        } while (elapsed < FadeDuration);

    }
    IEnumerator CR_Loading()
    {
        yield return CR_Fade(true);

        AsyncOperation op = SceneManager.LoadSceneAsync(m_targetScene);
        op.allowSceneActivation = false;

        while (!op.isDone)
        {
            if(op.progress < 0.9f)
                LoadingText.text = "Loading " + Mathf.RoundToInt(Mathf.Clamp01(op.progress / 0.9f) * 100f) + "%";
            else
            {
                LoadingText.text = "Initializing map";
                op.allowSceneActivation = true;
            }
            yield return null;
        }

        // Make sure async is done
        yield return op;

        // Fade back in
        yield return CR_Fade(false);

        m_loadState = LoadStates.Finished;
        yield break;
    }


    public bool IsLoadingScene()
    {
        return m_loadState != LoadStates.Idle;
    }
    public string CurrentScene
    {
        get
        {
            return SceneManager.GetActiveScene().name;
        }
    }
}
