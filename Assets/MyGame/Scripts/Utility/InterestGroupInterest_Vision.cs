﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterestGroupInterest_Vision : InterestGroupInterest_Base {

    [Header("Interest generation method")]
    [Tooltip("If this renderer is visible, it generates interest")]
    public Renderer CullingTarget;

    [Tooltip("If the interest is closer than this distance to the main camera, it becomes interesting regardless of visibility")]
    public float CameraDistanceOverride = 10f;

    protected override void Start()
    {
        base.Start();
    }

    public override bool IsInteresting()
    {
        float distanceToMainCamera = Camera.main != null ? Vector3.Distance(Camera.main.transform.position, transform.position) : Mathf.Infinity;

        return
            (CullingTarget != null && CullingTarget.isVisible) ||
            (Camera.main != null && distanceToMainCamera <= CameraDistanceOverride);
    }

}
