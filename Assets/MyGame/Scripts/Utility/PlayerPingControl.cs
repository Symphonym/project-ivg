﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPingControl : MonoBehaviour {

    public float PingUpdateDelay = 2f;
    float m_pingElapsed = 0f;

	// Use this for initialization
	void Start () {
        DontDestroyOnLoad(gameObject);
	}

    void Update()
    {
        m_pingElapsed += Time.deltaTime;
        if(m_pingElapsed >= PingUpdateDelay)
        {
            m_pingElapsed = 0;

            if (PhotonNetwork.connected)
            {
                ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
                hashtable[ConstantVars.PlayerPing_Key] = PhotonNetwork.GetPing();
                PhotonNetwork.player.SetCustomProperties(hashtable);
            }
        }
    }
}
