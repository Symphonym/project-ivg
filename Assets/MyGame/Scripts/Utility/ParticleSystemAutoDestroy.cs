﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleSystemAutoDestroy : MonoBehaviour {

    ParticleSystem m_ps;

	// Use this for initialization
	void Start () {
        m_ps = GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
        if (m_ps != null && !m_ps.IsAlive())
            Destroy(gameObject);
	}
}
