﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BitSet
{
    int m_bitset = 0;
    int bitset
    {
        get { return m_bitset; }
        set
        {
            m_bitset = value;
            m_asByte = (byte)m_bitset;
        }
    }
    byte m_asByte = 0;

    public void SetBit(int position, bool val)
    {
        // Bit already set
        if ((bitset & (1 << position)) != 0 && !val)
            bitset &= ~(1 << position);

        // Bit not set
        else if ((bitset & (1 << position)) == 0 && val)
            bitset |= (1 << position);
    }

    public bool GetBit(int position)
    {
        return (bitset & (1 << position)) != 0;
    }

    public void FromByte(byte newBitset)
    {
        bitset = newBitset;
    }
    public byte AsByte()
    {
        return m_asByte;
    }
}
