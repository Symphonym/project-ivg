﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollSceneTest : MonoBehaviour {

    public GameObject RagdollPrefab;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Vector3 spawnPos = Camera.main.transform.position + ray.direction * 5f;

            Vector3 dirToCam = Camera.main.transform.position - spawnPos;
            dirToCam.Normalize();

            GameObject ragdoll = Instantiate(RagdollPrefab, spawnPos, Quaternion.LookRotation(-dirToCam));
            ragdoll.GetComponent<RagdollShooter>().Launch(-dirToCam * 50f);
        }
    }
}
