﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(CharacterController))]
public class PlayerFootstep : MonoBehaviour {

    public float StepDist = 0.2f;
    public float MinPitch = 1f;
    public float MaxPitch = 1f;
    public bool PlayFootstepSound = true;

    public FootprintFader FootprintPrefab;

    public AudioSource FootstepSound;

    bool m_leftPrint = false;

    float m_distWalked = 0f;
    Vector3 m_lastPosition = Vector3.zero;

    PlayerMovement m_movement;
    PhotonView m_photonView;
    CharacterController m_char;

    private void Awake()
    {
        m_photonView = PhotonView.Get(this);
    }
    void Start () {
        m_movement = GetComponent<PlayerMovement>();
        m_char = GetComponent<CharacterController>();

        m_lastPosition = transform.position;
	}
	
	void Update ()
    {
        if (m_movement == null || m_char == null || m_movement.IsCrouching())
            return;


        Vector3 travelled = transform.position - m_lastPosition;
        travelled.y = 0f; // Ignore vertical distance moved
        float distWalked = travelled.magnitude;

        m_distWalked += distWalked;

        if (m_distWalked >= StepDist)
        {
            // Only make a footstep if we're grounded
            if (m_movement.GetPlayerMoveState() == PlayerMovement.PlayerMoveStates.Grounded)
            {
                // Explicitly reset distance walked to avoid sound being played repeatedly
                m_distWalked = 0;

                MakeFootstep();
            }
                
        }

        m_lastPosition = transform.position;
	}

    void MakeFootstep()
    {
        if (PlayFootstepSound)
        {
            FootstepSound.pitch = Random.Range(MinPitch, MaxPitch);
            FootstepSound.Play();
        }

        // Raycast downwards to find the floor, then spawn a footprint along the surface of the floor
        RaycastHit hit;
        if (Physics.Raycast(m_char.transform.position + m_char.center, -m_char.transform.up, out hit, (m_char.height / 2f) + 2f, LayerMask.GetMask("Environment")))
        {
            Quaternion rot = Quaternion.LookRotation(-hit.normal, m_char.transform.forward);
            FootprintFader footprint = Instantiate(FootprintPrefab, hit.point + hit.normal * 0.01f, rot);

            // Alternate left and right footprints
            if (m_leftPrint)
                footprint.RightPrint.gameObject.SetActive(false);
            else
                footprint.LeftPrint.gameObject.SetActive(false);

            m_leftPrint = !m_leftPrint;
        }
    }


    // Forcefully trigger a footstep
    public void ForceStep()
    {
        if (m_photonView != null && m_photonView.isMine)
            m_photonView.RPC("ForceStepRPC", PhotonTargets.All);
    }

    [PunRPC]
    public void ForceStepRPC(PhotonMessageInfo info)
    {
        PhotonPlayer owner = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;
        if (info.sender != owner)
            return;

        MakeFootstep();
    }
}
