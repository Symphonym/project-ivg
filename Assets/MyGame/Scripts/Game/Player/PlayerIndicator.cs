﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PhotonView))]
public class PlayerIndicator : MonoBehaviour {

    public Text PlayerNameText;
    public PingIndicator PingIndicatorPrefab;
    public AudioSource PingSound;
    public AudioSource TauntSound;

    public int LimitDuration = 10;
    public int PingsPerLimit = 5;

    PhotonView m_photonView;
    PhotonPlayer m_owner;

    int m_pingCount = 0;
    float m_elapsedLimit = 0f;
    bool m_resettingPing = false;

    KeybindControl.Keybind m_pingKeybind;
    KeybindControl.Keybind m_positionPingKeybind;
    KeybindControl.Keybind m_tauntKeybind;

    private void Awake()
    {
        m_photonView = PhotonView.Get(this);
    }

    // Use this for initialization
    void Start()
    {
        m_owner = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;
        m_pingKeybind = KeybindControl.GetKeybind("In-Game Ping");
        m_positionPingKeybind = KeybindControl.GetKeybind("In-Game Position Ping");
        m_tauntKeybind = KeybindControl.GetKeybind("Taunt");
    }
	
    void Update()
    {
        if(GameControl.instance.GameInputAllowed())
        {
            if(m_pingKeybind.GetDown())
                SendPing(false);
            if (m_positionPingKeybind.GetDown())
                SendPing(true);
            if (m_tauntKeybind.GetDown())
                Taunt();

        }

        UpdatePlayerText();
    }

    // Update is called once per frame
    void UpdatePlayerText()
    {
        if (PlayerNameText == null || Camera.main == null)
            return;

        // Ignore self
        if (m_owner == PhotonNetwork.player)
        {
            PlayerNameText.gameObject.SetActive(false);
            return;
        }

        string localPlayerTeam = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
        bool isTeammate = Utility.GetPhotonPlayerProp<string>(m_owner, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName) == localPlayerTeam;
        bool isSpectator = localPlayerTeam == ConstantVars.SpectatorTeamName;
        bool isDead = !Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerAlive_Key, true);

        bool allowPlayerName = isTeammate || isSpectator || isDead;

        PlayerNameText.gameObject.SetActive(allowPlayerName);

        if (allowPlayerName)
        {
            PlayerNameText.text = Utility.ParseChatPlayerName(m_owner);
            // Make player text face camera
            //Vector3 lookDir = PlayerNameText.transform.position - Camera.main.transform.position;
            // PlayerNameText.transform.rotation = Quaternion.LookRotation(lookDir);

        }

    }

    void Taunt()
    {
        if (!m_photonView.isMine)
            return;

        m_photonView.RPC("OnTauntRPC", PhotonTargets.All);
    }

    [PunRPC]
    void OnTauntRPC(PhotonMessageInfo info)
    {
        if (info.sender != m_photonView.GetPhotonOwner())
            return;

        if (TauntSound != null)
            TauntSound.Play();
    }

    IEnumerator CR_ResetPingDelay()
    {
        m_elapsedLimit = -Time.deltaTime;
        do
        {
            m_elapsedLimit += Time.deltaTime;
            yield return null;
        } while (m_elapsedLimit < LimitDuration);

        m_pingCount = 0;
        m_resettingPing = false;
    }

    void SendPing(bool positionPing)
    {
        // Only owning client can ping for this player
        if (!m_photonView.isMine)
            return;

        if(m_pingCount >= PingsPerLimit)
        {
            GameUIControl.instance.Chat.SendLocalChatMessage("You've sent too many pings, wait " + Mathf.RoundToInt(LimitDuration - m_elapsedLimit).ToString() + " seconds");
            return;
        }

        if (!m_resettingPing)
        {
            m_resettingPing = true;
            StartCoroutine(CR_ResetPingDelay());
        }

        ChatMenuInput.ChatMessageTypes chatMessageType = ChatMenuInput.ChatMessageTypes.Ping;
        Vector3 pingPosition = transform.position - transform.forward * 0.4f;
        string pingRoomName = RoomBoundaryControl.instance.GetCurrentRoomName();

        if (positionPing)
        {
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 100f, LayerMask.GetMask("Environment", "GameItem", "PlayerHitbox")))
            {
                chatMessageType = ChatMenuInput.ChatMessageTypes.PositionPing;
                pingPosition = hit.point;
                pingRoomName = RoomBoundaryControl.instance.GetRoomAt(pingPosition);
            }

            // We couldn't look onto a position, so we abort the ping
            else
                return;
        }

        ++m_pingCount;

        string myTeam = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            string theirTeam = Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
            if (myTeam.Equals(theirTeam))
                m_photonView.RPC("PingRPC", player, pingPosition, chatMessageType == ChatMenuInput.ChatMessageTypes.PositionPing);
        }
        GameUIControl.instance.Chat.SendChatMessage(pingRoomName, chatMessageType);

        // Play sound locally on the pinger
        PingSound.Play();
    }

    [PunRPC]
    void PingRPC(Vector3 position, bool positionPing, PhotonMessageInfo info)
    {
        PingIndicator ping = Instantiate(PingIndicatorPrefab, position, Quaternion.identity);

        ping.ArrowColor = positionPing ? (Color)(new Color32(67, 127, 229, 255)) : new Color(1f, 1f, 0);
        ping.InfoString = Utility.ParseChatPlayerName(info.sender);

        // Sound is played locally on owner
        if (!m_photonView.isMine)
            PingSound.Play();
    }
}
