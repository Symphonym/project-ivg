﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimator : MonoBehaviour {

    public float SpeedSmoothing = 10f;
    public float VerticalSpeedSmoothing = 10f;
    public float ForwardMovementSmoothing = 10f;
    public float HorizontalMovementSmoothing = 10f;
    public float AimingSmoothing = 10f;


    [HideInInspector]
    public Animator PlayerAnim;
    [HideInInspector]
    public List<Renderer> PlayerRenderers;
    [HideInInspector]
    public PlayerModel Model;

    [SerializeField]
    float m_modelYOffset = -1f;

    int m_animSpeedId = Animator.StringToHash("Speed");
    int m_animVerticalSpeed = Animator.StringToHash("VerticalSpeed");
    int m_animShootId = Animator.StringToHash("Shoot");
    int m_animForwardDirId = Animator.StringToHash("ForwardDirection");
    int m_animHorizontalDirId = Animator.StringToHash("HorizontalDirection");
    int m_animCrouchingId = Animator.StringToHash("Crouching");
    int m_animAimId = Animator.StringToHash("AimDirection");
    int m_animStunId = Animator.StringToHash("Stunned");
    int m_animGroundedId = Animator.StringToHash("Grounded");
    int m_animPunchId = Animator.StringToHash("Punch");
    int m_animReloadId = Animator.StringToHash("Reload");
    int m_animHoldingItemId = Animator.StringToHash("HoldingItem");
    int m_animThrowItemId = Animator.StringToHash("ThrowItem");

    PlayerMovement m_mov;
    FPSCamera m_fpsCamera;
    FPSRigData m_fpsRig;

    PhotonView m_photonView;

    Vector3 m_lastPos = Vector3.zero;

    private void Awake()
    {
        m_mov = GetComponent<PlayerMovement>();
        m_fpsCamera = GetComponent<FPSCamera>();

        m_photonView = PhotonView.Get(this);

        int levelIndex = Utility.GetPhotonRoomProp<int>("C0", 0);
        if (levelIndex < 0)
        {
            Debug.LogError("Invalid level index, failed to spawn player model");
            return;
        }

        // Spawn player model (this might vary per level, so we check which level it is and then spawn the correct model0
        Levels.LevelData level = Levels.instance.All[levelIndex];
        PhotonPlayer owner = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;
        string playerTeam = Utility.GetPhotonPlayerProp<string>(owner, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
        PlayerModel playerModelPrefab = playerTeam == ConstantVars.GuardTeamName ? level.GuardModelPrefab : level.InvisibleModelPrefab;

        PlayerModel playerModel = Instantiate(playerModelPrefab, transform, false);
        playerModel.transform.localPosition = new Vector3(0, m_modelYOffset, 0);
        Model = playerModel;
        PlayerAnim = playerModel.ModelAnimator;
        PlayerRenderers = playerModel.ModelRenderers;

        if(m_photonView.isMine)
        {
            if (playerTeam == ConstantVars.GuardTeamName && level.GuardFPSRig != null)
                m_fpsRig = Instantiate(level.GuardFPSRig, m_fpsCamera.TargetCamera.transform, false);
            else if (playerTeam == ConstantVars.InvisibleTeamName && level.GuardFPSRig != null)
                m_fpsRig = Instantiate(level.InvisibleFPSRig, m_fpsCamera.TargetCamera.transform, false);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        Vector3 speed = (transform.position - m_lastPos) / Time.deltaTime;
        m_lastPos = transform.position;
        Vector3 movementDirection = transform.InverseTransformDirection(speed).normalized;

        float curVert = PlayerAnim.GetFloat(m_animAimId);
        float newVert = Mathf.Lerp(curVert, -1f + Mathf.Clamp01((m_fpsCamera.GetYRotation() - m_fpsCamera.MinimumY) / (m_fpsCamera.MaximumY - m_fpsCamera.MinimumY)) * 2f, Time.deltaTime * AimingSmoothing);
        PlayerAnim.SetFloat(m_animAimId, newVert);

        PlayerAnim.SetBool(m_animStunId, m_mov.IsStunned());

        PlayerAnim.SetBool(m_animGroundedId, m_mov.GetPlayerMoveState() == PlayerMovement.PlayerMoveStates.Grounded);
        PlayerAnim.SetBool(m_animCrouchingId, m_mov.IsCrouching());

        PlayerAnim.SetFloat(m_animSpeedId, Mathf.Lerp(PlayerAnim.GetFloat(m_animSpeedId), new Vector2(speed.x, speed.z).magnitude, Time.deltaTime * SpeedSmoothing));
        PlayerAnim.SetFloat(m_animVerticalSpeed, Mathf.Lerp(PlayerAnim.GetFloat(m_animVerticalSpeed), speed.y, Time.deltaTime * VerticalSpeedSmoothing));
        PlayerAnim.SetFloat(m_animForwardDirId, Mathf.Lerp(PlayerAnim.GetFloat(m_animForwardDirId), Mathf.Clamp(movementDirection.z, -1f, 1f), Time.deltaTime * ForwardMovementSmoothing));
        PlayerAnim.SetFloat(m_animHorizontalDirId, Mathf.Lerp(PlayerAnim.GetFloat(m_animHorizontalDirId), Mathf.Clamp(movementDirection.x, -1f, 1f), Time.deltaTime * HorizontalMovementSmoothing));
    }

    public void TriggerShot()
    {
        PlayerAnim.SetTrigger(m_animShootId);

        if (m_fpsRig != null)
            m_fpsRig.RigAnimator.SetTrigger(m_animShootId);
    }

    public void TriggerPunch()
    {
        PlayerAnim.SetTrigger(m_animPunchId);

        if (m_fpsRig != null)
            m_fpsRig.RigAnimator.SetTrigger(m_animPunchId);
    }

    public void TriggerReload()
    {
        if (m_fpsRig != null)
            m_fpsRig.RigAnimator.SetTrigger(m_animReloadId);
    }

    public void TriggerThrowItem()
    {
        PlayerAnim.SetTrigger(m_animThrowItemId);

        if (m_fpsRig != null)
            m_fpsRig.RigAnimator.SetTrigger(m_animThrowItemId);
    }

    public void SetHoldingItem(bool holdingItem)
    {
        PlayerAnim.SetBool(m_animHoldingItemId, holdingItem);

        if (m_fpsRig != null)
            m_fpsRig.RigAnimator.SetBool(m_animHoldingItemId, holdingItem);
    }

    public FPSRigData GetFPSRig()
    {
        return m_fpsRig;
    }
    public bool HasFPSRig()
    {
        return m_fpsRig != null;
    }
    public bool HasFPSPistol()
    {
        return HasFPSRig() && m_fpsRig.RigPistol != null;
    }
}
