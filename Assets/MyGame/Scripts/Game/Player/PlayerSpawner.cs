﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour {

    public static string SelectedInvisibleSpawn = null;
    public static PlayerSpawner instance;

    public GameObject SpectatorPlayerPrefab;

    bool m_isNetworkedObj = false;
    GameObject m_playerObj = null;

	void Awake () {
        instance = this;
	}

    public void SpawnDefault()
    {
        string teamName = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);

        if (teamName == ConstantVars.SpectatorTeamName)
            SpawnSpectator();
        else if (teamName == ConstantVars.InvisibleTeamName)
            SpawnInvisible();
        else if (teamName == ConstantVars.GuardTeamName)
            SpawnGuard();
    }
    public void SpawnSpectator()
    {
        if (HasPlayerObject())
            DestroyPlayerObject();

        GameObject[] spawns = GameObject.FindGameObjectsWithTag("TeamSpectator_Spawn");
        if (spawns.Length > 0)
        {
            // Spectators are only created locally since they don't need to be synchronized
            GameObject spawnObj = spawns[Random.Range(0, spawns.Length)];
            m_playerObj = Instantiate(SpectatorPlayerPrefab, spawnObj.transform.position, spawnObj.transform.rotation);

            ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
            hashtable[ConstantVars.PlayerObject_Key] = -1;
            PhotonNetwork.player.SetCustomProperties(hashtable);

            m_isNetworkedObj = false;
            GameEvents.instance.Trigger("OnSpawnSpectator");
        }
    }
    public void SpawnInvisible()
    {
        if (HasPlayerObject())
            DestroyPlayerObject();

        //GameObject[] spawns = GameObject.FindGameObjectsWithTag("TeamInvisible_Spawn");
        //if (spawns.Length > 0)
        //{
        GameObject spawn = null;
        if (SelectedInvisibleSpawn != null)
            spawn = GameObject.Find(SelectedInvisibleSpawn);

        if (spawn != null)
        {
            // Spectators are only created locally since they don't need to be synchronized
            //GameObject spawnObj = spawns[Random.Range(0, spawns.Length)];
            m_playerObj = PhotonNetwork.Instantiate("InvisiblePlayer", spawn.transform.position, spawn.transform.rotation, 0);
            m_isNetworkedObj = true;

            ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
            hashtable[ConstantVars.PlayerObject_Key] = m_playerObj.GetComponent<PhotonView>().viewID;
            PhotonNetwork.player.SetCustomProperties(hashtable);

            GameEvents.instance.Trigger("OnSpawnInvisible");
        }
        //}
    }
    public void SpawnGuard()
    {
        if (HasPlayerObject())
            DestroyPlayerObject();

        GameObject[] spawns = GameObject.FindGameObjectsWithTag("TeamGuard_Spawn");
        if (spawns.Length > 0)
        {
            // Spectators are only created locally since they don't need to be synchronized
            GameObject spawnObj = spawns[Random.Range(0, spawns.Length)];
            m_playerObj = PhotonNetwork.Instantiate("GuardPlayer", spawnObj.transform.position, spawnObj.transform.rotation, 0);
            m_isNetworkedObj = true;

            ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
            hashtable[ConstantVars.PlayerObject_Key] = m_playerObj.GetComponent<PhotonView>().viewID;
            PhotonNetwork.player.SetCustomProperties(hashtable);

            GameEvents.instance.Trigger("OnSpawnGuard");
        }
    }

    public void DestroyPlayerObject()
    {
        if (!HasPlayerObject())
            return;

        if (m_isNetworkedObj)
            PhotonNetwork.Destroy(m_playerObj);
        else
            Destroy(m_playerObj);

        m_playerObj = null;
    }

    // True if this client has a player object it controls
    public bool HasPlayerObject()
    {
        return m_playerObj != null;
    }

    public GameObject GetPlayerObject()
    {
        return m_playerObj;
    }
}
