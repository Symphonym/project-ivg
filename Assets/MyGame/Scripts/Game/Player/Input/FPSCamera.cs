﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSCamera : MonoBehaviour, IPunObservable {

    public Camera TargetCamera;

    public float MaximumY = 80f;
    public float MinimumY = -80f;

    public float BobFreq = 10f;
    public float BobAmp = 1f;
    public float BobReturnSpeed = 5f;
    public float BobWeaveAngle = 45f;
    public float BobWeaveReturnSpeed = 5f;

    public bool NotNetworked = false;

    float m_rotationY = 0f;

    PhotonView m_photonView;

    Vector3 m_cameraStartLocalPos;
    PlayerMovement m_mov;

    bool m_isSpectator = false;

    private void Awake()
    {
        m_photonView = PhotonView.Get(this);
        m_isSpectator = GetComponent<SpectatorMovement>() != null;
    }

    // Use this for initialization
    void Start () {

        m_cameraStartLocalPos = TargetCamera.transform.localPosition;
        m_mov = GetComponent<PlayerMovement>();

        // Disable camera for all other players
        if(!NotNetworked)
        {
            TargetCamera.gameObject.tag = (m_photonView != null && m_photonView.isMine) ? "MainCamera" : "Untagged";
            TargetCamera.gameObject.SetActive(m_photonView != null && m_photonView.isMine);
        }
    }

    // Update is called once per frame
    void Update ()
    {
        // Just make sure the camera position stays in sync on remote clients, because the flashlight makes use of the camera position
        if(m_photonView != null && !m_photonView.isMine)
            TargetCamera.transform.localPosition = Vector3.Lerp(TargetCamera.transform.localPosition, m_mov.GetDesiredCameraPos(), Time.deltaTime * BobReturnSpeed);

        if ((m_photonView == null || !m_photonView.isMine) && !NotNetworked)
            return;

        if (!GameControl.instance.GameInputAllowed_IgnoreState())
            return;

        float mouseSens = OptionsControl.options.mouseSensitivity;

        float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * mouseSens;

        m_rotationY += Input.GetAxis("Mouse Y") * mouseSens;
        m_rotationY = Mathf.Clamp(m_rotationY, MinimumY, MaximumY);


        // X rotation is applied on the whole character
        transform.localEulerAngles = new Vector3(0, rotationX, 0);


        // If we're a spectator, ignore all the weave and headbob logic
        if (m_isSpectator)
        {
            // Y rotation is applied only on the camera
            TargetCamera.transform.localEulerAngles = new Vector3(-m_rotationY, 0, 0);
        }
        else
        {
            // Sidestep camera weave
            float targetRoll = -m_mov.GetSideInput() * BobWeaveAngle;

            // Y rotation is applied only on the camera
            TargetCamera.transform.localEulerAngles =
                new Vector3(
                    -m_rotationY,
                    0,
                    Mathf.LerpAngle(TargetCamera.transform.localEulerAngles.z, targetRoll, Time.deltaTime * BobWeaveReturnSpeed));

            // Head bobbing
            bool disableBob = m_mov.GetPlayerMoveState() == PlayerMovement.PlayerMoveStates.Airborne;
            Vector3 targetPos = new Vector3(m_cameraStartLocalPos.x, m_mov.GetDesiredCameraPos().y + Mathf.Sin(Time.time * BobFreq * Mathf.Round(m_mov.GetRealSpeed().magnitude)) * BobAmp * (disableBob ? 0f : 1f), m_cameraStartLocalPos.z);
            TargetCamera.transform.localPosition = Vector3.Lerp(TargetCamera.transform.localPosition, targetPos, Time.deltaTime * BobReturnSpeed);
        }

    }

    public void AimPunch(float verticalPunch)
    {
        m_rotationY += verticalPunch;
    }

    public void RotateTowards(Vector3 target)
    {
        Quaternion lookAtRot = Quaternion.LookRotation((target - TargetCamera.transform.position).normalized);

        // Vertical angle should now go between 0 to 180 and -0 to -180 instead of 0 to 360
        float yRot = lookAtRot.eulerAngles.x > 180 ? -(360f - lookAtRot.eulerAngles.x) : lookAtRot.eulerAngles.x;
        //m_rotationY = Mathf.Clamp(-lookAtRot.eulerAngles.x, MinimumY, MaximumY);
        m_rotationY = Mathf.Clamp(-yRot, MinimumY, MaximumY);

        TargetCamera.transform.localEulerAngles = new Vector3(-m_rotationY, 0, 0);
        transform.localEulerAngles = new Vector3(0, lookAtRot.eulerAngles.y, 0);
    }

    public float GetYRotation()
    {
        return m_rotationY;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // Synchronize Y rotation, since only the camera is moved up and down and not
        // the entire object/player.
        if(stream.isWriting)
            stream.SendNext(m_rotationY);
        else if (stream.isReading)
            m_rotationY = (float)stream.ReceiveNext();
    }
}
