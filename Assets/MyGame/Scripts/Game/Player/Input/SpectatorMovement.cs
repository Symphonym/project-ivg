﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpectatorMovement : MonoBehaviour {

    public Camera TargetCamera;

    public float MaxSpeed = 0.1f;
    public float SprintMaxSpeed = 0.2f;
    public float Acceleration = 2f;
    public float Drag = 0.9f;

    public float KillCamDuration = 2f;

    Vector3 m_speed = new Vector3(0, 0, 0);

    [SerializeField]
    Light m_flashLight;

    [SerializeField]
    AudioSource m_flashLightSound;

    KeybindControl.Keybind m_moveForward;
    KeybindControl.Keybind m_moveBackward;
    KeybindControl.Keybind m_moveRight;
    KeybindControl.Keybind m_moveLeft;
    KeybindControl.Keybind m_jump;
    KeybindControl.Keybind m_camDown;
    KeybindControl.Keybind m_camSprint;
    KeybindControl.Keybind m_flashlightBind;

    FPSCamera m_cam;

    SpectatorCamText m_spectatorText;

    enum CameraModes
    {
        KillCam,
        FollowPlayer,
        FreeCam
    }

    CameraModes m_cameraMode = CameraModes.FreeCam;

    PhotonPlayer m_targetPlayer = null;
    PhotonView m_targetObject = null;

    // FollowPlayer mode data
    enum SpectatorRefreshModes
    {
        Up,
        Down,
        Stay
    }

    float m_spectatingHorAngle = 0f;
    float m_spectatingVerAngle = 0f;
    float m_spectatingDistance = 5f;

    void Awake()
    {
        // This is to ensure spectator text is accessable immediately after spawning player (such as when going from Invisible to Spectator)
        // Specifically the call to "SetKillCam", so that we can update spectator text in that function call
        if(GameUIControl.instance != null)
            m_spectatorText = GameUIControl.instance.GetUIObject<SpectatorCamText>("SpectatorCam_Txt");
    }


    void Start()
    {
        m_moveForward = KeybindControl.GetKeybind("Move Forward");
        m_moveBackward = KeybindControl.GetKeybind("Move Backward");
        m_moveRight = KeybindControl.GetKeybind("Move Right");
        m_moveLeft = KeybindControl.GetKeybind("Move Left");
        m_jump = KeybindControl.GetKeybind("Jump");
        m_camDown = KeybindControl.GetKeybind("Camera Down");
        m_camSprint = KeybindControl.GetKeybind("Camera Sprint");
        m_flashlightBind = KeybindControl.GetKeybind("Flashlight");

        m_cam = GetComponent<FPSCamera>();

        m_spectatorText = GameUIControl.instance.GetUIObject<SpectatorCamText>("SpectatorCam_Txt");
        GameUIControl.instance.SetUIObjectValidator("SpectatorCam_Txt", () =>
        {
            return
                !GameControl.instance.IsGameState(GameControl.GameStates.Lobby) &&
                m_cameraMode != CameraModes.FreeCam;
        });

        GameUIControl.instance.ForceRefreshAllUIObjects();
    }

    void FixedUpdate()
    {
        // Some simple drag to slow down the player when no inputs are given
        m_speed *= Drag;
    }
	// Update is called once per frame
	void Update ()
    {
        if (!PhotonNetwork.inRoom)
            return;

        bool changedMode = false;

        if (m_flashlightBind.GetDown() && GameControl.instance.GameInputAllowed())
        {
            m_flashLight.enabled = !m_flashLight.enabled;
            m_flashLightSound.Play();
        }

        // Change Camera mode with Scroll click
        if (Input.GetMouseButtonDown(2) && GameControl.instance.GameInputAllowed())
        {
            switch (m_cameraMode)
            {
                case CameraModes.KillCam: break;

                case CameraModes.FollowPlayer:
                    m_cameraMode = CameraModes.FreeCam;
                    changedMode = true;
                    break;

                case CameraModes.FreeCam:
                    m_cameraMode = CameraModes.FollowPlayer;
                    changedMode = true;
                    break;
            }
        }

        switch(m_cameraMode)
        {
            case CameraModes.KillCam:
                UpdateKillCam();
                break;

            case CameraModes.FollowPlayer:
                UpdateFollowPlayer();
                break;

            case CameraModes.FreeCam:
                UpdateFreeCam();
                break;
        }

        if(changedMode)
            GameUIControl.instance.ForceRefreshUIObject("SpectatorCam_Txt");
    }

    void UpdateSpectatorText()
    {
        if (m_cameraMode == CameraModes.FollowPlayer)
            m_spectatorText.TitleText.text = "You are spectating";
        else if (m_cameraMode == CameraModes.KillCam)
            m_spectatorText.TitleText.text = "You were killed by";

        if (m_targetObject == null)
        {
            m_spectatorText.SubTitleText.text = "<color=#" + ColorUtility.ToHtmlStringRGB(ConstantVars.NeutralColor) + ">???</color>";
            return;
        }

        Color teamColor = ConstantVars.NeutralColor;
        string teamName = Utility.GetPhotonPlayerProp<string>(m_targetPlayer, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
        if (teamName == ConstantVars.InvisibleTeamName)
            teamColor = ConstantVars.InvisibleColor;
        else if (teamName == ConstantVars.GuardTeamName)
            teamColor = ConstantVars.GuardColor;

        m_spectatorText.SubTitleText.text =
            "<color=#" + ColorUtility.ToHtmlStringRGB(teamColor) + ">" +
            Utility.RemoveRichTextTags(m_targetPlayer.NickName) +
            "</color>";

        GameUIControl.instance.ForceRefreshUIObject("SpectatorCam_Txt");
    }

    void SetSpectatingPlayer(PhotonPlayer player)
    {
        m_targetPlayer = player;
        int objectId = Utility.GetPhotonPlayerProp<int>(m_targetPlayer, ConstantVars.PlayerObject_Key, -1);
        m_targetObject = PhotonView.Find(objectId);
    }

    // Finds another suitable player to spectate in the specified direction (relative to player list)
    void RefreshSpectator(SpectatorRefreshModes mode)
    {
        if (m_cameraMode != CameraModes.FollowPlayer)
            return;

        if(mode != SpectatorRefreshModes.Stay)
        {
            int referenceIndex = -1;
            for (int i = 0; i < PhotonNetwork.otherPlayers.Length; i++)
            {
                if (PhotonNetwork.otherPlayers[i] == m_targetPlayer || m_targetObject == null)
                {
                    referenceIndex = i;
                    break;
                }
            }

            // No players to spectate
            if (referenceIndex < 0)
            {
                m_cameraMode = CameraModes.FreeCam;
                GameUIControl.instance.ForceRefreshUIObject("SpectatorCam_Txt");
                return;
            }

            int index = 0;
            while (index < PhotonNetwork.otherPlayers.Length)
            {
                switch (mode)
                {
                    case SpectatorRefreshModes.Up: ++referenceIndex; break;
                    case SpectatorRefreshModes.Down: --referenceIndex; break;
                    case SpectatorRefreshModes.Stay: break;
                }

                if (referenceIndex >= PhotonNetwork.otherPlayers.Length)
                    referenceIndex = 0;
                else if (referenceIndex < 0)
                    referenceIndex = PhotonNetwork.otherPlayers.Length - 1;

                PhotonPlayer curPlayer = PhotonNetwork.otherPlayers[referenceIndex];
                string teamName = Utility.GetPhotonPlayerProp<string>(curPlayer, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
                bool alive = Utility.GetPhotonPlayerProp<bool>(curPlayer, ConstantVars.PlayerAlive_Key, true);

                // An ALIVE INVISIBLE or a GUARD can be spectated
                if ((teamName == ConstantVars.InvisibleTeamName && alive) || teamName == ConstantVars.GuardTeamName)
                {
                    SetSpectatingPlayer(curPlayer);
                    break;
                }

                ++index;
            }

            // No players suitable for spectating
            if (m_targetObject == null)
            {
                m_cameraMode = CameraModes.FreeCam;
                GameUIControl.instance.ForceRefreshUIObject("SpectatorCam_Txt");
                return;
            }
        }
        
        UpdateSpectatorText();
    }

    public void SetKillCam(PhotonPlayer killer)
    {
        SetSpectatingPlayer(killer);

        m_cameraMode = CameraModes.KillCam;

        UpdateSpectatorText();

        GameControl.instance.SetGameInputPreventionKey(ConstantVars.GIP_KillCam_Key, false);
        StartCoroutine(CR_KillCam());
    }

    void UpdateKillCam()
    {
        if (m_cam.enabled)
            m_cam.enabled = false;

        if (m_targetObject == null)
            return;

        m_cam.RotateTowards(m_targetObject.transform.position);
    }

    void UpdateFollowPlayer()
    {
        if (m_cam.enabled)
            m_cam.enabled = false;

        // Switch between different players to spectate with mouse buttons
        if (GameControl.instance.GameInputAllowed())
        {
            if (Input.GetMouseButtonDown(0))
                RefreshSpectator(SpectatorRefreshModes.Up);
            else if (Input.GetMouseButtonDown(1))
                RefreshSpectator(SpectatorRefreshModes.Down);
        }



        // Currently not spectating any player, find one to spectate
        if (m_targetObject == null)
        {
            RefreshSpectator(SpectatorRefreshModes.Up);
            return;
        }

        if(GameControl.instance.GameInputAllowed())
        {
            m_spectatingHorAngle =
                Mathf.LerpAngle(m_spectatingHorAngle,
                    m_spectatingHorAngle + Input.GetAxis("Mouse X") * OptionsControl.options.mouseSensitivity,
                    1f);

            m_spectatingVerAngle =
                Mathf.LerpAngle(m_spectatingVerAngle,
                    m_spectatingVerAngle + Input.GetAxis("Mouse Y") * OptionsControl.options.mouseSensitivity,
                    1f);
            m_spectatingVerAngle = Mathf.Clamp(m_spectatingVerAngle, m_cam.MinimumY, m_cam.MaximumY);

            m_spectatingDistance += -Input.mouseScrollDelta.y;
            m_spectatingDistance = Mathf.Clamp(m_spectatingDistance, 1f, 10f);
        }


        Vector3 cameraDirection = Quaternion.Euler(m_spectatingVerAngle, m_spectatingHorAngle, 0) * Vector3.forward;

        transform.position = m_targetObject.transform.position + cameraDirection * m_spectatingDistance;
        m_cam.RotateTowards(m_targetObject.transform.position);

    }

    void UpdateFreeCam()
    {
        if (!m_cam.enabled)
            m_cam.enabled = true;

        bool sprinting = m_camSprint.Get();

        int forwardDir = 0;
        int horizontalDir = 0;
        int verticalDir = 0;

        // Apply speed based on input
        if (m_moveForward.Get())
            forwardDir += 1;
        if (m_moveBackward.Get())
            forwardDir -= 1;
        if (m_moveRight.Get())
            horizontalDir += 1;
        if (m_moveLeft.Get())
            horizontalDir -= 1;
        if (m_jump.Get())
            verticalDir += 1;
        if (m_camDown.Get())
            verticalDir -= 1;

        if (!GameControl.instance.GameInputAllowed())
        {
            forwardDir = 0;
            horizontalDir = 0;
            verticalDir = 0;
        }

        m_speed += TargetCamera.transform.forward * forwardDir * Acceleration * Time.deltaTime;
        m_speed += TargetCamera.transform.right * horizontalDir * Acceleration * Time.deltaTime;
        m_speed += Vector3.up * verticalDir * Acceleration * Time.deltaTime;

        // Cap max-speed depending on we're sprinting or not
        float currentMaxSpeed = sprinting ? SprintMaxSpeed : MaxSpeed;
        if (m_speed.magnitude > currentMaxSpeed)
            m_speed = m_speed.normalized * currentMaxSpeed;

        transform.position += (m_speed);
    }



    IEnumerator CR_KillCam()
    {
        yield return new WaitForSeconds(KillCamDuration);

        // When KillCam is over, change to FollowPlayer mode
        m_cameraMode = CameraModes.FollowPlayer;

        RefreshSpectator(SpectatorRefreshModes.Stay);

        GameControl.instance.SetGameInputPreventionKey(ConstantVars.GIP_KillCam_Key, true);
        yield break;
    }

}
