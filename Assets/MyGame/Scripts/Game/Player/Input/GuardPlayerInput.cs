﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GuardPlayerInput : MonoBehaviour {

    public LayerMask ShootMask;
    public float WeaponRange = 100f;
    public float BulletRadius = 0.25f;

    public float ReloadSpinTime = 0.3f;

    public float AimPunchValue = 50f;
    public float AimPunchDuration = 0.1f;

    public float PunchCooldown = 1f;

    public ParticleSystem BulletHitPS;

    public AudioSource GunSound;
    public AudioSource ReloadSound;
    public AudioSource NoAmmoSound;
    public AudioSource KarateChopSound;

    [SerializeField]
    Sprite m_normalCrosshairSprite;
    [SerializeField]
    Sprite m_noAmmoCrosshairSprite;

    [SerializeField]
    GameObject m_muzzleFlashPrefab;

    [SerializeField]
    GameObject m_midAirBulletPrefab;

    PhotonView m_photonView;
    bool m_hasAmmo = true;
    bool m_shotInternalDelay = false;

    PlayerMovement m_mov;
    PlayerAnimator m_anim;

    Image m_crossHair;

    ObjectOutliner m_outliner = new ObjectOutliner();

    ObjectInteractor m_interactor;

    IEnumerator m_reloadSpinCR;

    KeybindControl.Keybind m_shootBind;
    KeybindControl.Keybind m_punchBind;

    float m_lastPunchTimestamp = 0;

    private void Awake()
    {
        m_lastPunchTimestamp = Time.time;
        m_photonView = PhotonView.Get(this);
    }
    // Use this for initialization
    void Start()
    {
        m_interactor = GetComponent<ObjectInteractor>();
        m_mov = GetComponent<PlayerMovement>();
        m_anim = GetComponent<PlayerAnimator>();

        m_shootBind = KeybindControl.GetKeybind("Shoot Gun");
        m_punchBind = KeybindControl.GetKeybind("Punch");

        if (m_photonView.isMine)
        {
            m_crossHair = GameUIControl.instance.GetUIObject<Image>("Crosshair_Img");
            GameUIControl.instance.SetUIObjectValidator("Crosshair_Img", () =>
            {
                return GameControl.instance.IsGameState(GameControl.GameStates.Game);
            });
            GameUIControl.instance.ForceRefreshAllUIObjects();

            UpdateCrosshair();
        }
    }

    private void OnDisable()
    {
        m_outliner.RemoveOutlines();
    }

    // Update is called once per frame
    void Update() {

        if (!m_photonView.isMine)
            return;

        if(GameControl.instance.GameInputAllowed())
        {
            if (m_shootBind.GetDown())
                Shoot();

            if (m_punchBind.GetDown())
                Punch();
        }
    }

    void UpdateCrosshair()
    {
        // Update crosshair image to notify player of ammo status
        m_crossHair.sprite = m_hasAmmo ? m_normalCrosshairSprite : m_noAmmoCrosshairSprite;
    }

    public void SetReloadAvailable(bool available)
    {
        if (m_interactor.GetInteractObject() == null || m_interactor.GetInteractObject().GetComponent<ReloadObject>() == null)
            return;

        if (available)
            m_outliner.ChangeOutlinedObject(m_interactor.GetInteractObject().gameObject);
        else
            m_outliner.RemoveOutlines();

        GameUIControl.instance.ForceRefreshAllUIObjects();
    }

    void Shoot()
    {
        if (!m_photonView.isMine)
            return;

        if (m_shotInternalDelay || !m_hasAmmo || m_mov.IsStunned())
        {
            // Click sound when firing without ammo
            if(!m_hasAmmo && !m_shotInternalDelay)
                m_photonView.RPC("OnDryFireRPC", PhotonTargets.All);

            return;
        }

        // Consume ammo
        m_hasAmmo = false;
        GameUIControl.instance.ForceRefreshAllUIObjects();

        UpdateCrosshair();

        GameEvents.instance.Trigger("OnPlayerShoot");

        // Aim punch
        StartCoroutine(CR_AimPunch());

        RaycastHit hit;
        bool raycastHit = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, WeaponRange, ShootMask);

        m_photonView.RPC("OnShotFiredRPC", PhotonTargets.All, raycastHit, raycastHit ? (hit.point + hit.normal * 0.03f) : Vector3.zero, hit.normal);

        if (raycastHit)
        {
            GameObject obj = hit.transform.gameObject;

            // Shot hit a player, only valid if the current game state is "Game"
            if (obj.CompareTag("Player") && GameControl.instance.IsGameState(GameControl.GameStates.Game))
            {
                PhotonView view = obj.GetComponentInParent<PhotonView>();
                if (view != null && !view.isMine && hit.rigidbody != null)
                {
                    string victimTeam = Utility.GetPhotonPlayerProp<string>(view.owner, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);

                    // Make sure the player who got shot is an Invisible player
                    if (victimTeam == ConstantVars.InvisibleTeamName)
                    {
                        // Notify the player that was shot and let them confirm the kill (if there was a kill)
                        view.RPC("OnShotRPC", view.owner, hit.rigidbody.name, hit.point);
                    }

                }
            }
        }

        StartCoroutine(CR_InternalShootDelay());
    }

    void Punch()
    {
        if (!m_photonView.isMine || Time.time - m_lastPunchTimestamp < PunchCooldown)
            return;

        m_photonView.RPC("OnPunchRPC", PhotonTargets.All);
        m_lastPunchTimestamp = Time.time;

        CharacterController charController = GetComponent<CharacterController>();
        float punchRadius = charController.radius + 0.15f;
        Collider[] collidersHitByPunch = Physics.OverlapSphere(Camera.main.transform.position + Camera.main.transform.forward * (punchRadius * 2f), punchRadius);

        foreach(Collider c in collidersHitByPunch)
        {
            if (!c.CompareTag("Player"))
                continue;
            else
            {
                PhotonView view = c.GetComponentInParent<PhotonView>();
                if (view != null && !view.isMine && c.GetComponentInParent<InvisiblePlayerInput>() != null &&
                    Utility.GetPhotonPlayerProp<string>(view.GetPhotonOwner(), ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName) == ConstantVars.InvisibleTeamName)
                {
                    view.RPC("OnPunchedRPC", view.GetPhotonOwner());
                    break;
                }
            }
        }

    }

    IEnumerator CR_ReloadSpin()
    {
        float elapsed = -Time.deltaTime;

        AnimationCurve curve = AnimationCurve.EaseInOut(0, 0, ReloadSpinTime, 1f);

        do
        {
            elapsed += Time.deltaTime;
            float ratio = curve.Evaluate(elapsed);

            if (m_anim.GetFPSRig() != null && m_anim.GetFPSRig().RigPistol != null)
                m_anim.GetFPSRig().RigPistol.transform.localRotation =
                    m_anim.GetFPSRig().RigPistol_StartRotation * Quaternion.Euler(0f, 0f, 360f * (1f - ratio));
            yield return null;
        } while (elapsed < ReloadSpinTime);
    }

    IEnumerator CR_InternalShootDelay()
    {
        m_shotInternalDelay = true;
        yield return new WaitForSeconds(1f / PhotonNetwork.sendRateOnSerialize);
        m_shotInternalDelay = false;
        yield break;
    }

    IEnumerator CR_MidAirBullet(bool hit, Vector3 hitPos, Vector3 hitNormal, float maxDuration, float bulletSpeed)
    {
        Transform muzzleFlashPos = m_photonView.isMine && GetComponent<PlayerAnimator>().HasFPSPistol() ? GetComponent<PlayerAnimator>().GetFPSRig().RigPistolMuzzlePosition : GetComponent<PlayerAnimator>().Model.MuzzleFlashPos;
        bool hitTooClose = Vector3.Distance(hitPos, muzzleFlashPos.position) <= 1f;

        Vector3 targetPos =
            (hit && !hitTooClose) ?
            hitPos
            :
            (muzzleFlashPos.position + muzzleFlashPos.forward);
        Vector3 direction = (targetPos - muzzleFlashPos.position).normalized;
        GameObject bullet = Instantiate(m_midAirBulletPrefab, muzzleFlashPos.position, Quaternion.LookRotation(direction, Vector3.up));

        float elapsed = 0f;
        while(elapsed < maxDuration)
        {
            bullet.transform.Translate(direction * bulletSpeed * Time.deltaTime, Space.World);

            // If the bullet should hit something, we check when the bullet has passed the hitPos
            // and then destroy the bullet.
            if (hit)
            {
                Vector3 dirToHitPos = (hitPos - bullet.transform.position).normalized;

                // A very low DOT product value means almost opposite directions, i.e we've passed our target
                if(Vector3.Dot(dirToHitPos, direction) <= 0.5f)
                {
                    bullet.transform.position = hitPos;
                    elapsed = maxDuration;
                }
            }

            elapsed += Time.deltaTime;
            yield return null;
        }

        // Spawn bullet hit particle effect
        if (hit)
            Instantiate(BulletHitPS, hitPos, Quaternion.LookRotation(hitNormal, Vector3.up));

        Destroy(bullet);

        yield break;
    }
    IEnumerator CR_AimPunch()
    {
        float elapsed = 0f;
        FPSCamera cam = GetComponent<FPSCamera>();
        if (cam == null)
            yield break;

        while (elapsed < AimPunchDuration)
        {
            float delta = Time.deltaTime;
            elapsed += Time.deltaTime;

            if (elapsed > AimPunchDuration)
                delta -= (elapsed - AimPunchDuration);

            cam.AimPunch(AimPunchValue * (delta / AimPunchDuration));

            yield return null;
        }

        yield break;
    }

    [PunRPC]
    void OnPunchRPC(PhotonMessageInfo info)
    {
        PhotonPlayer owner = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;
        if (info.sender != owner)
            return;

        m_anim.TriggerPunch();
        KarateChopSound.Play();
    }

    [PunRPC]
    public void OnDryFireRPC(PhotonMessageInfo info)
    {
        PhotonPlayer owner = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;
        if (info.sender != owner)
            return;

        NoAmmoSound.Play();
    }

    [PunRPC]
    public void OnShotFiredRPC(bool hit, Vector3 hitPos, Vector3 hitNormal, PhotonMessageInfo info)
    {
        PhotonPlayer owner = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;
        if (info.sender != owner)
            return;

        // Spawn muzzle flash
        Transform muzzleFlashPos = m_photonView.isMine && GetComponent<PlayerAnimator>().HasFPSPistol() ? GetComponent<PlayerAnimator>().GetFPSRig().RigPistolMuzzlePosition : GetComponent<PlayerAnimator>().Model.MuzzleFlashPos;
        Instantiate(m_muzzleFlashPrefab, muzzleFlashPos, false);

        // Play gun sound
        GunSound.Play();
        GetComponent<PlayerAnimator>().TriggerShot();

        // Spawn mid-air bullet effect (also spawns hit particle effect if it hits something)
        StartCoroutine(CR_MidAirBullet(hit, hitPos, hitNormal, 1f, 200f));
    }


    [PunRPC]
    public void OnHitByItemRPC(int thrownItemId, PhotonMessageInfo info)
    {
        if (!m_photonView.isMine)
            return;

        PhotonView thrownItem = PhotonView.Find(thrownItemId);
        if (thrownItem == null)
            return;

        // Quick sanity check, if the stun item is far away, a stun is unlikely
        if (Vector3.Distance(gameObject.transform.position, thrownItem.transform.position) > 5f)
            return;

        m_mov.StunPlayerWithItem(info.sender, thrownItemId);
    }

    public void OnHitByItem(float hitVelocity, int hitItemID)
    {
        if (hitVelocity >= ConstantVars.ItemMinStunVelocity)
        {
            // Send a "stun request" to the player being stunned, 
            m_photonView.RPC("OnHitByItemRPC", m_photonView.GetPhotonOwner(), hitItemID);
        }
    }

    public void Reload()
    {
        if (m_photonView.isMine)
        {
            ReloadSound.Play();
            m_hasAmmo = true;
            GameUIControl.instance.ForceRefreshAllUIObjects();

            // Reload spin
            if (m_reloadSpinCR != null)
            {
                StopCoroutine(m_reloadSpinCR);
                m_reloadSpinCR = null;
            }
            m_anim.TriggerReload();
            m_reloadSpinCR = CR_ReloadSpin();
            StartCoroutine(m_reloadSpinCR);

            UpdateCrosshair();
        }

    }
}
