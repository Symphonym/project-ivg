﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour, IPunObservable {

    public float WalkSpeed = 4f;
    public float CrouchWalkSpeed = 4f;
    public float CrouchDiff = 1.5f;
    public float CrouchSpeed = 10f;
    public float JumpHeight = 0.85f;
    [Range(0, 1)]
    public float Friction = 0.0f;

    [Header("Stun data")]
    public AudioSource StunSound;
    public AnimationCurve StunBlurring;
    public float StunCooldown = 1.5f;
    bool m_stunnable = true;

    [Header("Networking")]
    public float SyncValueSmoothing = 10f;

    PhotonView m_photonView;

    Vector3 m_speed = Vector3.zero;
    Vector3 m_realSpeed = Vector3.zero;

    CharacterController m_char;

    const int Flag_Crouching = 1;
    const int Flag_Stunned = 2;

    BitSet m_dataBitset = new BitSet();

    float m_elapsedAirTime = 0f;


    PhotonTransformView m_photonTransformView;
    Vector3 m_smoothedSyncValue = Vector3.zero;

    KeybindControl.Keybind m_moveForward;
    KeybindControl.Keybind m_moveBackward;
    KeybindControl.Keybind m_moveRight;
    KeybindControl.Keybind m_moveLeft;
    KeybindControl.Keybind m_jump;
    KeybindControl.Keybind m_crouchKeybind;

    public enum PlayerMoveStates : byte
    {
        Grounded = 0,
        Airborne
    }
    PlayerMoveStates m_playerMoveState = PlayerMoveStates.Grounded;

    float m_smoothedSlopeAngle = 0f;

    Vector3 m_standingCameraPos = Vector3.zero;
    Vector3 m_crouchingCameraPos = Vector3.zero;
    Vector3 m_desiredCameraPos = Vector3.zero;
    float m_standingHeight = 0f;
    float m_crouchingHeight = 0f;
    float m_standingCenterCapsuleY = 0;
    float m_crouchingCenterCapsuleY = 0;

    void Awake()
    {
        m_char = GetComponent<CharacterController>();
        m_photonTransformView = GetComponent<PhotonTransformView>();
        m_photonView = PhotonView.Get(this);
    }


    // Use this for initialization
    void Start()
    {
        m_moveForward = KeybindControl.GetKeybind("Move Forward");
        m_moveBackward = KeybindControl.GetKeybind("Move Backward");
        m_moveRight = KeybindControl.GetKeybind("Move Right");
        m_moveLeft = KeybindControl.GetKeybind("Move Left");
        m_jump = KeybindControl.GetKeybind("Jump");
        m_crouchKeybind = KeybindControl.GetKeybind("Crouch");

        m_desiredCameraPos = GetComponent<FPSCamera>().TargetCamera.transform.localPosition;
        m_standingCameraPos = m_desiredCameraPos;
        m_crouchingCameraPos = m_standingCameraPos - Vector3.up * CrouchDiff;

        m_standingHeight = m_char.height;
        m_crouchingHeight = m_standingHeight - CrouchDiff;

        m_standingCenterCapsuleY = m_char.center.y;
        m_crouchingCenterCapsuleY = m_standingCenterCapsuleY - CrouchDiff / 2f;
    }

    void FixedUpdate()
    {
        if (m_photonView == null || !m_photonView.isMine)
            return;

        // Apply friction on horizontal movement when the character is grounded
        //if (IsControllerGrounded() && !GivingInputs())
        // TODO: use this or the line above?
        if(!GivingInputs())
        {
            m_speed.x *= Friction;
            m_speed.z *= Friction;
        }
    }

    void Update()
    {
        // Update crouching status
        if (m_dataBitset.GetBit(Flag_Crouching))
        {
            m_char.height = m_crouchingHeight;
            m_char.center = Vector3.up * m_crouchingCenterCapsuleY;
        }
        else
        {
            m_char.height = m_standingHeight;
            m_char.center = Vector3.up * m_standingCenterCapsuleY;
        }
        m_desiredCameraPos = Vector3.Lerp(m_desiredCameraPos, m_dataBitset.GetBit(Flag_Crouching) ? m_crouchingCameraPos : m_standingCameraPos, Time.deltaTime * CrouchSpeed);

        if (m_photonView == null || !m_photonView.isMine)
            return;

        float deltaTime = Time.deltaTime;

        int forwardDir = 0;
        int horizontalDir = 0;

        // Movement allowed whenever we're not stunned
        if (!m_dataBitset.GetBit(Flag_Stunned))
        {

            forwardDir = GetForwardInput();
            horizontalDir = GetSideInput();

            // Crouching
            if (GameControl.instance.GameInputAllowed())
            {
                if (m_crouchKeybind.Get())
                    m_dataBitset.SetBit(Flag_Crouching, true);
                else
                {
                    // Check if we can uncrouch
                    Vector3 worldPosCapsuleCenter = transform.position + Vector3.up * m_standingCenterCapsuleY;
                    if (!Physics.CheckCapsule(
                        worldPosCapsuleCenter - Vector3.up * (m_standingHeight / 2f) + m_char.radius * Vector3.up,
                        worldPosCapsuleCenter + Vector3.up * (m_standingHeight / 2f) - m_char.radius * Vector3.up,
                        m_char.radius,
                        LayerMask.GetMask("Environment")))
                    {
                        m_dataBitset.SetBit(Flag_Crouching, false);
                    }
                }
            }
        }

        Vector3 moveDir = transform.TransformDirection(new Vector3(horizontalDir, 0, forwardDir));
        moveDir.Normalize();
        Vector3 baseSpeed = moveDir * (m_dataBitset.GetBit(Flag_Crouching) ? CrouchWalkSpeed : WalkSpeed);

        // Input is given, apply base speed in the wanted direction (no acceleration)
        if (GivingInputs())
        {
            m_speed.x = baseSpeed.x;
            m_speed.z = baseSpeed.z;
        }


        // Apply gravity
        m_speed += Physics.gravity * deltaTime;

        // Reset vertical speed when on ground, and allow for jumping
        if (m_playerMoveState == PlayerMoveStates.Grounded)
        {
            if (m_jump.Get() && GameControl.instance.GameInputAllowed() && !m_dataBitset.GetBit(Flag_Stunned) && !m_dataBitset.GetBit(Flag_Crouching))
            {
                m_playerMoveState = PlayerMoveStates.Airborne;

                // Calculate launch velocity given desired Jump height
                m_speed.y = Mathf.Sqrt(2 * Mathf.Abs(Physics.gravity.y) * Mathf.Abs(JumpHeight));

                // Force a footstep when the player jumps
                PlayerFootstep footstep = GetComponent<PlayerFootstep>();
                if (footstep != null)
                    footstep.ForceStep();
            }
        }

        Vector3 curPos = m_char.transform.position;

        m_char.Move(m_speed * deltaTime);

        // Make an effort to clamp player to slopes
        RaycastHit hit;
        if (Physics.Raycast(transform.position + m_char.center, Vector3.down, out hit, m_char.height / 2f + 0.5f, LayerMask.GetMask("Environment")))
        {
            float angle = Mathf.Clamp(Vector3.Angle(Vector3.up, hit.normal), 0f, 90f);

            m_smoothedSlopeAngle = Mathf.LerpAngle(m_smoothedSlopeAngle, angle, Time.deltaTime * 5f);
            float ratio = m_smoothedSlopeAngle / 90f;

            // This if statement might sound a bit contradictory, but it's basically saying:
            // IF CLOSE TO GROUND BUT NOT TOUCHING THE GROUND
            if (m_playerMoveState != PlayerMoveStates.Airborne && !IsControllerGrounded() && ratio > 0f)
                m_char.Move(Vector3.down * ratio * 2f);
        }

        if (!IsControllerGrounded())
            m_elapsedAirTime += Time.deltaTime;
        else
            m_elapsedAirTime = 0f;

        // Player changes state to Airborne if they've been in the air for a brief moment to ignore imprecision by m_char.isGrounded
        if (m_playerMoveState == PlayerMoveStates.Grounded && m_elapsedAirTime >= 0.2f)
            m_playerMoveState = PlayerMoveStates.Airborne;
        else if (m_playerMoveState == PlayerMoveStates.Airborne && IsControllerGrounded())
            m_playerMoveState = PlayerMoveStates.Grounded;

        if (m_playerMoveState == PlayerMoveStates.Grounded)
            m_speed.y = 0f;

        // Check how much the CharacterController moved (easy way to check if we're walking into a wall)
        m_realSpeed = m_char.transform.position - curPos;
        m_realSpeed /= deltaTime;

        // We don't use real speed Y (vertical speed) since we need to keep getting pushed into the ground to stay grounded.
        m_speed.x = m_realSpeed.x;
        m_speed.z = m_realSpeed.z;

        m_smoothedSyncValue = Vector3.Lerp(m_smoothedSyncValue, m_realSpeed, Time.deltaTime * SyncValueSmoothing);
        m_photonTransformView.SetSynchronizedValues(m_smoothedSyncValue, 0);
    }
    // TODO: Masterclient can check if movement speed is too fast for X seconds, and then kick player for abnormal speed

    IEnumerator CR_Stun()
    {
        if (!m_photonView.isMine || !m_stunnable)
            yield break;

        UnityStandardAssets.ImageEffects.BlurOptimized blur = GetComponent<FPSCamera>().TargetCamera.GetComponent<UnityStandardAssets.ImageEffects.BlurOptimized>();

        if (blur != null)
        {
            m_dataBitset.SetBit(Flag_Stunned, true);
            m_stunnable = false;

            blur.downsample = 0;
            blur.blurSize = 0;
            blur.enabled = true;
        }
        else
            yield break;

        float stunDuration = StunBlurring.keys[StunBlurring.length - 1].time;
        float elapsed = -Time.deltaTime;
        do
        {
            elapsed += Time.deltaTime;
            float ratio = StunBlurring.Evaluate(elapsed);

            blur.blurSize = ratio * 10f;
            yield return null;
        } while (elapsed < stunDuration);

        blur.enabled = false;
        m_dataBitset.SetBit(Flag_Stunned, false);

        yield return new WaitForSeconds(StunCooldown);

        m_stunnable = true;
        yield break;
    }

    [PunRPC]
    public void OnStunnedRPC(PhotonPlayer hitter, bool destroyItem, int thrownItemId, PhotonMessageInfo info)
    {
        PhotonPlayer owner = m_photonView.GetPhotonOwner();
        // Only the stunned player can send the stun notification
        if (info.sender != owner)
            return;

        // Apply the stun effect for the player who was stunned
        if (m_photonView.isMine)
        {
            StartCoroutine(CR_Stun());
            if (GameEvents.instance != null)
                GameEvents.instance.Trigger("OnPlayerStunned");
        }

        StunSound.Play();
        KillfeedControl.instance.SpawnKillfeedStun(hitter, m_photonView.GetPhotonOwner());

        // Let the item owner destroy the thrown item
        if(destroyItem)
        {
            PhotonView thrownItem = PhotonView.Find(thrownItemId);
            if (thrownItem != null && thrownItem.isMine)
                PhotonNetwork.Destroy(thrownItem);
        }
    }

    public void StunPlayerWithItem(PhotonPlayer hitter, int thrownItemId)
    {
        if(m_stunnable && m_photonView.isMine)
            m_photonView.RPC("OnStunnedRPC", PhotonTargets.All, hitter, true, thrownItemId);
    }
    public void StunPlayer(PhotonPlayer hitter)
    {
        if (m_stunnable && m_photonView.isMine)
            m_photonView.RPC("OnStunnedRPC", PhotonTargets.All, hitter, false, -1);
    }

    // True if the player is giving any movement inputs, false otherwise
    bool GivingInputs()
    {
        if (m_photonView != null && m_photonView.isMine)
        {
            bool givingInputs = m_moveForward.Get() || m_moveBackward.Get() || m_moveRight.Get() || m_moveLeft.Get();
            return givingInputs && GameControl.instance.GameInputAllowed();
        }
        else
        {
            Debug.LogError("Can't retrieve input status on non-local player");
            return false;
        }
    }

    bool IsControllerGrounded()
    {
        return m_char.isGrounded;
    }

    public Vector3 GetDesiredCameraPos()
    {
        return m_desiredCameraPos;
    }
    public bool IsCrouching()
    {
        return m_dataBitset.GetBit(Flag_Crouching);
    }
    public Vector3 GetRealSpeed()
    {
        return m_realSpeed;
    }

    public int GetSideInput()
    {
        int horDir = 0;
        if (m_photonView.isMine)
        {
            if (!GameControl.instance.GameInputAllowed())
                return 0;

            if (m_moveRight.Get())
                horDir += 1;
            if (m_moveLeft.Get())
                horDir -= 1;
        }
        else
            horDir = 0;

        return horDir;
    }
    public int GetForwardInput()
    {
        int forwardDir = 0;
        if (m_photonView.isMine)
        {
            if (!GameControl.instance.GameInputAllowed())
                return 0;

            if (m_moveForward.Get())
                forwardDir += 1;
            if (m_moveBackward.Get())
                forwardDir -= 1;
        }
        else
            forwardDir = 0;

        return forwardDir;
    }
    public bool IsStunned()
    {
        return m_dataBitset.GetBit(Flag_Stunned);
    }

    public PlayerMoveStates GetPlayerMoveState()
    {
        return m_playerMoveState;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(m_realSpeed);
            stream.SendNext((byte)m_playerMoveState);
            stream.SendNext(m_dataBitset.AsByte());
        }
        else if (stream.isReading)
        {
            m_realSpeed = (Vector3)stream.ReceiveNext();
            m_playerMoveState = (PlayerMoveStates)((byte)stream.ReceiveNext());
            m_dataBitset.FromByte((byte)stream.ReceiveNext());
        }

    }
}
