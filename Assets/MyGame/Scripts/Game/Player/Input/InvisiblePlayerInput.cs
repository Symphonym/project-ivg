﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PlayerMovement))]
[RequireComponent(typeof(PlayerAnimator))]
[RequireComponent(typeof(RigidbodyPickup))]
public class InvisiblePlayerInput : MonoBehaviour, IPunObservable {

    public Material TransparencyMaterial;

    public List<AudioClip> ThrowSounds;
    public List<AudioClip> PickupSounds;
    public AudioSource InteractSoundSource;
    public AudioSource DeathSoundSource;

    public float InvisibleDelay = 0.2f;

    public float ItemMaxPickupDistance = 2f;
    public float ItemThrowForce = 150f;
    public float ItemDropDistance = 3f;
    public string ItemPickupTag = "GameItem";
    public LayerMask ItemMask;

    public float PickupWalkSpeed = 4f;
    public float PickupCrouchWalkSpeed = 2f;
    float m_originalWalkSpeed;
    float m_originalCrouchWalkSpeed;

    [Header("Ragdoll")]
    public float RagdollDeathVelocity = 25f;

    PlayerMovement m_mov;
    PlayerAnimator m_anim;
    RigidbodyPickup m_pickup;

    class HoldData
    {
        public GameObject Object;

        public Vector3 LocalGrabPoint;
        public Quaternion RelativeRotation;
        public float HoldDistance;
        public OwnershipControl Ownership;
        public Collider ObjCollider;
    }
    HoldData m_heldObject;
    ObjectOutliner m_outliner = new ObjectOutliner();

    // Time since the last invisibilty
    float m_delayElapsed = 0f;
    List<Material> m_origMats = new List<Material>();

    // If the player is invisible or not
    bool m_visible = true;

    // Hacky fix to stop updates from running in the brief moment when the player is killed
    bool m_stopUpdating = false;

    PhotonView m_photonView;

    KeybindControl.Keybind m_pickupBind;
    KeybindControl.Keybind m_dropBind;
    KeybindControl.Keybind m_throwBind;

    ProgressBarInput m_invisBar;

    bool m_hoveringOverItem = false;

    private void Awake()
    {
        m_photonView = PhotonView.Get(this);
    }
    // Use this for initialization
    void Start ()
    {
        m_mov = GetComponent<PlayerMovement>();
        m_anim = GetComponent<PlayerAnimator>();
        m_pickup = GetComponent<RigidbodyPickup>();

        m_originalWalkSpeed = m_mov.WalkSpeed;
        m_originalCrouchWalkSpeed = m_mov.CrouchWalkSpeed;

        m_pickupBind = KeybindControl.GetKeybind("Pickup Item");
        m_dropBind = KeybindControl.GetKeybind("Drop Item");
        m_throwBind = KeybindControl.GetKeybind("Throw Item");

        // Configure UI
        if (m_photonView.isMine)
        {
            m_invisBar = GameUIControl.instance.GetUIObject<ProgressBarInput>("StealthBar");
            GameUIControl.instance.SetUIObjectValidator("StealthBar", () =>
            {
                return !GameControl.instance.IsGameState(GameControl.GameStates.Lobby) &&
                    !GameControl.instance.IsGameState(GameControl.GameStates.Ending) &&
                    Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerAlive_Key, true);
            });


            GameUIControl.instance.SetUIObjectValidator("InvisDefaultCrosshair_Img", () =>
            {
                return GameControl.instance.IsGameState(GameControl.GameStates.Game) &&
                    Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerAlive_Key, true) &&
                    !m_hoveringOverItem &&
                    !m_pickup.IsHoldingObject();
            });
            GameUIControl.instance.SetUIObjectValidator("InvisHoverCrosshair_Img", () =>
            {
                return GameControl.instance.IsGameState(GameControl.GameStates.Game) &&
                    Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerAlive_Key, true) &&
                    m_hoveringOverItem &&
                    !m_pickup.IsHoldingObject();
            });
            GameUIControl.instance.SetUIObjectValidator("InvisHoldCrosshair_Img", () =>
            {
                return GameControl.instance.IsGameState(GameControl.GameStates.Game) &&
                    Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerAlive_Key, true) &&
                    m_pickup.IsHoldingObject();
            });
            //GameUIControl.instance.GetUIObject<Image>("InteractSymbol_Img");
            /*GameUIControl.instance.SetUIObjectValidator("InteractSymbol_Img", () =>
            {
                return !GameControl.instance.IsGameState(GameControl.GameStates.Lobby) &&
                    Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerAlive_Key, true) &&
                    m_hoveringOverItem &&
                    !m_pickup.IsHoldingObject();
            });*/
            GameUIControl.instance.ForceRefreshAllUIObjects();
        }

        foreach (Renderer r in m_anim.PlayerRenderers)
            m_origMats.Add(r.material);
	}

    private void OnEnable()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.AddListener("OnPlayerStunned", OnPlayerStunned);
    }
    private void OnDisable()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.RemoveListener("OnPlayerStunned", OnPlayerStunned);
    }

    // Update is called once per frame
    void Update ()
    {
        if (m_stopUpdating)
            return;

        UpdateAll();
        UpdateOwningClient();
    }

    void UpdateAll()
    {
        m_anim.SetHoldingItem(m_pickup.IsHoldingObject());

        int index = 0;
        foreach (Renderer r in m_anim.PlayerRenderers)
        {
            r.material = m_visible ? m_origMats[index] : TransparencyMaterial;
            ++index;
        }

        if (m_anim.Model != null)
            m_anim.Model.SetCosmeticsInvisible(!m_visible);
    }

    void UpdateOwningClient()
    {
        if (!m_photonView.isMine)
            return;

        const float InvisibilityMovementLimit = 0.3f;

        // TODO: Make sure this new formula works, previous was only !m_mov.GivingInputs()
        bool invisible = m_mov.GetRealSpeed().magnitude <= InvisibilityMovementLimit && !m_mov.IsStunned();

        m_mov.WalkSpeed = m_pickup.IsHoldingObject() ? PickupWalkSpeed : m_originalWalkSpeed;
        m_mov.CrouchWalkSpeed = m_pickup.IsHoldingObject() ? PickupCrouchWalkSpeed : m_originalCrouchWalkSpeed;

        // Reset invisibilty
        if (!invisible)
        {
            m_visible = true;
            m_delayElapsed = 0f;
        }

        if (m_delayElapsed >= InvisibleDelay)
        {
            if (invisible)
            {
                m_invisBar.SetProgress(1f, "Invisible");
                m_visible = false;
            }
        }
        else
        {
            m_invisBar.SetProgress(m_delayElapsed / InvisibleDelay, System.Math.Round((InvisibleDelay - m_delayElapsed), 1).ToString() + "s");
            m_delayElapsed += Time.deltaTime;
        }

        if (GameControl.instance.GameInputAllowed())
        {
            if (m_pickup.IsHoldingObject())
            {
                // Drop object
                if (m_dropBind.GetDown())
                    DropObject();

                // Throw object
                else if (m_throwBind.GetDown())
                    ThrowObject(Camera.main.transform.forward * ItemThrowForce);
            }
            else
            {
                RaycastHit hit;
                bool raycastHit = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, ItemMaxPickupDistance, ItemMask);
                bool validHit = raycastHit && hit.transform.CompareTag(ItemPickupTag);

                // Can't pick up an object that is being held
                if(validHit)
                {
                    HoldableObject holdData = hit.transform.GetComponent<HoldableObject>();
                    if (holdData != null && holdData.IsHoldingObject)
                        validHit = false;
                }


                if (m_hoveringOverItem != validHit)
                {
                    m_hoveringOverItem = validHit;
                    RefreshCrosshair();
                }

                // Highlight the item that we're mousing over
                if (m_hoveringOverItem)
                    m_outliner.ChangeOutlinedObject(hit.transform.gameObject);
                else
                    m_outliner.RemoveOutlines();

                // Pick up object
                if (m_pickupBind.GetDown() && validHit)
                {
                    
                    OwnershipControl itemOwnership = hit.transform.GetComponent<OwnershipControl>();
                    Rigidbody itemBody = hit.transform.GetComponent<Rigidbody>();

                    if (itemOwnership != null && itemBody != null)
                    {
                        m_heldObject = new HoldData();
                        m_heldObject.Object = hit.transform.gameObject;
                        m_heldObject.ObjCollider = hit.collider;
                        m_heldObject.LocalGrabPoint = itemBody.transform.InverseTransformPoint(hit.point);
                        m_heldObject.HoldDistance = hit.distance;

                        // This quaternion formula took two days to figure out...
                        m_heldObject.RelativeRotation =
                            Quaternion.Inverse(Quaternion.Euler(0, Camera.main.transform.eulerAngles.y, 0)) * itemBody.rotation;

                        // Request ownership of item and await response from current owner
                        m_heldObject.Ownership = itemOwnership;
                        m_heldObject.Ownership.OnOwnershipGiven += OnGivenItemOwnership;
                        m_heldObject.Ownership.RequestOwnership();
                    }
                }

            }
        }
        


        // Update held item position
        if(m_pickup.IsHoldingObject())
        {
            Rigidbody body = m_heldObject.Object.GetComponent<Rigidbody>();
            Vector3 adjustedPoint = body.rotation * m_heldObject.LocalGrabPoint;

            m_pickup.SetHoldPosition(Camera.main.transform.position + Camera.main.transform.forward * m_heldObject.HoldDistance - adjustedPoint);
            if (Vector3.Distance(m_pickup.GetCurrentObjectPosition() + adjustedPoint, Camera.main.transform.position + Camera.main.transform.forward * m_heldObject.HoldDistance) >= ItemDropDistance)
                DropObject();
        }
    }

    void FixedUpdate()
    {
        if (!m_photonView.isMine)
            return;

        // Update held item rotation
        if (m_pickup.IsHoldingObject())
        {
            Rigidbody body = m_heldObject.Ownership.GetComponent<Rigidbody>();
            body.rotation = Quaternion.Euler(0 , Camera.main.transform.eulerAngles.y, 0) *  m_heldObject.RelativeRotation;
        }
    }

    void RefreshCrosshair()
    {
        GameUIControl.instance.ForceRefreshUIObject("InvisDefaultCrosshair_Img");
        GameUIControl.instance.ForceRefreshUIObject("InvisHoverCrosshair_Img");
        GameUIControl.instance.ForceRefreshUIObject("InvisHoldCrosshair_Img");
    }

    void ThrowObject(Vector3 force)
    {
        if (!m_photonView.isMine || !m_pickup.IsHoldingObject())
            return;

        m_pickup.ThrowObject(Camera.main.transform.forward * ItemThrowForce);

        m_photonView.RPC("OnThrowObjectRPC", PhotonTargets.All);

        m_heldObject.Ownership.SetOwnershipTransferAllowed(true);
        m_heldObject = null;

        RefreshCrosshair();
    }
    void DropObject()
    {
        if (!m_photonView.isMine || !m_pickup.IsHoldingObject())
            return;

        m_pickup.DropObject();

        m_heldObject.Ownership.SetOwnershipTransferAllowed(true);
        m_heldObject = null;

        RefreshCrosshair();
    }

    void PlayPickupSound()
    {
        InteractSoundSource.clip = PickupSounds[Random.Range(0, PickupSounds.Count)];
        InteractSoundSource.pitch = Random.Range(0.9f, 1.1f);
        InteractSoundSource.Play();
    }
    void PlayThrowSound()
    {
        InteractSoundSource.clip = ThrowSounds[Random.Range(0, ThrowSounds.Count)];
        InteractSoundSource.pitch = Random.Range(0.9f, 1.1f);
        InteractSoundSource.Play();
    }

    [PunRPC]
    void OnThrowObjectRPC(PhotonMessageInfo info)
    {
        PhotonPlayer owner = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;
        if (info.sender != owner)
            return;

        m_anim.TriggerThrowItem();
        PlayThrowSound();
    }
    [PunRPC]
    void OnPickupObjectRPC(PhotonMessageInfo info)
    {
        PhotonPlayer owner = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;
        if (info.sender != owner)
            return;

        PlayPickupSound();
    }

    [PunRPC]
    void OnPunchedRPC(PhotonMessageInfo info)
    {
        // Make sure the person who punched us is a Guard player
        if (Utility.GetPhotonPlayerProp<string>(info.sender, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName) != ConstantVars.GuardTeamName)
            return;

        if (!m_photonView.isMine)
            return;

        m_mov.StunPlayer(info.sender);
    }

    [PunRPC]
    void OnShotRPC(string hitBoneName, Vector3 hitPoint, PhotonMessageInfo info)
    {
        // Quick sanity check, killer (sender) must be a Guard player
        if (!m_photonView.isMine ||
            Utility.GetPhotonPlayerProp<string>(info.sender, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName) != ConstantVars.GuardTeamName)
            return;

        // We're only killable if we're alive
        if (Utility.GetPhotonPlayerProp<bool>(ConstantVars.PlayerAlive_Key, true))
        {
            // Make sure to drop any held object when killed
            DropObject();

            // Remove any outlines of objects when killed
            m_outliner.RemoveOutlines();

            // Set player status to dead
            ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
            hashtable[ConstantVars.PlayerAlive_Key] = false;
            PhotonNetwork.player.SetCustomProperties(hashtable);

            // Hide stealth bar when you die
            GameUIControl.instance.ForceRefreshAllUIObjects();

            // Immediately send out the "OnKilledRPC" before the Invisible player object is destroyed
            m_photonView.RPC("OnKilledRPC", PhotonTargets.All, hitBoneName, hitPoint, info.sender);
            PhotonNetwork.SendOutgoingCommands();

            Vector3 curPos = Camera.main.transform.position;

            // Respawn as a spectator at the location where you died
            PlayerSpawner.instance.SpawnSpectator();
            PlayerSpawner.instance.GetPlayerObject().transform.position = curPos;
            PlayerSpawner.instance.GetPlayerObject().GetComponent<SpectatorMovement>().SetKillCam(info.sender);
        }
    }

    [PunRPC]
    void OnKilledRPC(string hitBoneName, Vector3 hitPoint, PhotonPlayer killer, PhotonMessageInfo info)
    {
        PhotonPlayer owner = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;
        if (info.sender != owner)
            return;

        PhotonView killerObjView = PhotonView.Find(Utility.GetPhotonPlayerProp<int>(killer, ConstantVars.PlayerObject_Key, -1));
        GameObject killerObj = killerObjView != null ? killerObjView.gameObject : null;

        bool imTheKiller = PhotonNetwork.player.Equals(killer);

        PhotonPlayer victim = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;

        m_stopUpdating = true;

        // Retrieve the ragdolls before we detach
        Rigidbody[] rbs = GetComponentsInChildren<Rigidbody>();
        m_anim.Model.DetachIntoRagdollMode();

        if (killerObj != null)
        {
            foreach (Rigidbody rb in rbs)
            {
                if (rb.name.Equals(hitBoneName))
                {
                    if(killerObj.GetComponent<PlayerAnimator>().Model.MuzzleFlashPos != null)
                    {
                        Vector3 shootDirection = (hitPoint - killerObj.GetComponent<PlayerAnimator>().Model.MuzzleFlashPos.position).normalized;
                        // TODO: ForceAtPos or just force?
                        //rb.AddForceAtPosition(shootDirection * RagdollDeathVelocity, hitPoint, ForceMode.VelocityChange);
                        rb.AddForce(shootDirection * RagdollDeathVelocity, ForceMode.VelocityChange);
                    }
                    break;
                }
            }
        }

        // Killfeed
        if (killer != null)
            KillfeedControl.instance.SpawnKillfeedGuard(killer, victim);

        // The kill has been confirmed by the victim, so give the killer their rewards
        if (imTheKiller)
        {
            // Increment player kill count
            ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
            hashtable[ConstantVars.PlayerKills_Key] = Utility.GetPhotonPlayerProp<int>(ConstantVars.PlayerKills_Key, 0) + 1;
            PhotonNetwork.player.SetCustomProperties(hashtable);

            // Guard weapon is reloaded if it was a kill
            GuardPlayerInput guardInput = PlayerSpawner.instance.GetPlayerObject().GetComponent<GuardPlayerInput>();
            if (guardInput)
                guardInput.Reload();

            if (GameEvents.instance != null)
                GameEvents.instance.Trigger("OnPlayerScoreKill");
        }

        DeathSoundSource.transform.parent = null;
        DeathSoundSource.GetComponent<AudioSourceAutoDestroy>().AllowDestruction();
        DeathSoundSource.Play();
    }

    [PunRPC]
    void OnItemThrownAwayRPC(PhotonMessageInfo info)
    {
        KillfeedControl.instance.SpawnKillfeedInvisible(info.sender);
        GameControl.instance.ItemWasScored();
    }

    void OnGivenItemOwnership(OwnershipControl item)
    {
        if (!m_photonView.isMine)
            return;

        // While item is held, no one else is allowed to take ownership of the object
        item.SetOwnershipTransferAllowed(false);

        m_photonView.RPC("OnPickupObjectRPC", PhotonTargets.All);

        item.OnOwnershipGiven -= OnGivenItemOwnership;
        m_pickup.HoldObject(item.GetComponent<HoldableObject>());

        // Hide interaction symbol when the item is picked up
        RefreshCrosshair();

        m_outliner.RemoveOutlines();
    }

    void OnPlayerStunned(GameEvents.Event eventInfo)
    {
        if (m_photonView.isMine)
            DropObject();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        // Synchronize visibility
        if(stream.isWriting)
            stream.SendNext(m_visible);
        else if(stream.isReading)
            m_visible = (bool)stream.ReceiveNext();
    }
}
