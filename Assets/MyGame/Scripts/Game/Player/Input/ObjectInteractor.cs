﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(PhotonView))]
public class ObjectInteractor : MonoBehaviour {

    public float MaxInteractRange = 5f;
    public LayerMask InteractMask;


    PhotonView m_photonView;

    InteractableObject m_hoverObject = null;
    bool m_interacting = false;

    KeybindControl.Keybind m_interactKeybind = null;
    Text m_interactText;

    InteractionTimerData m_interactionTimer;
    float m_interactionElapsed = 0f;
    bool m_showTimer = false;

    private void Awake()
    {
        m_photonView = PhotonView.Get(this);
    }

    private void Start()
    {
        if(m_photonView.isMine)
        {
            m_interactKeybind = KeybindControl.GetKeybind("Interact");
            m_interactText = GameUIControl.instance.GetUIObject<Text>("Interact_Txt");
            GameUIControl.instance.SetUIObjectValidator("Interact_Txt", () =>
            {
                return GameControl.instance.IsGameState(GameControl.GameStates.Game) && GetInteractObject() != null;
            });

            m_interactionTimer = GameUIControl.instance.GetUIObject<InteractionTimerData>("InteractionTimer");
            GameUIControl.instance.SetUIObjectValidator("InteractionTimer", () =>
            {
                return GameControl.instance.IsGameState(GameControl.GameStates.Game) && GetInteractObject() != null && m_showTimer;
            });
        }

    }
    // Update is called once per frame
    void Update ()
    {
        if (!m_photonView.isMine || !GameControl.instance.GameInputAllowed())
            return;

        RaycastHit hit;
        bool raycastHit = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, MaxInteractRange, InteractMask);
        InteractableObject obj = raycastHit ? hit.transform.gameObject.GetComponent<InteractableObject>() : null;

        if (obj == null)
            raycastHit = false;
        else
        {
            string team = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
            if ((team == ConstantVars.InvisibleTeamName && !obj.EnableForTeamInvisible) ||
                (team == ConstantVars.GuardTeamName && !obj.EnableForTeamGuard))
                raycastHit = false;
        }

        if (raycastHit)
        {
            // Make sure we actually hover another object
            if (obj != m_hoverObject)
            {
                // Cleanup on previosuly hovered object
                if (m_hoverObject != null)
                {
                    m_hoverObject.OnInteractEnd(gameObject);
                    m_hoverObject.OnHoverEnd(gameObject);
                    m_hoverObject = null;
                }

                InteractionCleanup();

                m_hoverObject = obj;
                m_hoverObject.OnHoverStart(gameObject);

                m_interactText.text = m_hoverObject.InfoText.Replace("#", "[" + m_interactKeybind.AsString() + "]");

                GameUIControl.instance.ForceRefreshUIObject("Interact_Txt");
                GameUIControl.instance.ForceRefreshUIObject("InteractionTimer");
            }

            // Otherwise player is still hovering the same object
            else if(m_interacting)
            {
                // If no delay, trigger continuous interaction
                if(m_hoverObject.InteractionDelay <= 0f)
                    m_hoverObject.OnInteracting(gameObject, hit);
                else
                {
                    m_interactionElapsed += Time.deltaTime;
                    m_interactionTimer.TimerImage.fillAmount = Mathf.Clamp01(m_interactionElapsed / m_hoverObject.InteractionDelay);
                    m_interactionTimer.TimerText.text = System.Math.Round(Mathf.Clamp(m_hoverObject.InteractionDelay - m_interactionElapsed, 0f, m_hoverObject.InteractionDelay), 1).ToString() + "s";

                    if(m_interactionElapsed >= m_hoverObject.InteractionDelay)
                    {
                        m_hoverObject.OnInteractStart(gameObject);

                        // If we don't immediately reset interaction, the interaction ends here
                        if (!m_hoverObject.ImmediatelyResetInteraction)
                            EndInteraction();

                        // Otherwise just reset the elapsed time and continue
                        else
                            m_interactionElapsed = 0f;
                    }
                }
            }
        }
        else
        {
            // No longer hovering over any object, do cleanup
            if(m_hoverObject != null)
            {
                m_hoverObject.OnInteractEnd(gameObject);
                m_hoverObject.OnHoverEnd(gameObject);

                m_hoverObject = null;

                InteractionCleanup();
            }
        }

        if(m_hoverObject != null)
        {
            if(m_interactKeybind.GetDown() && !m_interacting)
            {
                // Immediately interact if there is no delay
                if (m_hoverObject.InteractionDelay <= 0f)
                    m_hoverObject.OnInteractStart(gameObject);
                else
                {
                    m_interactionTimer.TimerImage.fillAmount = 0f;
                    m_interactionTimer.TimerText.text = System.Math.Round(m_hoverObject.InteractionDelay, 1).ToString() + "s";
                    m_showTimer = true;
                }
                m_interacting = true;

                GameUIControl.instance.ForceRefreshUIObject("Interact_Txt");
                GameUIControl.instance.ForceRefreshUIObject("InteractionTimer");
            }
            else if(m_interactKeybind.GetUp() && m_interacting)
                EndInteraction();
        }
    }

    void EndInteraction()
    {
        if (m_hoverObject == null)
            return;

        m_hoverObject.OnInteractEnd(gameObject);
        InteractionCleanup();
    }

    void InteractionCleanup()
    {
        m_interactionElapsed = 0f;
        m_interacting = false;
        m_showTimer = false;

        GameUIControl.instance.ForceRefreshUIObject("Interact_Txt");
        GameUIControl.instance.ForceRefreshUIObject("InteractionTimer");
    }

    public InteractableObject GetInteractObject()
    {
        return m_hoverObject;
    }
}
