﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(FPSCamera))]
[RequireComponent(typeof(PlayerAnimator))]
public class PlayerFlashlight : MonoBehaviour, IPunObservable{

    public Light Flashlight;
    public AudioSource FlickSound;

    public float RemoteClientMieScatter = 0.7f;

    FPSCamera m_cam;
    bool m_flashLightOn = false;
    bool m_prevFlashlightOn = false;

    PhotonView m_photonView;

    KeybindControl.Keybind m_flashlightBind;
    Camera m_playerCamera;

    float m_smoothedYRotation = 0f;

    void Awake()
    {
        Flashlight.enabled = false;
        m_photonView = PhotonView.Get(this);
    }

    // Use this for initialization
    void Start ()
    {
        m_cam = GetComponent<FPSCamera>();
        m_flashlightBind = KeybindControl.GetKeybind("Flashlight");

        m_playerCamera = GetComponent<FPSCamera>().TargetCamera;

        Flashlight.transform.SetParent(GetComponent<PlayerAnimator>().Model.FlashlightMount);
        Flashlight.transform.localPosition = Vector3.zero;


        HxVolumetricLight volLight = Flashlight.GetComponent<HxVolumetricLight>();
        if (volLight != null)
        {
            volLight.ExtraDensity = m_photonView.isMine ? 0f : volLight.ExtraDensity;

            volLight.CustomMieScatter = !m_photonView.isMine;
            volLight.MieScattering = m_photonView.isMine ? 0f : RemoteClientMieScatter;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (m_photonView.isMine && m_flashlightBind.GetDown() && GameControl.instance.GameInputAllowed())
            m_flashLightOn = !m_flashLightOn;

        // Play a sound when the flashlight is toggled (this is heard by all players)
        if (m_flashLightOn != m_prevFlashlightOn)
        {
            m_prevFlashlightOn = m_flashLightOn;
            FlickSound.Play();
        }

        Flashlight.enabled = m_flashLightOn;
	}

    private void LateUpdate()
    {
        m_smoothedYRotation = Mathf.LerpAngle(m_smoothedYRotation, m_cam.GetYRotation(), Time.deltaTime * 8f);

        float yRot = m_smoothedYRotation;
        // No smoothing used if we're the owning client
        if (m_photonView.isMine)
            yRot = m_cam.GetYRotation();

        Vector3 targetLightDir = Quaternion.AngleAxis(-yRot, gameObject.transform.right) * gameObject.transform.forward;
        Vector3 lookPos = m_playerCamera.transform.position + targetLightDir * Flashlight.range;
        Quaternion targetRot = Quaternion.LookRotation((lookPos - Flashlight.transform.position).normalized);

        Flashlight.transform.rotation = targetRot;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
            stream.SendNext(m_flashLightOn);
        else if (stream.isReading)
            m_flashLightOn = (bool)stream.ReceiveNext();
    }
}
