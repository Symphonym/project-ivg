﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSoundAdjuster : MonoBehaviour {

	// Use this for initialization
	void Start () {

        PhotonView view = PhotonView.Get(this);
        if (view != null && !view.isMine)
            return;

        AudioSource[] sources = GetComponentsInChildren<AudioSource>();
        foreach(AudioSource s in sources)
        {
            s.spatialize = false;
            s.spatialBlend = 0;
        }
    }
}
