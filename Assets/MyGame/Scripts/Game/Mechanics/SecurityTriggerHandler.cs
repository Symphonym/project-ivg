﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityTriggerHandler : MonoBehaviour {
    
    public virtual void InitializeHandler(SecurityTrigger trigger)
    {

    }
    public virtual void OnSecurityTriggerEnter(Collider other)
    {

    }
}
