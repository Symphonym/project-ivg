﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootprintFader : MonoBehaviour {

    public Renderer LeftPrint;
    public Renderer RightPrint;

    public float Duration = 8f;

    float m_elapsed = 0f;
    Color m_leftColor = Color.black;
    Color m_rightColor = Color.black;

    void Start()
    {
        if(LeftPrint != null)
            m_leftColor = LeftPrint.material.color;
        if (RightPrint != null)
            m_rightColor = RightPrint.material.color;
    }

	// Update is called once per frame
	void Update ()
    {
        m_elapsed += Time.deltaTime;
        float ratio = Mathf.Clamp01(m_elapsed / Duration);

        m_leftColor.a = 1f - ratio;
        m_rightColor.a = 1f - ratio;

        if (LeftPrint != null)
            LeftPrint.material.color = m_leftColor;
        if (RightPrint != null)
            RightPrint.material.color = m_rightColor;

        if (m_elapsed >= Duration)
            Destroy(gameObject);
	}
}
