﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameItemKillZone : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("GameItem"))
            return;

        PhotonView view = other.GetComponentInParent<PhotonView>();
        if (view == null)
            view = other.GetComponentInParent<PhotonView>();

        // Only the owner can destroy the object
        if (!view.isMine)
            return;

        HoldableObject holdObj = view.GetComponent<HoldableObject>();
        if (holdObj == null)
            return;

        // Make sure the item is valid (i.e if it should be in the level or not due to density settings)
        int itemDensity = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomSetting_ItemDensity_Key, (int)GameControl.ItemDensities.High);
        if ((int)holdObj.ItemDensityGroup > itemDensity)
            return;

        // Make sure the player owning the destroy object is actually an Team Invisible player
        bool isInvisible = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName) == ConstantVars.InvisibleTeamName;
        if (isInvisible && GameControl.instance.IsGameState(GameControl.GameStates.Game)) // Throw only valid if in Game state
        {
            // Increment player score
            ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
            hashtable[ConstantVars.PlayerThrows_Key] = Utility.GetPhotonPlayerProp<int>(ConstantVars.PlayerThrows_Key, 0) + 1;
            PhotonNetwork.player.SetCustomProperties(hashtable);

            if (GameEvents.instance != null)
                GameEvents.instance.Trigger(new GameEventTypes.PositionEvent(view.gameObject.transform.position, "OnPlayerScoreItem"));
        }

        // Network destroy the item
        PhotonNetwork.Destroy(view.gameObject);
        // TODO: Add sound effect or particle or whatever when item is destroyed?

        // Notifiy all players of the item being thrown away
        if(GameControl.instance.IsGameState(GameControl.GameStates.Game)) // Throw only valid if in Game state
            PhotonView.Get(PlayerSpawner.instance.GetPlayerObject().gameObject).RPC("OnItemThrownAwayRPC", PhotonTargets.All);

    }
}
