﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillfeedItem : MonoBehaviour {

    public Text LeftPlayerText;
    public Image ActionImage;
    public UnityEngine.UI.Outline ActionImageOutline;
    public Text RightPlayerText;
}
