﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XPControl : MonoBehaviour {

    public static XPControl instance;

    int m_throws = 0;
    int m_kills = 0;

    // Data used by the level progress bar, has no "real" functionality
    float m_curXp = 0f;
    float m_xpGain = 0f;

    private void Awake()
    {
        instance = this;
    }

    void Start ()
    {
        GameEvents.instance.AddListener("OnGameStateGame", OnGameStateGame);
        GameEvents.instance.AddListener("OnPlayerScoreItem", OnPlayerScoreItem);
        GameEvents.instance.AddListener("OnPlayerScoreKill", OnPlayerScoreKill);
    }
	
    void OnGameStateGame(GameEvents.Event eventData)
    {
        m_throws = 0;
        m_kills = 0;
    }

    void OnPlayerScoreItem(GameEvents.Event eventData)
    {
        if(OptionsControl.options.xpGain)
            ++m_throws;
    }
    void OnPlayerScoreKill(GameEvents.Event eventData)
    {
        if (OptionsControl.options.xpGain)
            ++m_kills;
    }

    public void SetProgressBarData(float curXp, float xpGain)
    {
        m_curXp = curXp;
        m_xpGain = xpGain;
    }
    public float GetProgressBarCurXP()
    {
        return m_curXp;
    }
    public float GetProgressBarXPGain()
    {
        return m_xpGain;
    }

    public float GetXPGained()
    {
        // Some sanity checks
        string team = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
        if (team == ConstantVars.SpectatorTeamName || team == ConstantVars.InvisibleTeamName)
            m_kills = 0;
        if (team == ConstantVars.SpectatorTeamName || team == ConstantVars.GuardTeamName)
            m_throws = 0;

        if (m_kills > PhotonNetwork.room.PlayerCount)
            m_kills = PhotonNetwork.room.PlayerCount;
        if (m_throws > Utility.GetPhotonRoomProp<int>(ConstantVars.RoomItemsRequired_Key, 0))
            m_throws = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomItemsRequired_Key, 0);

        return (float)m_kills * 10f + (float)m_throws;
    }
    public void ResetXPGained()
    {
        m_kills = 0;
        m_throws = 0;
    }

    /// <summary>
    /// Whether or not the current room allows for XP gain
    /// </summary>
    /// <returns>True if the room allows for XP gain, false otherwise</returns>
    public static bool IsXPGainAllowed()
    {
        return true; // TODO: REALLY FUCKING REMOVE THIS EVENTUALLY
        if (!PhotonNetwork.inRoom)
            return false;

        // You need 3 or more players
        if (PhotonNetwork.room.PlayerCount <= 2)
            return false;

        bool hasInvisPlayer = false;
        bool hasGuardPlayer = false;
        foreach(PhotonPlayer player in PhotonNetwork.playerList)
        {
            string team = Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
            if (team == ConstantVars.InvisibleTeamName)
                hasInvisPlayer = true;
            else if (team == ConstantVars.GuardTeamName)
                hasGuardPlayer = true;

            if (hasInvisPlayer && hasGuardPlayer)
                break;
        }

        // There needs to be atleast one player on each team
        if (!hasInvisPlayer || !hasGuardPlayer)
            return false;

        return true;
    }
    public static float XPPerLevel()
    {
        return 150f;
    }

    // Current level from your current XP
    public static int CalculateLevel(float xp)
    {
        return Mathf.FloorToInt(xp / XPPerLevel()) + 1;
    }

    // XP needed until you level up
    public static float CalculateXPNeeded(float xp)
    {
        int curLevel = CalculateLevel(xp);

        // Becuase first level is 1
        // e.g, level 2 needs 150 xp (1 * 150)
        //      level 3 needs 300 xp (2 * 150)
        // (in these examples XPPerLevel() is 150f
        float nextLevelXp = (float)curLevel * XPPerLevel();
        return nextLevelXp - xp;
    }

    public static Color GetLevelColor(int level)
    {
        if (level <= 9) // 0 - 9 = White
            return Color.white;
        else if (level <= 19) // 10 - 19 = Green
            return Color.green;
        else if (level <= 49) // 20 - 49 = Cyan
            return Color.cyan;
        else                    // 50+ = Red
            return Color.red;
    }

    public static string GetColoredLevelString(int level)
    {
        Color color = GetLevelColor(level);
        return "<color=#" + ColorUtility.ToHtmlStringRGBA(color) + ">" + level.ToString() + "</color>";
    }
}
