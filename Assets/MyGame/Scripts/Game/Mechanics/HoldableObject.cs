﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class HoldableObject : MonoBehaviour, IPunObservable {


    public bool RequiresInteraction = false;
    public GameControl.ItemDensities ItemDensityGroup = GameControl.ItemDensities.High;

    const int Flag_HoldingObject = 1;
    const int Flag_InteractedWith = 2;
    const int Flag_ThrownRecently = 3;

    bool m_imHoldingIt = false;

    PhotonPlayer m_prevOwner = null;

    BitSet m_dataMask = new BitSet();

    float m_prevVelocity = 0f;

    [HideInInspector]
    public bool IsHoldingObject
    {
        get
        {
            return m_dataMask.GetBit(Flag_HoldingObject);
        }
        set
        {
            if(IsLocallyOwned())
            {
                m_dataMask.SetBit(Flag_InteractedWith, true);
                m_dataMask.SetBit(Flag_HoldingObject, value);
                m_imHoldingIt = value;

                UpdateRigidBodyLocks();
            }
        }
    }

    [HideInInspector]
    public Rigidbody RBody;

    PhotonView m_photonView;
    private void Awake()
    {
        m_photonView = GetComponent<PhotonView>();
    }
    void Start()
    {
        RBody = GetComponent<Rigidbody>();
        m_dataMask.SetBit(Flag_InteractedWith, !RequiresInteraction);
        UpdateRigidBodyLocks();
    }
    private void OnDestroy()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.Trigger(new GameEventTypes.PositionEvent(transform.position, "OnGameItemDestroyed"));
    }

    void UpdateRigidBodyLocks()
    {
        if (RBody == null)
            return;

        // If we're not holding it and we own it, then the holding flag should be false
        if (!m_imHoldingIt && IsLocallyOwned() && IsHoldingObject)
            m_dataMask.SetBit(Flag_HoldingObject, false);


        RBody.freezeRotation = IsHoldingObject || !m_dataMask.GetBit(Flag_InteractedWith);
        RBody.useGravity = !(IsHoldingObject || !m_dataMask.GetBit(Flag_InteractedWith));
    }

    public void SetThrowStatus(bool recentlyThrown)
    {
        if (!IsLocallyOwned())
            return;

        m_dataMask.SetBit(Flag_ThrownRecently, recentlyThrown);
    }



    void FixedUpdate()
    {
        m_prevVelocity = RBody.velocity.magnitude;

        if(m_prevOwner != m_photonView.owner)
        {
            m_prevOwner = m_photonView.owner;
            UpdateRigidBodyLocks();
        }

        if (!IsLocallyOwned())
            return;

        // If velocity falls below the minimum stun velocity, the throw status is reset
        if (RBody.velocity.magnitude < ConstantVars.ItemMinStunVelocity)
            m_dataMask.SetBit(Flag_ThrownRecently, false);

    }

    void OnCollisionEnter(Collision other)
    {
        if (!IsLocallyOwned())
            return;

        // If we are touched by another rigidbody, that counts as "interaction"
        if(other.rigidbody != null)
        {
            m_dataMask.SetBit(Flag_InteractedWith, true);
            UpdateRigidBodyLocks();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (!IsLocallyOwned())
            return;

        if (other.gameObject.CompareTag("Player") && m_dataMask.GetBit(Flag_ThrownRecently))
        {
            m_dataMask.SetBit(Flag_ThrownRecently, false);

            GuardPlayerInput guardPlayer = other.gameObject.GetComponentInParent<GuardPlayerInput>();
            if (guardPlayer != null)
                guardPlayer.OnHitByItem(m_prevVelocity, m_photonView.viewID);
        }
    }

    public bool IsLocallyOwned()
    {
        return m_photonView != null && m_photonView.isMine;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
            stream.SendNext(m_dataMask.AsByte());
        else if (stream.isReading)
        {
            m_dataMask.FromByte((byte)stream.ReceiveNext());
            UpdateRigidBodyLocks();
        }
    }
}
