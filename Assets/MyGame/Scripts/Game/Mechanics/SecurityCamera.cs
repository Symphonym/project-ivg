﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityCamera : MonoBehaviour {

    public Camera SourceCamera;

    public Renderer DisplayRenderer;
    public int DisplayMaterialIndex;

    public Vector3 StartAngle;
    public Vector3 EndAngle;
    public float PanDuration = 5f;
    public float PauseTime = 1f;

    public float NetworkedLerpSpeed = 5f;

    RenderTexture m_renderTex;

	// Use this for initialization
	void Start () {
        m_renderTex = new RenderTexture(256, 256, 16, RenderTextureFormat.Default);
        m_renderTex.Create();

        DisplayRenderer.materials[DisplayMaterialIndex].mainTexture = m_renderTex;
        DisplayRenderer.materials[DisplayMaterialIndex].SetTexture("_EmissionMap", m_renderTex);
        SourceCamera.targetTexture = m_renderTex;
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (!PhotonNetwork.inRoom || !PhotonNetwork.connected)
            return;

        // How many seconds the game has been progressing
        double networkTime = PhotonNetwork.time;

        float segmentDuration = (PanDuration + PauseTime);
        int segmentIndex = 1 + Mathf.FloorToInt((float)networkTime / segmentDuration);
        float segmentElapsed = (float)networkTime - (float)(segmentIndex - 1) * segmentDuration;
        float panRatio;

        // Segment structure
        // [-- PAN -- PAUSE --]
        // Timeline example
        // 0                   1                   2
        // [-- PAN -- PAUSE --][-- PAN -- PAUSE --][-- PAN -- PAUSE --]...

        // Even segment index - Pan from END to START
        if (segmentIndex % 2 == 0)
            panRatio = 1f - Mathf.Clamp01(segmentElapsed / PanDuration);

        // Uneven segment index - Pan from START to END
        else
            panRatio = Mathf.Clamp01(segmentElapsed / PanDuration);

        Vector3 newAngle = new Vector3(
            Mathf.LerpAngle(StartAngle.x, EndAngle.x, panRatio),
            Mathf.LerpAngle(StartAngle.y, EndAngle.y, panRatio),
            Mathf.LerpAngle(StartAngle.z, EndAngle.z, panRatio));
        SourceCamera.transform.rotation =
            Quaternion.Lerp(
                SourceCamera.transform.rotation,
                Quaternion.Euler(newAngle),
                Time.deltaTime * NetworkedLerpSpeed);
    }
}
