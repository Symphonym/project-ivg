﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecurityTrigger : MonoBehaviour {

    [SerializeField]
    SecurityTriggerHandler m_triggerTarget;

    private void Start()
    {
        m_triggerTarget.InitializeHandler(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        m_triggerTarget.OnSecurityTriggerEnter(other);
    }
}
