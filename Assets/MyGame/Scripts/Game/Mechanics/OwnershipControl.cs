﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OwnershipControl : MonoBehaviour {

    public delegate void OwnershipDelegate(OwnershipControl control);
    public event OwnershipDelegate OnOwnershipGiven;

    bool m_allowedOwnershipTransfer = true;
    double m_lastRequestTime = 0;

    // How long before an ownership request can be made again
    const double RequestCooldown = 1f;

    PhotonView m_photonView;

    // Use this for initialization
    void Awake ()
    {
        m_photonView = PhotonView.Get(this);
	}

    public void RequestOwnership()
    {
        // Assuming we are given the ownership, deny transfer by default (This is only set locally)
        SetOwnershipTransferAllowed(false);

        // Owning client doesn't have to make an ownership request
        if (m_photonView.isMine)
            OnOwnershipGiven(this);
        else
        {
            // Making an ownership request has a cooldown to make sure requests can't be spammed
            if (PhotonNetwork.time - m_lastRequestTime >= RequestCooldown)
            {
                m_lastRequestTime = PhotonNetwork.time;
                m_photonView.RequestOwnership();
                PhotonNetwork.SendOutgoingCommands();
            }
        }

    }

    public void SetOwnershipTransferAllowed(bool allowed)
    {
        //if (m_photonView.isMine)
        m_allowedOwnershipTransfer = allowed;
    }

    [PunRPC]
    void NotifyNewOwnerRPC()
    {
        if(OnOwnershipGiven != null)
            OnOwnershipGiven(this);
    }

    void OnOwnershipRequest(object[] viewAndPlayer)
    {
        PhotonView view = (PhotonView)viewAndPlayer[0];

        // Only the relevant item will handle the request
        OwnershipControl ownerControl = view.GetComponent<OwnershipControl>();
        if (ownerControl == null || !ownerControl.m_photonView.Equals(m_photonView))
            return;

        // Just make sure a non-owner isn't transferring ownership
        if (!view.isMine)
            return;

        PhotonPlayer requestingPlayer = (PhotonPlayer)viewAndPlayer[1];

        // Quick sanity check, player requesting an item should be reasonably close to it
        int requestingPlayerViewId = Utility.GetPhotonPlayerProp<int>(requestingPlayer, ConstantVars.PlayerObject_Key, -1);
        if (requestingPlayerViewId < 0)
            return;

        PhotonView requestingPlayerView = PhotonView.Find(requestingPlayerViewId);
        if (requestingPlayerView == null)
            return;

        if (Vector3.Distance(requestingPlayerView.transform.position, view.transform.position) > 10f)
            return;


        // Check if ownership transfer is currently allowed
        if (ownerControl.m_allowedOwnershipTransfer)
        {
            view.TransferOwnership(requestingPlayer.ID);

            // Let the new owner know they became the owner
            view.RPC("NotifyNewOwnerRPC", requestingPlayer);
            PhotonNetwork.SendOutgoingCommands(); // Force RPC to be sent
        }
    }
}
