﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpectatorCamText : MonoBehaviour
{
    public Text TitleText;
    public Text SubTitleText;
}
