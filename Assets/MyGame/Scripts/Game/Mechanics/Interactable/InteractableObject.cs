﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractableObject : MonoBehaviour {

    public string InfoText = "Press # to interact";
    public float InteractionDelay = 0f;
    public bool ImmediatelyResetInteraction = false;
    public bool EnableForTeamInvisible = true;
    public bool EnableForTeamGuard = true;

    private void Start()
    {
        GameEvents.instance.AddListener("OnGameStateChanged", OnGameStateChanged);
    }

    public virtual void OnInteractStart(GameObject playerObj)
    {

    }

    public virtual void OnInteractEnd(GameObject playerObj)
    {

    }
    public virtual void OnInteracting(GameObject playerObj, RaycastHit hit)
    {

    }

    public virtual void OnHoverStart(GameObject playerObj)
    {

    }
    public virtual void OnHoverEnd(GameObject playerObj)
    {

    }

    void OnGameStateChanged(GameEvents.Event eventData)
    {
        if (!PhotonNetwork.inRoom)
            return;

        if (!GameControl.instance.IsGameState(GameControl.GameStates.Game))
        {
            OnHoverEnd(null);
            OnInteractEnd(null);
        }
    }
}
