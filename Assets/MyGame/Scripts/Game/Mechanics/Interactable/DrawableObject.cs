﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class DrawableObject : InteractableObject, IPunObservable {

    [SerializeField]
    Renderer m_targetAlbedo;

    [SerializeField]
    Material m_unlitMaterial;

    [SerializeField]
    Material m_brushStroke;

    public float BrushStrokeSize = 0.2f;
    public float BrushStrokeDelay = 0.1f;
    public int BrushStrokeLimit = 500;

    Camera m_paintCamera;
    RenderTexture m_paintTexture;
    GameObject m_albedoQuad;
    PhotonView m_photonView;

    float m_brushElapsed = 0f;
    List<GameObject> m_brushStrokes = new List<GameObject>();
    Vector2[] m_brushPositions;
    int m_brushIndex = 0;

    bool m_forceResync = true;

    class LocalBrushStroke
    {
        public float timeStamp;
        public GameObject brushStroke;
        public LocalBrushStroke(float timeStamp, GameObject brushStroke)
        {
            this.timeStamp = timeStamp;
            this.brushStroke = brushStroke;
        }
    }
    List<LocalBrushStroke> m_localBrushStrokes = new List<LocalBrushStroke>();

    private void Awake()
    {
        m_photonView = GetComponent<PhotonView>();

        if (m_targetAlbedo.material.mainTexture == null)
            m_paintTexture = new RenderTexture(512, 512, 24);
        else
            m_paintTexture = new RenderTexture(
                m_targetAlbedo.material.mainTexture.width,
                m_targetAlbedo.material.mainTexture.height, 24);


        GameObject paintCamera = new GameObject("Paint Camera (" + gameObject.name + ")");
        m_paintCamera = paintCamera.AddComponent<Camera>();
        m_paintCamera.transform.position = transform.position;
        m_paintCamera.depth = -100;
        m_paintCamera.targetTexture = m_paintTexture;
        m_paintCamera.clearFlags = CameraClearFlags.Depth;
        m_paintCamera.orthographic = true;
        m_paintCamera.orthographicSize = 0.5f;
        m_paintCamera.cullingMask = LayerMask.GetMask("DrawableObjectTexture");

        m_albedoQuad = GameObject.CreatePrimitive(PrimitiveType.Quad);
        m_albedoQuad.transform.SetParent(m_paintCamera.transform);
        m_albedoQuad.transform.localPosition = new Vector3(0, 0f, m_paintCamera.nearClipPlane + 3f);
        m_albedoQuad.GetComponent<Renderer>().material = m_unlitMaterial;
        m_albedoQuad.GetComponent<Renderer>().material.mainTexture = m_targetAlbedo.material.mainTexture;
        m_albedoQuad.gameObject.layer = LayerMask.NameToLayer("DrawableObjectTexture");

        m_targetAlbedo.material.mainTexture = m_paintTexture;

        m_brushPositions = new Vector2[BrushStrokeLimit];
        for (int i = 0; i < m_brushPositions.Length; i++)
            m_brushPositions[i] = Vector2.one * -100;
    }


    public override void OnInteracting(GameObject playerObj, RaycastHit hit)
    {
        m_brushElapsed += Time.deltaTime;

        if (m_brushElapsed >= BrushStrokeDelay)
        {
            m_brushElapsed = 0f;

            if (hit.transform.gameObject != m_targetAlbedo.gameObject)
                return;

            PhotonPlayer owner = m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner;

            m_photonView.RPC("DrawBrushStrokeRPC", owner, hit.textureCoord);
            PhotonNetwork.SendOutgoingCommands();
            DrawLocalBrush(hit.textureCoord);
        }
    }

    GameObject CreateBrushStroke(string name)
    {
        GameObject newBrush = GameObject.CreatePrimitive(PrimitiveType.Quad);
        newBrush.layer = LayerMask.NameToLayer("DrawableObjectTexture");
        newBrush.name = name;
        newBrush.transform.SetParent(m_albedoQuad.transform);
        newBrush.transform.localScale = Vector3.one * BrushStrokeSize;

        Renderer ren = newBrush.GetComponent<Renderer>();
        ren.sharedMaterial = m_brushStroke;
        ren.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        ren.receiveShadows = false;

        return newBrush;
    }

    void DrawBrushStroke(Vector2 uvCoords)
    {
        if (m_brushIndex >= BrushStrokeLimit)
            m_brushIndex = 0;

        if(m_brushIndex >= m_brushStrokes.Count)
        {
            GameObject newBrush = CreateBrushStroke("Brush stroke " + m_albedoQuad.transform.childCount.ToString());
            m_brushStrokes.Add(newBrush);
        }

        GameObject brushStroke = m_brushStrokes[m_brushIndex];
        brushStroke.transform.localPosition = GetBrushPosition(uvCoords);

        if (m_photonView.isMine)
        {
            // Sync the brush stroke with remote clients
            m_brushPositions[m_brushIndex] = uvCoords;

            // Also make sure we don't apply any local brush strokes since we don't need them as the owner
            ClearAllLocalBrushes();
        }
        else
        {

            for(int i = 0; i < m_localBrushStrokes.Count; i++)
            {
                // If the brush stroke we received was one we sent previously, erase the local brush stroke
                // since it has now been synced.
                if (m_localBrushStrokes[i].brushStroke.transform.localPosition == brushStroke.transform.localPosition)
                {
                    Destroy(m_localBrushStrokes[i].brushStroke);
                    m_localBrushStrokes.RemoveAt(i);
                    break;
                }

                // In the case a local brush stays around for a bit too long, just remove it
                else if(Time.time - m_localBrushStrokes[i].timeStamp >= 3f)
                {
                    Destroy(m_localBrushStrokes[i].brushStroke);
                    m_localBrushStrokes.RemoveAt(i);
                    --i;
                }
            }
        }

        ++m_brushIndex;
    }

    void DrawLocalBrush(Vector2 uvCoords)
    {
        if (m_photonView.isMine || !PhotonNetwork.inRoom)
            return;

        // We create a local brush that auto-destroys itself after the round-trip time, after which
        // the brush is hopefully synced back and forth with the owning client.
        GameObject localBrush = CreateBrushStroke("Local brush");
        localBrush.transform.localPosition = GetBrushPosition(uvCoords);
        m_localBrushStrokes.Add(new LocalBrushStroke(Time.time, localBrush));
    }

    void ClearAllLocalBrushes()
    {
        while (m_localBrushStrokes.Count > 0)
        {
            Destroy(m_localBrushStrokes[0].brushStroke);
            m_localBrushStrokes.RemoveAt(0);
        }
    }

    [PunRPC]
    void DrawBrushStrokeRPC(Vector2 uvCoords)
    {
        DrawBrushStroke(uvCoords);
    }

    void UpdateBrushes(Vector2[] newBrushes, int newBrushIndex)
    {
        if (m_forceResync)
        {
            m_brushIndex = 0;
            for(int i = 0; i < newBrushes.Length; i++)
            {
                m_brushPositions[i] = newBrushes[i];
                DrawBrushStroke(m_brushPositions[i]);
            }
            m_brushIndex = newBrushIndex;
            m_forceResync = false;
        }
        else
        {
            // Draw all brush strokes we've missed, from our brush index to the remote brush index
            while(m_brushIndex != newBrushIndex)
            {
                if (m_brushIndex >= BrushStrokeLimit)
                    m_brushIndex = 0;

                m_brushPositions[m_brushIndex] = newBrushes[m_brushIndex];
                DrawBrushStroke(m_brushPositions[m_brushIndex]);
            }
        }
    }

    Vector3 GetBrushPosition(Vector2 uvCoords)
    {
        return new Vector3(-0.5f, -0.5f, 0f) + new Vector3(uvCoords.x, uvCoords.y, -1f);
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if(stream.isWriting)
        {
            stream.SendNext(m_brushPositions);
            stream.SendNext(m_brushIndex);
        }
        else
            UpdateBrushes((Vector2[])stream.ReceiveNext(), (int)stream.ReceiveNext());
    }
}
