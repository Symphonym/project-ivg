﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReloadObject : InteractableObject {

    GuardPlayerInput GetGuardPlayer(GameObject playerObj)
    {
        if (playerObj == null)
            return null;

        GuardPlayerInput guardPlayer = playerObj.GetComponent<GuardPlayerInput>();
        if (guardPlayer == null)
            return null;

        return guardPlayer;
    }

    public override void OnInteractStart(GameObject playerObj)
    {
        GuardPlayerInput guardPlayer = GetGuardPlayer(playerObj);

        if (guardPlayer != null)
            guardPlayer.Reload();

    }
    public override void OnHoverStart(GameObject playerObj)
    {
        GuardPlayerInput guardPlayer = GetGuardPlayer(playerObj);

        if(guardPlayer != null)
            guardPlayer.SetReloadAvailable(true);
    }
    public override void OnHoverEnd(GameObject playerObj)
    {
        GuardPlayerInput guardPlayer = GetGuardPlayer(playerObj);

        if (guardPlayer != null)
            guardPlayer.SetReloadAvailable(false);

    }
}
