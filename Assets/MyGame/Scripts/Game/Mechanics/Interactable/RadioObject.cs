﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class RadioObject : InteractableObject, IPunObservable {

    [SerializeField]
    AudioSource m_music;

    [SerializeField]
    Light m_powerLight;

    PhotonView m_photonView;
    bool m_playing = false;


    // Use this for initialization
    void Awake ()
    {
        m_photonView = PhotonView.Get(this);	
	}

    private void Start()
    {
        UpdateMusic();
    }

    private void OnEnable()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.AddListener("OnGameStateLobby", OnGameStateLobby);
    }
    private void OnDisable()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.RemoveListener("OnGameStateLobby", OnGameStateLobby);
    }



    public override void OnInteractStart(GameObject playerObj)
    {
        // Send RPC to owner of light switch and make them toggle it
        m_photonView.RPC("ToggleMusicRPC", m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner);
    }

    private void UpdateMusic()
    {
        if (m_music.isPlaying && !m_playing)
            m_music.Pause();
        else if (!m_music.isPlaying && m_playing)
            m_music.Play();

        if (m_powerLight.gameObject.activeSelf && !m_playing)
            m_powerLight.gameObject.SetActive(false);
        else if (!m_powerLight.gameObject.activeSelf && m_playing)
            m_powerLight.gameObject.SetActive(true);
    }

    [PunRPC]
    public void ToggleMusicRPC()
    {
        if (!m_photonView.isMine)
            return;

        m_playing = !m_playing;
        UpdateMusic();
    }

    void OnGameStateLobby(GameEvents.Event eventData)
    {
        // Turn off music when returning to lobby
        if (m_photonView.isMine)
        {
            m_playing = false;
            UpdateMusic();
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
            stream.SendNext(m_playing);
        else
        {
            m_playing = (bool)stream.ReceiveNext();
            UpdateMusic();
        }
    }
}
