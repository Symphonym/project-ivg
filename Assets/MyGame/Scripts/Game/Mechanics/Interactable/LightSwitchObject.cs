﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class LightSwitchObject : InteractableObject, IPunObservable {


    public bool IsOn = true;
    bool m_prevIsOn = true;

    [SerializeField]
    List<Light> m_associatedLights = new List<Light>();

    [SerializeField]
    AudioSource m_flickSound;

    PhotonView m_photonView;

    void Awake()
    {
        m_photonView = PhotonView.Get(this);
    }
    private void Start()
    {
        m_prevIsOn = !IsOn;
        UpdateLights(false);
    }

    void UpdateLights(bool withSound = true)
    {
        if(m_prevIsOn != IsOn)
        {
            if(withSound)
                m_flickSound.Play();

            foreach (Light light in m_associatedLights)
                light.enabled = IsOn;
        }

        m_prevIsOn = IsOn;
    }

    public override void OnInteractStart(GameObject playerObj)
    {
        // Send RPC to owner of light switch and make them toggle it
        m_photonView.RPC("ToggleLightRPC", m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner);
    }

    [PunRPC]
    public void ToggleLightRPC()
    {
        if (!m_photonView.isMine)
            return;

        IsOn = !IsOn;

        UpdateLights();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
            stream.SendNext(IsOn);
        else
            IsOn = (bool)stream.ReceiveNext();

        UpdateLights();
    }
}
