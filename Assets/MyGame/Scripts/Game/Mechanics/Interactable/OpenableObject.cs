﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PhotonView))]
public class OpenableObject : InteractableObject, IPunObservable {

    public Collider TargetCollider;
    public Transform TargetHinge;
    public float OpenAngleOffset = 90;
    public float TransitionDuration = 0.5f;

    [SerializeField]
    AudioSource m_doorAudioSource;

    [SerializeField]
    AudioClip m_openSound;

    [SerializeField]
    AudioClip m_closeSound;

    bool m_closed = true;

    PhotonView m_photonView;
    float m_objectAngle = 0f;

    enum OpeningStates
    {
        Opening,
        Closing,
        Open,
        Closed
    };
    OpeningStates m_state = OpeningStates.Closed;
    IEnumerator m_transitionCR = null;

    Quaternion m_closedAngle;
    Quaternion m_openAngle;

    // Use this for initialization
    void Awake () {
        m_photonView = PhotonView.Get(this);

        m_state = m_closed ? OpeningStates.Closed : OpeningStates.Open;
        m_closedAngle = TargetHinge.transform.localRotation;
        m_openAngle = m_closedAngle * Quaternion.Euler(0, OpenAngleOffset, 0);
        TargetCollider.isTrigger = !m_closed;
        TargetHinge.localRotation = m_closed ? m_closedAngle : m_openAngle;
    }

    IEnumerator CR_Transition(OpeningStates targetState)
    {
        if (targetState == OpeningStates.Closed)
            m_state = OpeningStates.Closing;
        else if (targetState == OpeningStates.Open)
            m_state = OpeningStates.Opening;

        if (m_doorAudioSource != null)
        {
            m_doorAudioSource.clip = m_openSound;
            m_doorAudioSource.Play();
        }

        // Disable collision during transition
        TargetCollider.isTrigger = true;


        // Offset elapsed time so we start at 0
        float elapsed = -Time.deltaTime;
        AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, TransitionDuration, 1f);

        do
        {
            elapsed += Time.deltaTime;
            float ratio = curve.Evaluate(elapsed);

            if (targetState == OpeningStates.Closed)
                TargetHinge.localRotation = Quaternion.Slerp(m_openAngle, m_closedAngle, ratio);
            else if (targetState == OpeningStates.Open)
                TargetHinge.localRotation = Quaternion.Slerp(m_closedAngle, m_openAngle, ratio);

            yield return null;
        } while (elapsed < TransitionDuration);

        TargetCollider.isTrigger = false;
        m_transitionCR = null;

        if (targetState == OpeningStates.Closed)
        {
            if(m_doorAudioSource != null)
            {
                m_doorAudioSource.clip = m_closeSound;
                m_doorAudioSource.Play();
            }
            m_state = OpeningStates.Closed;
        }
        else if (targetState == OpeningStates.Open)
            m_state = OpeningStates.Open;

        yield break;
    }

    void StartTransition(OpeningStates targetState)
    {
        // Open and Closed are the only valid target states for a transition
        if (targetState != OpeningStates.Open && targetState != OpeningStates.Closed)
            return;

        if(m_transitionCR != null)
        {
            StopCoroutine(m_transitionCR);
            m_transitionCR = null;
        }

        // To signify the state the object should be in on all remote clients
        if(m_photonView.isMine)
            m_closed = targetState == OpeningStates.Closed;

        m_transitionCR = CR_Transition(targetState);
        StartCoroutine(m_transitionCR);
    }

    void UpdateState()
    {
        if(m_closed)
        {
            if(m_state != OpeningStates.Closed && m_state != OpeningStates.Closing)
                StartTransition(OpeningStates.Closed);
        }
        else
        {
            if (m_state != OpeningStates.Open && m_state != OpeningStates.Opening)
                StartTransition(OpeningStates.Open);
        }
    }

    public override void OnInteractStart(GameObject playerObj)
    {
        if (m_state != OpeningStates.Closed && m_state != OpeningStates.Open)
            return;

        m_photonView.RPC("OpenOrCloseRPC", m_photonView.isMine ? PhotonNetwork.player : m_photonView.owner);

        if (!m_photonView.isMine)
            StartTransition(m_state == OpeningStates.Closed ? OpeningStates.Open : OpeningStates.Closed);
    }

    [PunRPC]
    public void OpenOrCloseRPC()
    {
        if (!m_photonView.isMine || m_state == OpeningStates.Opening || m_state == OpeningStates.Closing)
            return;

        m_closed = !m_closed;
        UpdateState();
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(m_objectAngle);
            stream.SendNext(m_closed);
        }
        else
        {
            m_objectAngle = (float)stream.ReceiveNext();
            m_closed = (bool)stream.ReceiveNext();

            UpdateState();
        }
    }
}
