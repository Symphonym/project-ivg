﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillfeedControl : MonoBehaviour {

    public static KillfeedControl instance;

    public Transform KillfeedParent;
    public KillfeedItem KillfeedPrefab;

    public Sprite KillSprite;
    public Sprite ThrowSprite;
    public Sprite StunSprite;

    public float TransitionDuration;

    [Tooltip("How many killfeed objects to cache in a pool for reuse")]
    public int PoolSize = 5;
   
    List<KillfeedItem> m_killfeed = new List<KillfeedItem>();
    List<KillfeedItem> m_killfeedPool = new List<KillfeedItem>();

    void Awake()
    {
        instance = this;
    }

    public void SpawnKillfeedInvisible(PhotonPlayer invisiblePlayer)
    {
        KillfeedItem item = CreateKillfeedItem();
        item.LeftPlayerText.text = Utility.ParseChatPlayerName(invisiblePlayer);

        item.ActionImage.sprite = ThrowSprite;
        item.ActionImageOutline.effectColor = ConstantVars.InvisibleColor;

        item.RightPlayerText.gameObject.SetActive(false);
        item.LeftPlayerText.gameObject.SetActive(true);

        item.transform.SetAsFirstSibling();
        m_killfeed.Add(item);

        StartCoroutine(CR_Transition(item, true));
    }
    public void SpawnKillfeedGuard(PhotonPlayer guardPlayer, PhotonPlayer invisiblePlayer)
    {
        KillfeedItem item = CreateKillfeedItem();
        item.LeftPlayerText.text = Utility.ParseChatPlayerName(guardPlayer);

        item.ActionImage.sprite = KillSprite;
        item.ActionImageOutline.effectColor = ConstantVars.GuardColor;

        item.RightPlayerText.text = Utility.ParseChatPlayerName(invisiblePlayer);

        item.RightPlayerText.gameObject.SetActive(true);
        item.LeftPlayerText.gameObject.SetActive(true);

        item.transform.SetAsFirstSibling();
        m_killfeed.Add(item);

        StartCoroutine(CR_Transition(item, true));
    }

    public void SpawnKillfeedStun(PhotonPlayer stunner, PhotonPlayer stunee)
    {
        Color stunnerColor = ConstantVars.NeutralColor;
        string stunnerTeam = Utility.GetPhotonPlayerProp<string>(stunner, ConstantVars.PlayerTeam_Key, ConstantVars.InvisibleTeamName);

        if (stunnerTeam == ConstantVars.GuardTeamName)
            stunnerColor = ConstantVars.GuardColor;
        else if (stunnerTeam == ConstantVars.InvisibleTeamName)
            stunnerColor = ConstantVars.InvisibleColor;

        KillfeedItem item = CreateKillfeedItem();
        item.LeftPlayerText.text = Utility.ParseChatPlayerName(stunner);

        item.ActionImage.sprite = StunSprite;
        item.ActionImageOutline.effectColor = stunnerColor;

        item.RightPlayerText.text = Utility.ParseChatPlayerName(stunee);

        item.RightPlayerText.gameObject.SetActive(true);
        item.LeftPlayerText.gameObject.SetActive(true);

        item.transform.SetAsFirstSibling();
        m_killfeed.Add(item);

        StartCoroutine(CR_Transition(item, true));
    }

    KillfeedItem CreateKillfeedItem()
    {
        KillfeedItem item;

        // Reuse objects whenever possible
        if (m_killfeedPool.Count > 0)
        {
            item = m_killfeedPool[0];
            item.gameObject.SetActive(true);

            m_killfeedPool.RemoveAt(0);
        }
        else
            item = Instantiate(KillfeedPrefab, KillfeedParent, false);


        return item;
    }

    IEnumerator CR_Destroy(KillfeedItem item)
    {
        yield return new WaitForSeconds(2);
        yield return CR_Transition(item, false);
    }
    IEnumerator CR_Transition(KillfeedItem item, bool fadeIn)
    {
        float elapsed = -Time.deltaTime;
        Vector3 startScale = item.transform.localScale;
        AnimationCurve curve = AnimationCurve.EaseInOut(0, 0, TransitionDuration, 1f);

        do
        {
            elapsed += Time.deltaTime;
            float ratio = curve.Evaluate(elapsed);

            if (fadeIn)
                item.gameObject.transform.localScale = Vector3.Lerp(
                    new Vector3(0f, 0f, 0f),
                    startScale,
                    ratio);
            else
                item.gameObject.transform.localScale = Vector3.Lerp(
                    startScale,
                    new Vector3(0f, 0f, 0f),
                    ratio);

            if(elapsed >= TransitionDuration)
            {
                if(fadeIn)
                    StartCoroutine(CR_Destroy(item));
                else
                {
                    // If the pool isn't full, store the killfeed object there for reuse
                    if(m_killfeedPool.Count < PoolSize)
                    {
                        item.gameObject.SetActive(false);
                        item.gameObject.transform.localScale = startScale;
                        m_killfeedPool.Add(item);
                    }

                    // Otherwise destroy the object
                    else
                    {
                        Destroy(item.gameObject);
                        m_killfeed.Remove(item);
                    }
                }
                yield break;
            }
            yield return null;
        } while (elapsed < TransitionDuration);

        yield return null;
    }
}
