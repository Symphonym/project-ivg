﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreFeedback : MonoBehaviour {

    public float StayDuration = 2f;
    public float FadeDuration = 0.2f;
    public float PulseFreq = 1f;

    [Range(0f, 0.9f)]
    public float PulseAmp = 0.9f;

    [SerializeField]
    AudioSource m_scoreSound;

    [SerializeField]
    Sprite m_scoreSprite;

    [SerializeField]
    Sprite m_killSprite;

    [SerializeField]
    Image m_feedbackImage;


    private void Awake()
    {
        string team = Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
        if (team == ConstantVars.GuardTeamName)
            m_feedbackImage.sprite = m_killSprite;
        else
            m_feedbackImage.sprite = m_scoreSprite;
    }
    // Use this for initialization
    void Start ()
    {
        StartCoroutine(CR_ShowFeedback());
    }


    IEnumerator CR_ShowFeedback()
    {
        m_scoreSound.Play();
        float elapsed = -Time.deltaTime;

        do
        {
            elapsed += Time.deltaTime;
            m_feedbackImage.transform.localScale = Vector3.one + Vector3.one * Mathf.Sin(Time.time * PulseFreq) * PulseAmp;

            yield return null;
        } while (elapsed < StayDuration);

        elapsed = -Time.deltaTime;

        Vector3 startScale = m_feedbackImage.transform.localScale;

        AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, FadeDuration, 1f);
        do
        {
            elapsed += Time.deltaTime;
            float ratio = curve.Evaluate(elapsed);

            m_feedbackImage.transform.localScale = Vector3.Lerp(
                startScale,
                new Vector3(0.01f, 0.01f, 0.01f),
                ratio);

            yield return null;
        } while (elapsed < FadeDuration);

        Destroy(gameObject);
        yield break;
    }
}
