﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class RoomBoundary : MonoBehaviour {

    public string RoomName;

    Collider m_collider;
    //bool m_initialized = false;

	// Use this for initialization
	void Start () {
        m_collider = GetComponent<Collider>();

        // Make sure collider is a trigger
        m_collider.isTrigger = true;
	}
	
    void HandleTrigger(Collider other, bool entering)
    {
        // Ensure the colliding object is a player
        if (!other.CompareTag("Player"))
            return;

        PhotonView playerView = other.GetComponent<PhotonView>();
        
        // Ignore non-local players
        if ((playerView == null && other.GetComponent<SpectatorMovement>() == null) || (playerView != null && !playerView.isMine))
            return;

        if (entering)
            RoomBoundaryControl.instance.AddOverlapingCollider(RoomName, m_collider);
        else
            RoomBoundaryControl.instance.RemoveOverlappingCollider(RoomName, m_collider);
    }

    void OnTriggerEnter(Collider other)
    {
        HandleTrigger(other, true);
    }
    void OnTriggerExit(Collider other)
    {
        HandleTrigger(other, false);
    }
    /*void OnTriggerStay(Collider other)
    {
        // If the player starts inside a trigger
        if(!m_initialized)
        {
            HandleTrigger(other, true);
            m_initialized = true;
        }
    }*/
}
