﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreFeedbackSpawner : MonoBehaviour {

    [SerializeField]
    GameObject m_scoreFeedbackPrefab;

    private void OnEnable()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.AddListener("OnPlayerScoreItem", OnPlayerScoreItem);
    }
    private void OnDisable()
    {
        if (GameEvents.instance != null)
            GameEvents.instance.RemoveListener("OnPlayerScoreItem", OnPlayerScoreItem);
    }

    // Update is called once per frame
    void OnPlayerScoreItem(GameEvents.Event eventData)
    {
        GameEventTypes.PositionEvent posEvent = (GameEventTypes.PositionEvent)eventData;
        Instantiate(m_scoreFeedbackPrefab, posEvent.position, Quaternion.identity);
    }
}
