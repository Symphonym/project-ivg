﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PhotonView))]
public class SecurityAlarm : SecurityTriggerHandler, IPunObservable {

    public float AlarmDuration = 5f;
    public float AlarmSoundInterval = 1f;
    public float RotationSpeed = 10f;
    public float LightFadeDuration = 0.5f;
    public float NetworkedLerpSpeed = 5f;

    [SerializeField]
    AudioSource m_alarmSound;

    [SerializeField]
    Transform m_alarmLightParent;


    List<SecurityTrigger> m_laserBeams = new List<SecurityTrigger>();

    PhotonView m_photonView;

    double m_alarmEndTime;
    bool m_alarmActive = false;
    bool m_alarmActiveLocally = false;

    float m_alarmSoundElapsed = 0f;
    Quaternion m_remoteLightRotation;
    Quaternion m_alarmStartRotation = Quaternion.identity;

    List<Light> m_alarmLights = new List<Light>();
    List<float> m_lightStartSpotAngles = new List<float>();
    IEnumerator m_lightFadeCR = null;

    private void Awake()
    {
        m_photonView = PhotonView.Get(this);
        m_alarmStartRotation = m_alarmLightParent.transform.localRotation;
    }

    // Use this for initialization
    void Start ()
    {
        m_alarmEndTime = -1337f; // Just to make sure alarm doesn't trigger by default
        m_alarmLightParent.gameObject.SetActive(false);

        Light[] alarmLights = m_alarmLightParent.GetComponentsInChildren<Light>();
        foreach (Light light in alarmLights)
            m_alarmLights.Add(light);

        foreach (Light light in m_alarmLights)
            m_lightStartSpotAngles.Add(light.spotAngle);
	}

    public override void InitializeHandler(SecurityTrigger trigger)
    {
        m_laserBeams.Add(trigger);
    }

    // Update is called once per frame
    void Update ()
    {
        CheckAlarmStatus();

        if (m_alarmActive)
        {
            m_alarmSoundElapsed += Time.deltaTime;

            if(m_alarmSoundElapsed >= AlarmSoundInterval)
            {
                m_alarmSoundElapsed = 0f;
                m_alarmSound.Play();
            }
        }

        if(m_alarmLightParent.gameObject.activeSelf)
            m_alarmLightParent.transform.localRotation = Quaternion.AngleAxis((float)((double)(PhotonNetwork.time * RotationSpeed) % 360.0), Vector3.up) * m_alarmStartRotation;
	}
    void CheckAlarmStatus()
    {
        if (!PhotonNetwork.inRoom)
            return;

        bool isActive = (m_alarmEndTime - PhotonNetwork.time) > 0 || m_alarmActiveLocally;

        if(isActive != m_alarmActive)
        {
            m_alarmActive = isActive;

            foreach (SecurityTrigger laserBeam in m_laserBeams)
                laserBeam.gameObject.SetActive(!m_alarmActive);

            SetLightsVisible(m_alarmActive);
        }
    }
    void SetLightsVisible(bool visible)
    {
        if(visible)
        {
            m_alarmSoundElapsed = AlarmSoundInterval;

            // Stop light fading before making lights visible
            if(m_lightFadeCR != null)
            {
                StopCoroutine(m_lightFadeCR);
                m_lightFadeCR = null;
            }

            for(int i = 0; i < m_alarmLights.Count; i++)
                m_alarmLights[i].spotAngle = m_lightStartSpotAngles[i];

            m_alarmLightParent.gameObject.SetActive(true);
        }
        else
        {
            if (m_lightFadeCR == null)
            {
                m_lightFadeCR = CR_FadeLights();
                StartCoroutine(m_lightFadeCR);
            }
        }
    }

    public override void OnSecurityTriggerEnter(Collider other)
    {
        // Alarm can't be triggered when it's already active
        if (m_alarmActive)
            return;

        if (other.gameObject.CompareTag("GameItem"))
        {
            // Game items can only trigger the alarm if they're over a certain speed
            // This is to prevent the INVISIBLE from placing items on the trigger and
            // idefinitely triggering the alarm without nobobdy there.
            Rigidbody rbody = other.attachedRigidbody;
            if (rbody != null && rbody.velocity.magnitude < 0.2f)
                return;
        }
        else if (other.gameObject.CompareTag("Player"))
        {
            PhotonView playerView = other.gameObject.GetComponent<PhotonView>();
            if (playerView == null)
                return;

            PhotonPlayer player = playerView.isMine ? PhotonNetwork.player : playerView.owner;

            string playerTeam = Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
            bool alive = Utility.GetPhotonPlayerProp<bool>(player, ConstantVars.PlayerAlive_Key, true);

            // A dead INVISIBLE player, a GUARD or a SPECTATOR will not trigger the alarm
            if ((playerTeam == ConstantVars.InvisibleTeamName && !alive) ||
                (playerTeam == ConstantVars.GuardTeamName) ||
                (playerTeam == ConstantVars.SpectatorTeamName))
                return;
        }

        // Only game items and players can trigger the alarm
        else
            return;

        // Owner will trigger the alarm
        if (m_photonView.isMine)
            TriggerAlarm();

        else
        {
            // If we own the object that triggered the alarm, we let the owner know since we have the most accurate representation
            // of the object.
            PhotonView owningView = other.GetComponent<PhotonView>();
            if(owningView != null && owningView.isMine)
                StartCoroutine(CR_LocalAlarmTrigger());
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
            stream.SendNext(m_alarmEndTime);
        else
            m_alarmEndTime = (double)stream.ReceiveNext();
    }

    void TriggerAlarm()
    {
        if (!m_photonView.isMine || m_alarmActive)
            return;

        m_alarmEndTime = PhotonNetwork.time + AlarmDuration;
    }
    [PunRPC]
    void TriggerAlarmRPC()
    {
        TriggerAlarm();
    }

    IEnumerator CR_LocalAlarmTrigger()
    {
        if (m_alarmActiveLocally)
            yield break;

        m_alarmActiveLocally = true;
        m_photonView.RPC("TriggerAlarmRPC", m_photonView.GetPhotonOwner());
        yield return new WaitForSeconds(1f);
        m_alarmActiveLocally = false;
        yield break;
    }

    IEnumerator CR_FadeLights()
    {
        float elapsed = -Time.deltaTime;
        do
        {
            elapsed += Time.deltaTime;
            float ratio = Mathf.Clamp01(elapsed / LightFadeDuration);

            for(int i = 0; i < m_alarmLights.Count; i++)
                m_alarmLights[i].spotAngle = Mathf.Lerp(m_lightStartSpotAngles[i], 0f, ratio);

            yield return null;
        } while (elapsed < LightFadeDuration);

        m_alarmLightParent.gameObject.SetActive(false);
        m_lightFadeCR = null;

        yield break;
    }
}
