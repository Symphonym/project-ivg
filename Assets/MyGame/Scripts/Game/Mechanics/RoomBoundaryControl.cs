﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomBoundaryControl : MonoBehaviour {

    public static RoomBoundaryControl instance;

    // Mapping of roomNames to all colliders (currently being overlapped) associated with that room
    Dictionary<string, List<Collider>> m_overlappingTriggers = new Dictionary<string, List<Collider>>();

    string m_currentRoom = null;

    Text m_roomNameText;

	// Use this for initialization
	void Awake ()
    {
        instance = this;
	}

    void Start()
    {
        //GameUIControl.instance.RoomNameText.gameObject.SetActive(true);
        m_roomNameText = GameUIControl.instance.GetUIObject<Text>("RoomName_Txt");
        GameUIControl.instance.SetUIObjectValidator("RoomName", () =>
        {
            return //(Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName) != ConstantVars.SpectatorTeamName) &&
                GameControl.instance.IsGameState(GameControl.GameStates.Game);
        });

    }

    void UpdateRoomText()
    {
        //GameUIControl.instance.RoomNameText.text = "<color=#4286F4>" + GetCurrentRoomName() +"</color>";
        m_roomNameText.text = GetCurrentRoomName();
    }

    public void AddOverlapingCollider(string roomName, Collider col)
    {
        List<Collider> overlappingColliders;
        if (m_overlappingTriggers.TryGetValue(roomName, out overlappingColliders))
            overlappingColliders.Add(col);
        else
        {
            m_overlappingTriggers[roomName] = new List<Collider>();
            m_overlappingTriggers[roomName].Add(col);
        }

        // If there is no current room, simply set it
        if (m_currentRoom == null)
        {
            m_currentRoom = roomName;
            UpdateRoomText();
        }
        else
            RefreshCurrentRoom();
    }
    public void RemoveOverlappingCollider(string roomName, Collider col)
    {
        List<Collider> overlappingColliders;
        if (m_overlappingTriggers.TryGetValue(roomName, out overlappingColliders))
            overlappingColliders.Remove(col);
        else
            Debug.LogError("Trying to remove collider from non-existant room, where's the add operation!?");

        RefreshCurrentRoom();
    }

    public string GetCurrentRoomName()
    {
        return m_currentRoom == null ? "???" : m_currentRoom;
    }
    public string GetRoomAt(Vector3 position)
    {
        Collider[] colliders = Physics.OverlapSphere(position, 1f, LayerMask.GetMask("CollideOnlyPlayer"));

        foreach(Collider c in colliders)
        {
            RoomBoundary roomBound = c.GetComponent<RoomBoundary>();
            if (roomBound == null)
                continue;
            // We just go with the first room we collided with, doesn't need to be too accurate
            else
                return roomBound.RoomName;
        }

        return "???";
    }

    void RefreshCurrentRoom()
    {

        KeyValuePair<string, List<Collider>> currentBestRoom = new KeyValuePair<string, List<Collider>>(
            null,
            new List<Collider>());

        foreach (KeyValuePair<string, List<Collider>> p in m_overlappingTriggers)
        {
            // If a room has more overlapping triggers than the current room, that becomes the new current room
            if (currentBestRoom.Value == null || p.Value.Count > currentBestRoom.Value.Count)
                currentBestRoom = p;
        }
        m_currentRoom = currentBestRoom.Key;
        UpdateRoomText();
    }
}
