﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyPickup : MonoBehaviour {

    [Tooltip("The speed at which the Rigidbody moves towards the given position")]
    public float CenterSpeed = 10f;

    public float ThrowSpeed = 10f;

    HoldableObject m_heldObject = null;
    Vector3 m_holdPosition = Vector3.zero;

    void FixedUpdate()
    {
        if (!IsHoldingObject())
            return;

        Vector3 velocity = (m_holdPosition - m_heldObject.RBody.position) * CenterSpeed;
        if (velocity.magnitude > CenterSpeed)
            velocity = velocity.normalized * CenterSpeed;
        m_heldObject.RBody.velocity = velocity;// (m_holdPosition - m_heldObject.RBody.position) * CenterSpeed;
    }


    public void SetHoldPosition(Vector3 position)
    {
        m_holdPosition = position;
    }

    // Picks up an object and holds it at the current hold position
    // This function does nothing if an object is currently being held.
    public void HoldObject(HoldableObject obj)
    {
        if (IsHoldingObject() || !obj.IsLocallyOwned())
            return;

        m_heldObject = obj;
        m_heldObject.IsHoldingObject = true;
        m_holdPosition = obj.RBody.position;
    }
    public void ThrowObject(Vector3 dir)
    {
        if (!IsHoldingObject())
            return;

        m_heldObject.IsHoldingObject = false;
        m_heldObject.SetThrowStatus(true);
        m_heldObject.RBody.velocity = dir.normalized * ThrowSpeed;

        m_heldObject = null;
    }
    public void DropObject()
    {
        if (!IsHoldingObject())
            return;

        float dropVelocity = m_heldObject.RBody.velocity.magnitude;
        if (dropVelocity > ThrowSpeed)
            dropVelocity = ThrowSpeed;

        m_heldObject.IsHoldingObject = false;
        m_heldObject.RBody.velocity = m_heldObject.RBody.velocity.normalized * dropVelocity;

        m_heldObject = null;
    }

    public Vector3 GetCurrentObjectPosition()
    {
        if (IsHoldingObject())
            return m_heldObject.RBody.position;
        else
            return Vector3.zero;
    }
    public bool IsHoldingObject()
    {
        return m_heldObject != null;
    }
}
