﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEventTypes {

	public class PositionEvent : GameEvents.Event
    {
        public Vector3 position;

        public PositionEvent(Vector3 position, string eventName) : base(eventName)
        {
            this.position = position;
        }
    }
}
