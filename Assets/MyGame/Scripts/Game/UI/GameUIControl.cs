﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIControl : MonoBehaviour {

    public static GameUIControl instance;
 
    [Tooltip("In-game chat")]
    public ChatMenuInput Chat;

    [Tooltip("The scoreboard")]
    public ScoreboardInput Scoreboard;

    public delegate bool GameUIObjectValidator();

    [System.Serializable]
    class GameUIObject
    {
        public string Name = null;
        public GameObject Obj;

        [HideInInspector]
        public GameUIObjectValidator Validator;

        [HideInInspector]
        public Vector3 OriginalScale;
        [HideInInspector]
        public IEnumerator EffectRoutine = null;
        [HideInInspector]
        public bool Visible = false;

        public bool FadeInInstantly = false;
        public bool FadeOutInstantly = false;
    }

    [SerializeField]
    List<GameUIObject> m_gameUIObjects;

    [SerializeField]
    GameObject m_pauseMenu;

    bool m_uiChecksEnabled = true;

    // Allow UI objects to be easily accessed by their name to do fast lookup on individual UI objects
    Dictionary<string, GameUIObject> m_uiByName = new Dictionary<string, GameUIObject>();

    void Awake()
    {
        instance = this;

        foreach(GameUIObject uiObj in m_gameUIObjects)
        {
            uiObj.OriginalScale = uiObj.Obj.transform.localScale;

            if (m_uiByName.ContainsKey(uiObj.Name))
            {
                Debug.LogError("Duplicate name GameUIObject!");
                m_uiByName[uiObj.Name] = uiObj;
            }
            else
                m_uiByName.Add(uiObj.Name, uiObj);
        }
    }
	// Use this for initialization
	void Start ()
    {
        GameEvents.instance.AddListener("OnGameStateLobby", OnGameStateLobby);
        GameEvents.instance.AddAnyListener(OnGameEvent);

        GameUIControl.instance.SetUIObjectValidator("TopMenu", () =>
        {
            return GameControl.instance.IsGameState(GameControl.GameStates.Lobby);
        });
        GameUIControl.instance.ForceRefreshUIObject("TopMenu");
    }

    void Update()
    {
        if (!PhotonNetwork.inRoom)
            return;

        // Allow game menu to be brought up in-game
        if (!GameControl.instance.IsGameState(GameControl.GameStates.Lobby))
        {
            if(Input.GetKeyDown(KeyCode.Escape))
                SetPauseMenuVisible(!m_pauseMenu.activeSelf);
        }

    }

    public void SetPauseMenuVisible(bool visible)
    {
        m_pauseMenu.gameObject.SetActive(visible);
        GameControl.instance.SetGameInputPreventionKey(ConstantVars.GIP_GameMenu_Key, !visible);
        GameControl.instance.SetMouseRequiredKey(ConstantVars.MR_GameMenu_Key, visible);
        ForceRefreshAllUIObjects();
    }

    public void SetUIChecksEnabled(bool enabled)
    {
        m_uiChecksEnabled = enabled;
    }

    public void SetUIObjectValidator(string name, GameUIObjectValidator validator)
    {
        GameUIObject obj;
        if (m_uiByName.TryGetValue(name, out obj))
            obj.Validator = () => { return validator() && !m_pauseMenu.gameObject.activeSelf; };
        else
            Debug.LogError("Could not find a GameUIObject by the name: " + name);
    }
    public GameObject GetUIObject(string name)
    {
        GameUIObject obj;
        if (m_uiByName.TryGetValue(name, out obj))
            return obj.Obj;

        Debug.LogError("Could not find a GameUIObject by the name: " + name);
        return null;
    }
    public T GetUIObject<T>(string name) where T : Component
    {
        GameUIObject obj;
        if(m_uiByName.TryGetValue(name, out obj))
        {
            T comp = obj.Obj.GetComponent<T>();

            if (comp == null)
                Debug.LogError("Could not find a component of type: " + typeof(T).Name + " from GameUIObject: " + name);
            else
                return comp;
        }

        Debug.LogError("Could not find a GameUIObject by the name: " + name);
        return null;
    }
    void CheckUIObjects()
    {
        if (!PhotonNetwork.inRoom || !m_uiChecksEnabled)
            return;

        foreach (GameUIObject obj in m_gameUIObjects)
        {
            bool valid = obj.Validator != null ? obj.Validator() : false;
            if (obj.Visible != valid)
            {
                if (obj.EffectRoutine != null)
                    StopCoroutine(obj.EffectRoutine);

                obj.Visible = valid;
                obj.EffectRoutine = CR_UIEffect(obj, valid);
                StartCoroutine(obj.EffectRoutine);
            }
        }
    }
    public void ForceRefreshAllUIObjects()
    {
        CheckUIObjects();
    }
    public void ForceRefreshUIObject(string name)
    {
        if (!PhotonNetwork.inRoom || !m_uiChecksEnabled)
            return;

        GameUIObject obj;
        if (m_uiByName.TryGetValue(name, out obj))
        {
            bool valid = obj.Validator != null ? obj.Validator() : false;
            if (obj.Visible != valid)
            {
                if (obj.EffectRoutine != null)
                    StopCoroutine(obj.EffectRoutine);

                obj.EffectRoutine = CR_UIEffect(obj, valid);
                obj.Visible = valid;
                StartCoroutine(obj.EffectRoutine);
            }
        }
        else
            Debug.LogError("Could not find a GameUIObject by the name: " + name);
    }
    IEnumerator CR_UIEffect(GameUIObject obj, bool fadeIn)
    {
        //if (obj.Name.Equals("InvisiblePlayerCount_Txt"))
            //Debug.Log("START " + fadeIn.ToString());
        Vector3 AlmostZero = new Vector3(0.1f, 0.1f, 0.1f);


        float elapsed = -Time.deltaTime;
        Vector3 startScale = fadeIn ? AlmostZero : obj.OriginalScale;
        Vector3 targetScale = fadeIn ? obj.OriginalScale : AlmostZero;

        if (fadeIn)
        {
            // If the object isn't visible, make sure to reset its scaling first
            if(!obj.Obj.activeSelf)
                obj.Obj.transform.localScale = AlmostZero;

            obj.Obj.SetActive(true);
        }

        const float EffectDuration = 0.2f;

        // Check where the scaling is relative to the targetScale and go from there
        // Because the scale isn't reset, it goes from the current scale and adjusts elapsed
        // time to match with scaling progress towards targetScale.
        float totalScalingDist = Vector3.Distance(obj.OriginalScale, AlmostZero);
        elapsed += (Vector3.Distance(obj.Obj.transform.localScale, startScale) / totalScalingDist) * EffectDuration;

        AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, EffectDuration, 1f);

        if ((fadeIn && obj.FadeInInstantly) ||
            (!fadeIn && obj.FadeOutInstantly))
            elapsed = EffectDuration;

        do
        {
            elapsed += Time.deltaTime;
            obj.Obj.transform.localScale = Vector3.Lerp(startScale, targetScale, curve.Evaluate(elapsed));

            yield return null;
        } while (elapsed < EffectDuration);

        if (!fadeIn)
            obj.Obj.SetActive(false);

        obj.EffectRoutine = null;
        yield break;
    }


    void OnGameStateLobby(GameEvents.Event eventData)
    {
        // Hide game menu
        // TODO: Commented this out temporarily while working in new lobby menu, because MenuCotnrol isn't used there MenuControl.instance.HideMenuGroup("GameMenu");
        SetPauseMenuVisible(false);
        GameControl.instance.SetGameInputPreventionKey(ConstantVars.GIP_GameMenu_Key, true);
        GameControl.instance.SetMouseRequiredKey(ConstantVars.MR_GameMenu_Key, false);
    }

    void OnGameEvent(GameEvents.Event eventData)
    {
        CheckUIObjects();
    }
}
