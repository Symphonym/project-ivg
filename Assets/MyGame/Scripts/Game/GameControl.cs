﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

// Takes care of general events such as Players leaving and whatnot
// and provides some game related utilities
public class GameControl : MonoBehaviour
{

    public static GameControl instance;

    public double StartDelay = 5.0;

    Transform m_gameItemsParent;

    public enum GameStates
    {
        NONE = 0,
        Lobby = 1,
        Waiting = 2,
        Starting = 3,
        Game = 4,
        Ending = 5
    }
    public enum EndReasons : int
    {
        // Invisible win conditions
        AllItemsThrown = 0,

        // Guard win conditions
        InvisibleDead,
        NotEnoughItems,
        TimeExpired,

        // Neutral win conditions
        ReturnToLobby

    }

    public enum ItemDensities
    {
        Low = 0,
        Mid = 1,
        High = 2
    }

    GameStates m_previousGameState = GameStates.NONE;

    Dictionary<string, bool> m_gameInputPreventions = new Dictionary<string, bool>();
    bool m_gameInputAllowed = true;

    Dictionary<string, bool> m_mouseRequirements = new Dictionary<string, bool>();
    bool m_mouseRequired = false;

    PhotonView m_photonView;

    GameObject m_lobbyCam;

    bool m_firstUpdate = false;

    bool m_levelLoaded = true;

    Text m_itemNeededText;

    Text m_preGameText;
    GameHeaderData m_gameHeaderData;

    void Awake()
    {
        instance = this;

        // Make sure to require the mouse if we're in the lobby
        SetMouseRequiredKey(ConstantVars.MR_GameState_Key, IsGameState(GameStates.Lobby));

        m_photonView = PhotonView.Get(this);

        PhotonNetwork.OnEventCall += OnPhotonEvent;
    }

    void Start()
    {
        GameObject gameItemsParent = GameObject.Find("GameItems");
        m_gameItemsParent = gameItemsParent != null ? gameItemsParent.transform : null;


        m_lobbyCam = GameObject.Find("LobbyCamera");
        HoldableObject[] gameItems = m_gameItemsParent.GetComponentsInChildren<HoldableObject>();
        int validItemCount = 0;

        foreach(HoldableObject gameItem in gameItems)
        {
            int itemDensity = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomSetting_ItemDensity_Key, (int)GameControl.ItemDensities.High);
            if (!gameItem.CompareTag("GameItem") || (int)gameItem.ItemDensityGroup > itemDensity)
                Destroy(gameItem.gameObject);
            else
                ++validItemCount;
        }
        if (PhotonNetwork.inRoom && PhotonNetwork.isMasterClient)
        {
            int currentLevelIndex = Utility.GetPhotonRoomProp<int>("C0", 0);
            Levels.LevelData level = Levels.instance.All[currentLevelIndex];

            ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
            hashtable[ConstantVars.RoomItemsRequired_Key] =
                Mathf.RoundToInt(Utility.GetPhotonRoomProp<float>(ConstantVars.RoomSetting_ItemsRequired_Key, 0.8f) * validItemCount);
            PhotonNetwork.room.SetCustomProperties(hashtable);
        }


        GameUIControl.instance.SetUIObjectValidator("ItemsNeeded", () =>
        {
            return IsGameState(GameStates.Game);
        });
        GameUIControl.instance.SetUIObjectValidator("GameHeader", () =>
        {
            return IsGameState(GameStates.Game);
        });

        GameUIControl.instance.SetUIObjectValidator("PreGame", () =>
        {
            return !IsGameState(GameStates.Game) && !IsGameState(GameStates.Lobby);
        });
        GameUIControl.instance.SetUIObjectValidator("Tutorial_Guard", () =>
        {
            return IsGameState(GameStates.Starting) && Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName) == ConstantVars.GuardTeamName;
        });
        GameUIControl.instance.SetUIObjectValidator("Tutorial_Invis", () =>
        {
            return IsGameState(GameStates.Starting) && Utility.GetPhotonPlayerProp<string>(ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName) == ConstantVars.InvisibleTeamName;
        });

        m_itemNeededText = GameUIControl.instance.GetUIObject<Text>("ItemsNeeded_Txt");

        m_preGameText = GameUIControl.instance.GetUIObject<Text>("PreGame_Txt");
        m_gameHeaderData = GameUIControl.instance.GetUIObject<GameHeaderData>("GameHeader");
    }

    IEnumerator CR_GameStateUpdate()
    {
        // We handle logic for the game state only a few times each second
        // to save performance.
        while(true)
        {
            yield return new WaitForSeconds(0.2f);

            // Update mouse lock state
            Utility.SetMouseLocked(!m_mouseRequired);

            if (!PhotonNetwork.inRoom || IsGameState(GameStates.Lobby))
                continue;

            if (IsGameState(GameStates.Waiting))
            {
                m_preGameText.text = LoadedPlayers() + "/" + PhotonNetwork.playerList.Length + " players loaded";

                // Start game when all players have loaded
                if (PhotonNetwork.isMasterClient && LoadedPlayers() == PhotonNetwork.playerList.Length)
                {
                    ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
                    hashtable.Add(ConstantVars.RoomGameStartTime_Key, PhotonNetwork.time + StartDelay); // Set room property for game start time
                    PhotonNetwork.room.SetCustomProperties(hashtable);

                    SetGameState(GameStates.Starting);
                }
            }

            else if (IsGameState(GameStates.Starting))
            {
                double timeToStart = Utility.GetPhotonRoomProp<double>(ConstantVars.RoomGameStartTime_Key, PhotonNetwork.time) - PhotonNetwork.time;
                int secondsToStart = Mathf.RoundToInt((float)timeToStart);



                if (timeToStart <= 0)
                {
                    m_preGameText.text = "Starting...";
                    if (PhotonNetwork.isMasterClient)
                        SetGameState(GameStates.Game);
                }
                else
                    m_preGameText.text = "Starting in " + secondsToStart.ToString() + "s";

            }

            // Update game duration text as well as Invisibility bonus
            else if (IsGameState(GameStates.Game))
            {
                int timeLeft;
                string timeLeftString = Utility.GetGametimeLeftAsString(out timeLeft);

                if (timeLeft <= 0)
                {
                    timeLeft = 0;

                    // Guards win when the time expires
                    EndGame(EndReasons.TimeExpired);
                }

                m_gameHeaderData.GameDurationText.text = timeLeftString;

                // Update ALIVE player count
                int invisPlayerCount = 0;
                int guardPlayerCount = 0;
                foreach (PhotonPlayer player in PhotonNetwork.playerList)
                {
                    string team = Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName);
                    bool alive = Utility.GetPhotonPlayerProp<bool>(player, ConstantVars.PlayerAlive_Key, true);

                    if (!alive)
                        continue;

                    if (team == ConstantVars.GuardTeamName)
                        ++guardPlayerCount;
                    else if (team == ConstantVars.InvisibleTeamName)
                        ++invisPlayerCount;
                }
                m_gameHeaderData.TeamInvisibleText.text = invisPlayerCount.ToString();
                m_gameHeaderData.TeamGuardText.text = guardPlayerCount.ToString();


                // Show items left text
                int itemsScored = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomItemsScored_Key, 0);
                int itemsRequired = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomItemsRequired_Key, m_gameItemsParent.childCount);

                int itemsNeededToWin = itemsRequired - itemsScored;
                m_itemNeededText.text = itemsNeededToWin + "/" + m_gameItemsParent.childCount;

                // Endgame conditions
                if (itemsNeededToWin <= 0)
                    EndGame(EndReasons.AllItemsThrown);
                else if (itemsNeededToWin > m_gameItemsParent.childCount)
                    EndGame(EndReasons.NotEnoughItems);
                else if (AllInvisibleDead())
                    EndGame(EndReasons.InvisibleDead);

            }
        }
    }

    void Update()
    {
        if (!PhotonNetwork.inRoom)
            return;

        if (!m_firstUpdate)
        {
            m_firstUpdate = true;
            OnGameStateChanged();

            // Change loaded status to finished
            if (IsGameState(GameStates.Waiting))
            {
                ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
                hashtable[ConstantVars.PlayerLoaded_Key] = true;
                PhotonNetwork.player.SetCustomProperties(hashtable);
            }

            StartCoroutine(CR_GameStateUpdate());
        }
    }

    public void ItemWasScored()
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        // Increment scored item count
        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable[ConstantVars.RoomItemsScored_Key] = Utility.GetPhotonRoomProp<int>(ConstantVars.RoomItemsScored_Key, 0) + 1;
        PhotonNetwork.room.SetCustomProperties(hashtable);
    }

    bool AllInvisibleDead()
    {
        int invisPlayerCount = 0;
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            // Check if player is of the Invisible team
            if (Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerTeam_Key, ConstantVars.SpectatorTeamName) == ConstantVars.InvisibleTeamName)
            {
                ++invisPlayerCount;

                // Check if player is alive
                if (Utility.GetPhotonPlayerProp<bool>(player, ConstantVars.PlayerAlive_Key, true))
                    return false;
            }
        }

        // If there's no Invisible players in the game (for whatever reason there's only guards in the game)
        // don't treat this as a "All Invisible Dead" situation, just let the host return to lobby or whatever
        return invisPlayerCount > 0;
    }


    int LoadedPlayers()
    {
        int loaded = 0;
        foreach (PhotonPlayer player in PhotonNetwork.playerList)
        {
            if (Utility.GetPhotonPlayerProp<bool>(player, ConstantVars.PlayerLoaded_Key, false))
                ++loaded;
        }
        return loaded;
    }


    // Starts a multiplayer game on the specified level
    // (Only callable by master client)
    public void StartGame(Levels.LevelData level)
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        // Can only start game from lobby
        if (!IsGameState(GameStates.Lobby))
            return;

        bool changingLevel = ConsistentSceneLoader.instance.CurrentScene != level.SceneName;

        // Change game state
        if (changingLevel)
            SetGameState(GameStates.Lobby);
        else
            SetGameState(GameStates.Waiting);

        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable.Add("C0", Levels.instance.GetLevelIndex(level.Name)); // Set room property for current level
        hashtable.Add(ConstantVars.RoomGameStartTime_Key, PhotonNetwork.time); // Reset game start time (this will be overwritten once all players have loaded)
        hashtable.Add(ConstantVars.RoomItemsScored_Key, 0); // Reset items scored count
        hashtable.Add(ConstantVars.RoomSetting_XPGain_Key, XPControl.IsXPGainAllowed()); // Set the XP Gain bool when we start the game
        PhotonNetwork.room.SetCustomProperties(hashtable);

        // Send the RPC to all clients (including master client) that loads the level
        m_photonView.RPC("StartGameRPC", PhotonTargets.AllViaServer, level.SceneName, changingLevel);
        PhotonNetwork.SendOutgoingCommands();
    }

    [PunRPC]
    void StartGameRPC(string levelSceneName, bool changingLevel, PhotonMessageInfo info)
    {
        // Only master client can send this RPC
        if (info.sender != PhotonNetwork.masterClient)
            return;

        // Reset player state when starting a new game
        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        // TODO: Add kills and throw score reset here?
        hashtable[ConstantVars.PlayerReady_Key] = false;
        hashtable[ConstantVars.PlayerLoaded_Key] = false;
        hashtable[ConstantVars.PlayerAlive_Key] = true;
        PhotonNetwork.player.SetCustomProperties(hashtable);

        ConsistentSceneLoader.instance.LoadScene(levelSceneName, changingLevel ? "Host is changing level" : "Game is starting");
    }

    public void EndGame(EndReasons endReason)
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        // Game can only be ended with a winning team during the Game state
        if (endReason != EndReasons.ReturnToLobby && !IsGameState(GameStates.Game))
            return;

        // Store the end reason as a cache for the outcome of the last played game, useful for new clients connecting and such
        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable.Add(ConstantVars.RoomGameEndReason_Key, (int)endReason);
        PhotonNetwork.room.SetCustomProperties(hashtable);

        if(endReason != EndReasons.ReturnToLobby)
            SetGameState(GameStates.Ending);
        else
            SetGameState(GameStates.Lobby);

        m_photonView.RPC("EndGameRPC", PhotonTargets.AllViaServer, (int)endReason);
        PhotonNetwork.SendOutgoingCommands();
    }
    [PunRPC]
    void EndGameRPC(int endReason, PhotonMessageInfo info)
    {
        // Only master client can send this RPC
        if (!info.sender.IsMasterClient)
            return;

        // Give the player their XP (if the room allows it)
        if((EndReasons)endReason != EndReasons.ReturnToLobby && Utility.GetPhotonRoomProp<bool>(ConstantVars.RoomSetting_XPGain_Key, false))
        {
            float xpGain = XPControl.instance.GetXPGained();
            float curXp = SteamStatsControl.instance.GetXPForPhotonPlayer(PhotonNetwork.player);

            XPControl.instance.SetProgressBarData(curXp, xpGain);

            if (xpGain > 0f)
                SteamStatsControl.instance.ChangeXP(curXp + xpGain);

            XPControl.instance.ResetXPGained();

            // Activate the level progress bar
            GameEvents.instance.Trigger("OnLevelProgressBarPrepared");
        }
    }

    // Sets the value at [key] to the value of gameInputAllowed
    // If there is any value set to false in the Game Input Prevention
    // dictionary, game input is disallowed
    public void SetGameInputPreventionKey(string key, bool gameInputAllowed)
    {
        m_gameInputPreventions[key] = gameInputAllowed;

        // Update input allowed status
        m_gameInputAllowed = true;
        foreach (KeyValuePair<string, bool> p in m_gameInputPreventions)
        {
            if (!p.Value)
            {
                m_gameInputAllowed = false;
                break;
            }

        }
    }
    public bool GameInputAllowed_IgnoreState()
    {
        return m_gameInputAllowed;
    }
    public bool GameInputAllowed()
    {
        return GameInputAllowed_IgnoreState() && (IsGameState(GameStates.Game) || IsGameState(GameStates.Ending));
    }



    public void SetMouseRequiredKey(string key, bool mouseRequired)
    {
        m_mouseRequirements[key] = mouseRequired;

        m_mouseRequired = false;
        foreach (KeyValuePair<string, bool> p in m_mouseRequirements)
        {
            if (p.Value)
            {
                m_mouseRequired = true;
                break;
            }
        }
    }
    public bool IsMouseRequired()
    {
        return m_mouseRequired;
    }



    public void SetGameState(GameStates gameState)
    {
        if (!PhotonNetwork.isMasterClient)
            return;

        ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
        hashtable.Add(ConstantVars.RoomGameState_Key, (int)gameState);
        PhotonNetwork.room.SetCustomProperties(hashtable);
    }
    public GameStates GetGameState()
    {
        return (GameStates)Utility.GetPhotonRoomProp<int>(ConstantVars.RoomGameState_Key, (int)GameStates.Lobby);
    }
    public bool IsGameState(GameStates gameState)
    {
        return GetGameState() == gameState;
    }


    public int GetItemsLeft()
    {
        return m_gameItemsParent.childCount;
    }




    void OnApplicationFocus(bool focus)
    {
        SetMouseRequiredKey(ConstantVars.ML_AppFocused_Key, !focus);
    }




    void OnGameStateChanged()
    {
        if (!m_levelLoaded)
            return;

        SetMouseRequiredKey(ConstantVars.MR_GameState_Key, IsGameState(GameStates.Lobby));
        if (m_previousGameState != GetGameState())
        {
            // Ensure player is in the correct level
            Levels.LevelData gameLevel = Levels.instance.All[Utility.GetPhotonRoomProp<int>("C0", 0)];
            if (ConsistentSceneLoader.instance.CurrentScene != gameLevel.SceneName)
                ConsistentSceneLoader.instance.LoadScene(gameLevel.SceneName);
            else
            {
                // Detect when a game is started, and ensure no further state changes are processed until the new level loads
                if (IsGameState(GameStates.Waiting) && m_previousGameState == GameStates.Lobby)
                {
                    m_levelLoaded = false;

                    // Prevent in-game UI from popping up before the level is loaded
                    GameUIControl.instance.SetUIChecksEnabled(false);
                }

                else if (IsGameState(GameStates.Lobby))
                {
                    if (m_previousGameState == GameStates.Ending || m_previousGameState == GameStates.Game)
                        MenuControlNEW.instance.ChangeMenu("ScoreboardMenu");
                    else
                        MenuControlNEW.instance.ChangeMenu("LobbyMenu");

                    // Move LobbyCamera to player position (it will be lerped back to its original position)
                    if (Camera.main != null)
                    {
                        m_lobbyCam.transform.position = Camera.main.transform.position;
                        m_lobbyCam.transform.eulerAngles = Camera.main.transform.eulerAngles;
                    }

                    MusicControl.instance.PlaySongUnlessAlreadyPlaying("MainMenu_Music");

                    // Reset player loaded status when entering lobby
                    ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
                    hashtable[ConstantVars.PlayerLoaded_Key] = false;
                    PhotonNetwork.player.SetCustomProperties(hashtable);

                    if (PlayerSpawner.instance != null)
                        PlayerSpawner.instance.DestroyPlayerObject();

                    if (m_lobbyCam != null)
                        m_lobbyCam.SetActive(true);

                    GameEvents.instance.Trigger("OnGameStateLobby");
                }
                else if (IsGameState(GameStates.Waiting))
                {
                    if (m_lobbyCam != null)
                        m_lobbyCam.SetActive(false);

                    // Spawn player (this is part of the loading/waiting since it might take a moment)
                    if (!PlayerSpawner.instance.HasPlayerObject())
                        PlayerSpawner.instance.SpawnDefault();

                    MusicControl.instance.StopSong();
                    GameEvents.instance.Trigger("OnGameStateWaiting");
                }
                else if (IsGameState(GameStates.Starting))
                {
                    if (m_lobbyCam != null)
                        m_lobbyCam.SetActive(false);

                    // Joining the game in progress will spawn you as a spectator
                    if (!PlayerSpawner.instance.HasPlayerObject())
                        PlayerSpawner.instance.SpawnSpectator();

                    MusicControl.instance.StopSong();
                    GameEvents.instance.Trigger("OnGameStateStarting");
                }
                else if (IsGameState(GameStates.Game))
                {
                    if (m_lobbyCam != null)
                        m_lobbyCam.SetActive(false);

                    // Joining the game in progress will spawn you as a spectator
                    if (!PlayerSpawner.instance.HasPlayerObject())
                        PlayerSpawner.instance.SpawnSpectator();

                    MusicControl.instance.StopSong();
                    GameEvents.instance.Trigger("OnGameStateGame");
                }
                else if(IsGameState(GameStates.Ending))
                {
                    if (m_lobbyCam != null)
                        m_lobbyCam.SetActive(false);

                    // Joining the game in progress will spawn you as a spectator
                    if (!PlayerSpawner.instance.HasPlayerObject())
                        PlayerSpawner.instance.SpawnSpectator();

                    EndReasons endReason = (EndReasons)Utility.GetPhotonRoomProp<int>(ConstantVars.RoomGameEndReason_Key, (int)EndReasons.ReturnToLobby);
                    bool invisibleWin = endReason == EndReasons.AllItemsThrown;
                    bool guardWin = endReason == EndReasons.TimeExpired || endReason == EndReasons.InvisibleDead || endReason == EndReasons.NotEnoughItems;

                    if (invisibleWin)
                        m_preGameText.text = "<color=#" + ColorUtility.ToHtmlStringRGBA(ConstantVars.InvisibleColor) + ">Invisible Win!</color>";
                    else if (guardWin)
                        m_preGameText.text = "<color=#" + ColorUtility.ToHtmlStringRGBA(ConstantVars.GuardColor) + ">Guards Win!</color>";
                    else
                        m_preGameText.text = "<color=#" + ColorUtility.ToHtmlStringRGBA(ConstantVars.NeutralColor) + ">Game Over!</color>";

                    if (PhotonNetwork.isMasterClient)
                        Invoke("EndEnding", 5f);

                    MusicControl.instance.PlaySongUnlessAlreadyPlaying("MainMenu_Music");
                    GameEvents.instance.Trigger("OnGameStateEnding");
                }
            }

            
            GameEvents.instance.Trigger("OnGameStateChanged");
            m_previousGameState = GetGameState();
        }

        // Master client updates game state for matchmaker as well
        if (PhotonNetwork.isMasterClient)
        {
            // Set match making variable for whether or not we're in the lobby
            ExitGames.Client.Photon.Hashtable hashtable = new ExitGames.Client.Photon.Hashtable();
            hashtable.Add("C1", IsGameState(GameStates.Lobby) ? 1 : 0);
            PhotonNetwork.room.SetCustomProperties(hashtable);
        }
    }

    void EndEnding()
    {
        if (PhotonNetwork.isMasterClient && IsGameState(GameStates.Ending))
            SetGameState(GameStates.Lobby);
    }

    public void LeaveRoom()
    {
        if (!PhotonNetwork.inRoom)
            return;

        // Make sure LobbyCamera is enabled when we leave the room so it doesn't just turn black
        // Because player object is destroyed automatically upon leaving the room (and its camera) 
        if (m_lobbyCam != null)
        {
            m_lobbyCam.transform.position = Camera.main.transform.position;
            m_lobbyCam.transform.eulerAngles = Camera.main.transform.eulerAngles;
            m_lobbyCam.SetActive(true);
        }

        PhotonNetwork.LeaveRoom();
    }

    void OnLeftRoom()
    {
        // Go back to main menu when we leave the room/game
        ConsistentSceneLoader.instance.LoadScene("MainMenu");
    }
    void OnDisconnectedFromPhoton()
    {
        MessagePopupControl.instance.ShowPopup("Disconnected from region", "Disconnected");
    }
    void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        if (PhotonNetwork.isMasterClient)
            GameUIControl.instance.Chat.SendChatMessage(Utility.ParseChatPlayerNameNoTags(player) + " connected!", ChatMenuInput.ChatMessageTypes.HostAnnouncement);
    }
    void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        if (PhotonNetwork.isMasterClient)
            GameUIControl.instance.Chat.SendChatMessage(Utility.ParseChatPlayerName(otherPlayer) + " disconnected!", ChatMenuInput.ChatMessageTypes.HostAnnouncement);
    }
    void OnMasterClientSwitched(PhotonPlayer newMasterClient)
    {
        if (PhotonNetwork.isMasterClient)
            GameUIControl.instance.Chat.SendChatMessage(Utility.ParseChatPlayerName(newMasterClient) + " is the new game host!", ChatMenuInput.ChatMessageTypes.HostAnnouncement);
    }
    void OnPhotonCustomRoomPropertiesChanged(ExitGames.Client.Photon.Hashtable propertiesThatChanged)
    {
        if (propertiesThatChanged.ContainsKey(ConstantVars.RoomGameState_Key))
            OnGameStateChanged();
    }

    void OnPhotonEvent(byte eventCode, object content, int senderid)
    {
        // Handle KICK event
        if (eventCode == PunEvent.CloseConnection)
        {
            // Show a popup notification to the player when they are kicked
            PhotonPlayer sender = PhotonPlayer.Find(senderid);
            if (sender != null && sender.IsMasterClient)
                MessagePopupControl.instance.ShowPopup("You were kicked by the host.", "Disconnected");
        }
    }
}
