﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEvents : MonoBehaviour {

    public static GameEvents instance;

    public class Event
    {
        public string name;

        public Event(string eventName)
        {
            name = eventName;
        }
    }

    class GameEvent : UnityEvent<Event> { };

    Dictionary<string, GameEvent> m_events = new Dictionary<string, GameEvent>();
    GameEvent m_anyEvent = new GameEvent();

    void Awake()
    {
        instance = this;
    }

    public void AddListener(string eventName, UnityAction<Event> listener)
    {
        GameEvent thisEvent = null;
        if (m_events.TryGetValue(eventName, out thisEvent))
            thisEvent.AddListener(listener);
        else
        {
            GameEvent newEvent = new GameEvent();
            newEvent.AddListener(listener);
            m_events[eventName] = newEvent;
        }
    }

    public void RemoveListener(string eventName, UnityAction<Event> listener)
    {
        GameEvent thisEvent = null;
        if(m_events.TryGetValue(eventName, out thisEvent))
            thisEvent.RemoveListener(listener);

    }

    public void AddAnyListener(UnityAction<Event> listener)
    {
        m_anyEvent.AddListener(listener);
    }

    public void RemoveAnyListener(UnityAction<Event> listener)
    {
        m_anyEvent.RemoveListener(listener);
    }

    public void Trigger(Event eventData)
    {
        GameEvent thisEvent = null;
        if (m_events.TryGetValue(eventData.name, out thisEvent))
            thisEvent.Invoke(eventData);

        m_anyEvent.Invoke(eventData);
    }
    public void Trigger(string eventName)
    {
        Trigger(new Event(eventName));
    }
}
