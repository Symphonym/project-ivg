﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuSwitcher : MonoBehaviour {

    [System.Serializable]
    public class MenuItem
    {
        public string name;
        public string backMenu;

        public GameObject menu;
    }

    [SerializeField]
    List<MenuItem> m_menus = new List<MenuItem>();

    MenuItem m_currentMenu = null;


    public void ChangeMenu(string menuName)
    {
        MenuItem menu = null;
        foreach(MenuItem m in m_menus)
        {
            if (m.name.Equals(menuName))
                menu = m;
            else if (m.menu.activeSelf)
                m.menu.SetActive(false);
        }

        if (menu == null)
            return;

        m_currentMenu = menu;

        menu.menu.SetActive(true);
    }

    public void GoBack()
    {
        if (m_currentMenu != null && !string.IsNullOrEmpty(m_currentMenu.backMenu))
            ChangeMenu(m_currentMenu.backMenu);
    }
}
