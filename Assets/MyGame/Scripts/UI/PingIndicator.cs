﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PingIndicator : MonoBehaviour {

    public float Duration = 3f;
    public float DissapearDistance = 2f;
    public float RotationSpeed = 2f;
    public float FadeDuration = 0.1f;
    public string InfoString = "Unknown";
    public Color ArrowColor = Color.white;


    [SerializeField]
    Transform m_arrowModel;

    [SerializeField]
    Text m_distanceText;

    Vector3 m_arrowStartPosition;
    float m_startTime;

    float m_elapsed;

    bool m_hiding = false;
    bool m_distanceKillable = false;

	// Use this for initialization
	void Start () {
        m_arrowStartPosition = m_arrowModel.localPosition;
        m_startTime = Time.time;

        m_elapsed = Duration;
        m_arrowModel.GetComponent<Renderer>().material.SetColor("_Color", ArrowColor);
        m_arrowModel.GetComponent<Renderer>().material.SetColor("_EmissionColor", ArrowColor);
    }
	
	// Update is called once per frame
	void Update () {

        // Move indicator up and down in along a sine wave
        m_arrowModel.localPosition = new Vector3(
            m_arrowStartPosition.x,
            m_arrowStartPosition.y + Mathf.Sin((Time.time - m_startTime) * 2f) * 0.5f,
            m_arrowStartPosition.z);
        m_arrowModel.Rotate(new Vector3(0, Time.deltaTime * RotationSpeed, 0), Space.World);


        m_elapsed -= Time.deltaTime;

        if(PlayerSpawner.instance != null && PlayerSpawner.instance.HasPlayerObject())
        {
            float distanceToPlayer = Vector3.Distance(
                PlayerSpawner.instance.GetPlayerObject().transform.position,
                transform.position);

            m_distanceText.text =
                System.Math.Round(distanceToPlayer, 1).ToString("#.0") + "\n" +
                InfoString;

            if (distanceToPlayer <= DissapearDistance)
            {
                if (m_distanceKillable && !m_hiding)
                    StartCoroutine(CR_HidePing());
            }
            else
                m_distanceKillable = true;

        }

        if (m_elapsed <= 0 && !m_hiding)
            StartCoroutine(CR_HidePing());
    }

    IEnumerator CR_HidePing()
    {
        m_hiding = true;

        float elapsed = -Time.deltaTime;

        do
        {
            elapsed += Time.deltaTime;
            float ratio = Mathf.Clamp01(elapsed / FadeDuration);

            gameObject.transform.localScale = new Vector3(1, 1, 1) * Mathf.Clamp((1f - ratio), 0.01f, 1f);

            yield return null;
        } while (elapsed < FadeDuration);

        Destroy(gameObject);
    }
}
