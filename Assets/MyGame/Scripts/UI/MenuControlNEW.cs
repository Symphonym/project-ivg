﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MenuControlNEW : MonoBehaviour {

    public static MenuControlNEW instance;

    [SerializeField]
    Image m_highlightMenuBar;

    [System.Serializable]
    public class MenuItem
    {
        public string name;

        // This is only used whenever cameraTransform is null
        public Vector3 cameraPosition;
        public Vector3 cameraRotation;

        public Transform cameraTransform = null;

        // The menu header to adjust the menu bar for
        public RectTransform menuHeader;

        public CanvasGroup menu;

        public UnityEvent onMenuOpen;
    }

    public List<MenuItem> Menus = new List<MenuItem>();
    public float DefaultTransitionDuration = 1f;
    public string StartMenu = "";


    IEnumerator m_transitionCR = null;
    Dictionary<string, IEnumerator> m_hideCRs = new Dictionary<string, IEnumerator>();

    MenuItem m_currentMenu = null;

    void Awake()
    {
        instance = this;
    }

	void Start ()
    {
        foreach (MenuItem m in Menus)
        {
            m.menu.alpha = 0f;
            m.menu.interactable = false;
            m.menu.gameObject.SetActive(false);
        }

        m_highlightMenuBar.rectTransform.pivot = new Vector2(0.5f, 0.5f);
        ChangeMenu(StartMenu);
    }


    public void HideMenu()
    {
        if (m_transitionCR != null)
            StopCoroutine(m_transitionCR);

        foreach (MenuItem m in Menus)
        {
            if (!m_hideCRs.ContainsKey(m.name))
            {
                m_hideCRs[m.name] = CR_HideMenu(m, DefaultTransitionDuration);
                StartCoroutine(m_hideCRs[m.name]);
            }
        }
    }
    public void ChangeMenu(string menuName)
    {
        ChangeMenu(menuName, -1f);
    }
    public void ChangeMenu(string menuName, float transitionDuration)
    {
        if (m_currentMenu != null && m_currentMenu.name.Equals(menuName))
            return;

        MenuItem menu = null;
        foreach(MenuItem m in Menus)
        {
            if(m.name.Equals(menuName))
            {
                menu = m;
                break;
            }
        }
        if (menu == null)
            return;

        if (m_transitionCR != null)
            StopCoroutine(m_transitionCR);


        // Hide previous menu
        if(m_currentMenu != null && !m_hideCRs.ContainsKey(m_currentMenu.name))
        {
            m_hideCRs[m_currentMenu.name] = CR_HideMenu(m_currentMenu, transitionDuration < 0 ? DefaultTransitionDuration : transitionDuration);
            StartCoroutine(m_hideCRs[m_currentMenu.name]);
        }

        m_currentMenu = menu;

        // Stop any hiding coroutine for the new menu
        if (m_hideCRs.ContainsKey(m_currentMenu.name))
        {
            StopCoroutine(m_hideCRs[m_currentMenu.name]);
            m_hideCRs.Remove(m_currentMenu.name);
        }

        m_transitionCR = CR_ChangeMenu(m_currentMenu, transitionDuration < 0 ? DefaultTransitionDuration : transitionDuration);
        StartCoroutine(m_transitionCR);
    }

    IEnumerator CR_ChangeMenu(MenuItem menu, float duration)
    {
        // Make sure UI is properly calculated (mostly matters if this is run at the very start of scene initialization)
        yield return new WaitForEndOfFrame();

        float elapsed = -Time.deltaTime;

        AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, duration, 1f);

        Quaternion startRot = Camera.main != null ? Camera.main.transform.rotation : Quaternion.identity;
        Quaternion endRot = Quaternion.Euler(menu.cameraTransform == null ? menu.cameraRotation : menu.cameraTransform.rotation.eulerAngles);

        Vector3 startPos = Camera.main != null ? Camera.main.transform.position : Vector3.zero;
        Vector3 endPos = menu.cameraTransform == null ? menu.cameraPosition : menu.cameraTransform.position;

        Vector2 barStartSize = m_highlightMenuBar.rectTransform.sizeDelta;
        Vector2 barEndSize = new Vector2(menu.menuHeader.rect.width, barStartSize.y);

        Vector3 barStartPos = m_highlightMenuBar.rectTransform.position;
        Vector3 barEndPos = barStartPos;
        barEndPos.x = menu.menuHeader.position.x;

        float startAlpha = menu.menu.alpha;

        menu.menu.interactable = true;
        menu.menu.gameObject.SetActive(true);
        menu.onMenuOpen.Invoke();

        do
        {
            elapsed += Time.deltaTime;

            float ratio = curve.Evaluate(elapsed);

            if(Camera.main != null)
            {
                Camera.main.transform.rotation = Quaternion.Lerp(startRot, endRot, ratio);
                Camera.main.transform.position = Vector3.Lerp(startPos, endPos, ratio);
            }

            m_highlightMenuBar.rectTransform.sizeDelta = Vector2.Lerp(barStartSize, barEndSize, ratio);
            m_highlightMenuBar.rectTransform.position = Vector3.Lerp(barStartPos, barEndPos, ratio);

            menu.menu.alpha = Mathf.Lerp(startAlpha, 1f, ratio);

            yield return null;
        } while (elapsed < duration);

        yield break;
    }

    IEnumerator CR_HideMenu(MenuItem menu, float duration)
    {
        float elapsed = -Time.deltaTime;
        AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, duration, 1f);

        float startAlpha = menu.menu.alpha;

        menu.menu.interactable = false;

        do
        {
            elapsed += Time.deltaTime;

            float ratio = curve.Evaluate(elapsed);
            menu.menu.alpha = Mathf.Lerp(startAlpha, 0f, ratio);

            yield return null;
        } while (elapsed < duration);

        menu.menu.gameObject.SetActive(false);

        m_hideCRs.Remove(menu.name);

        yield break;
    }
}
