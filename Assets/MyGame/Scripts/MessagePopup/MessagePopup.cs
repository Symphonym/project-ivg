﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class MessagePopup : MonoBehaviour {



    public Text TitleText;
    public Text MessageText;

    public Transform ButtonParent;

    public float FadeDuration = 0.5f;

    CanvasGroup m_canvasGroup;

    void Start()
    {
        // Initialize popup as hidden
        m_canvasGroup = GetComponent<CanvasGroup>();
        m_canvasGroup.alpha = 0;
        m_canvasGroup.interactable = false;

        StartCoroutine(CR_Fade(FadeDuration, true));
    }

    IEnumerator CR_Fade(float fadeTime, bool fadeIn)
    {
        Vector3 originalScale = transform.localScale;
        AnimationCurve curve = AnimationCurve.EaseInOut(0, 0, fadeTime, 1f);

        float elapsed = -Time.deltaTime;

        do
        {
            elapsed += Time.deltaTime;
            float ratio = curve.Evaluate(elapsed);
            m_canvasGroup.alpha = fadeIn ? ratio : 1.0f - ratio;
            transform.localScale = Vector3.Lerp(new Vector3(0, 0, 0), originalScale, fadeIn ? ratio : 1f - ratio);

            yield return null;
        } while (elapsed < fadeTime);



        if(fadeIn)
            m_canvasGroup.interactable = true;
        else
            Destroy(gameObject);
    }

    public void ClosePopup()
    {
        m_canvasGroup.interactable = false;
        StartCoroutine(CR_Fade(FadeDuration, false));
    }
}
