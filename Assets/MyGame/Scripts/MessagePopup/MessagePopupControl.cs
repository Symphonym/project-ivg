﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MessagePopupControl : MonoBehaviour {

    public static MessagePopupControl instance;

    public MessagePopup MessagePopupPrefab;
    public Canvas MessagePopupCanvas;

    public float FadeAlpha = 0.6f;

    public class MessagePopupButtonInfo
    {
        public string Title;
        public UnityAction Callback;

        public static MessagePopupButtonInfo Default = new MessagePopupButtonInfo("OK", () => { });

        public MessagePopupButtonInfo(string title, UnityAction callback)
        {
            Title = title;
            Callback = callback;
        }
    }

    [SerializeField]
    Button m_buttonPrefab;

    [SerializeField]
    GameObject m_uiBlocker;
    [SerializeField]
    Image m_backgroundImage;

    int m_popupCount = 0;
    bool m_forceUIBlocker = false;

    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        DontDestroyOnLoad(gameObject);
        RefreshUIBlocker();
    }

    public void RefreshUIBlocker()
    {
        if(m_uiBlocker != null)
            m_uiBlocker.gameObject.SetActive(m_popupCount > 0 || m_forceUIBlocker);

        if(m_backgroundImage != null)
            m_backgroundImage.CrossFadeAlpha(m_popupCount > 0 ? FadeAlpha : 0f, 0.3f, true);
    }

    public void ForceBlocker(bool block)
    {
        m_forceUIBlocker = block;
        RefreshUIBlocker();
    }

    public void ShowPopup(string message, string title, params MessagePopupButtonInfo[] buttons)
    {
        // Check if a popup with the same message is already displayed,
        // if so, avoid displaying another (to avoid duplicate popups)
        bool duplicates = false;
        MessagePopup[] popups = MessagePopupCanvas.GetComponentsInChildren<MessagePopup>();
        foreach(MessagePopup p in popups)
        {
            if (p.MessageText.text == message)
            {
                duplicates = true;
                break;
            }
        }

        if (duplicates)
            return;

        // Create new popup object and attach it to the designated popup Canvas
        MessagePopup popup = Instantiate(MessagePopupPrefab, MessagePopupCanvas.transform, false);
        popup.TitleText.text = title;
        popup.MessageText.text = message;

        if(buttons.Length <= 0)
        {
            Button button = Instantiate(m_buttonPrefab, popup.ButtonParent, false);
            button.GetComponentInChildren<Text>().text = MessagePopupButtonInfo.Default.Title;
            button.onClick.AddListener(MessagePopupButtonInfo.Default.Callback);
            button.onClick.AddListener(() => { popup.ClosePopup(); --m_popupCount; RefreshUIBlocker(); });
        }
        else
        {
            foreach (MessagePopupButtonInfo buttonInfo in buttons)
            {
                Button button = Instantiate(m_buttonPrefab, popup.ButtonParent, false);
                button.GetComponentInChildren<Text>().text = buttonInfo.Title;
                button.onClick.AddListener(buttonInfo.Callback);
                button.onClick.AddListener(() => { popup.ClosePopup(); --m_popupCount; RefreshUIBlocker(); });
                button.transform.SetAsLastSibling();
            }
        }

        ++m_popupCount;
        RefreshUIBlocker();

        // Lifetime of the message popups are handled by the popups themselves
    }
}
