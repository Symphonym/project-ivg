﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollShooter : MonoBehaviour {

    public Rigidbody HeadBone;
	
    public void Launch(Vector3 force)
    {
        HeadBone.AddForce(force, ForceMode.Impulse);
    }
}
