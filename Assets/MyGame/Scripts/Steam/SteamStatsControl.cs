﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class SteamStatsControl : MonoBehaviour {

    public static SteamStatsControl instance;

    protected Callback<UserStatsReceived_t> m_steamUserStatsReceived;

    // Mapping Photon player Id's to their XP
    Dictionary<int, float> m_playerStats = new Dictionary<int, float>();

    // True whenever to want to increment the player's level stat
    bool m_changeXP = false;
    float m_newXP = 0;

    private void Awake()
    {
        instance = this;
    }

    void Start ()
    {
        GameEvents.instance.AddListener("OnSteamIdentityChanged", OnSteamIdentityChanged);
        GameEvents.instance.AddListener("OnGameStateLobby", OnGameStateLobby);

        RequestStats();
	}

    private void OnEnable()
    {
        if (SteamManager.Initialized)
            m_steamUserStatsReceived = Callback<UserStatsReceived_t>.Create(OnSteamUserStatsReceived);
    }

    public void RequestStats()
    {
        if (!SteamManager.Initialized || !PhotonNetwork.inRoom)
            return;

        // Request stats for all other players
        foreach (PhotonPlayer player in PhotonNetwork.otherPlayers)
        {
            CSteamID steamId = SteamIdentityControl.instance.GetPhotonPlayerSteamId(player);
            if (steamId.IsValid())
                SteamUserStats.RequestUserStats(steamId);
        }

        // Request stats for this player
        SteamUserStats.RequestCurrentStats();
    }

    public void ChangeXP(float xp)
    {
        if (!SteamManager.Initialized)
            return;

        m_newXP = xp;
        m_changeXP = true;
        SteamUserStats.RequestCurrentStats();
    }

    void ChangeBackupXP(float xp)
    {
        // This XP value is used by a remote client whenever they don't have access to Steam
        // The value is less secure since it doesn't use any Steam verification, hence it is only
        // used as a backup for players without Steam so that they can see the levels of other players
        // even if the XP value is storedo on the Steam backend.
        ExitGames.Client.Photon.Hashtable table = new ExitGames.Client.Photon.Hashtable();
        table[ConstantVars.PlayerSteamXPBackup_Key] = xp;
        PhotonNetwork.player.SetCustomProperties(table);
    }

    public float GetXPForPhotonPlayer(PhotonPlayer player)
    {
        if (player == null)
            return 0f;

        float xp;
        if (m_playerStats.TryGetValue(player.ID, out xp))
            return xp;

        // Only use the backup value when we aren't connected to Steam
        return SteamManager.Initialized ? 0f : Utility.GetPhotonPlayerProp<float>(player, ConstantVars.PlayerSteamXPBackup_Key, 0f);
    }

    IEnumerator CR_UpdateStats()
    {
        // When we enter the lobby we request the stats after a small delay to hopefully get the new levels
        // if anyone leveled up after the game
        if(GameControl.instance != null && GameControl.instance.IsGameState(GameControl.GameStates.Lobby))
        {
            yield return new WaitForSeconds(5f);
            RequestStats();
        }
    }

    void OnSteamUserStatsReceived(UserStatsReceived_t pCallback)
    {
        if (pCallback.m_eResult != EResult.k_EResultOK)
        {
            // If we failed to retrieve OUR OWN stats, request them again, we need them to be able
            // to modify them.
            if (pCallback.m_steamIDUser == SteamUser.GetSteamID())
                SteamUserStats.RequestCurrentStats();

            return;
        }

        PhotonPlayer player = SteamIdentityControl.instance.GetPhotonPlayerFromSteamID(pCallback.m_steamIDUser);

        if(player != null)
        {
            float currentXP;

            // Only store data if stats were found
            if (SteamUserStats.GetUserStat(pCallback.m_steamIDUser, ConstantVars.SteamStats_XP_Key, out currentXP))
            {
                // We received data for ourself
                if(pCallback.m_steamIDUser == SteamUser.GetSteamID())
                {
                    // Check if we requested the XP to be changed
                    if (m_changeXP)
                    {
                        if (SteamUserStats.SetStat(ConstantVars.SteamStats_XP_Key, m_newXP))
                        {
                            currentXP = m_newXP;
                            SteamUserStats.StoreStats();
                        }

                        m_changeXP = false;
                    }

                    // Store a backup of our XP on the Photon back-end for clients who don't have Steam
                    ChangeBackupXP(currentXP);
                }

                // Store the XP by the Photon Player id
                m_playerStats[player.ID] = currentXP;

                if (GameEvents.instance != null)
                    GameEvents.instance.Trigger("OnPlayerXPChanged");
            }
            else
                m_playerStats.Remove(player.ID);
        }
    }

    void OnSteamIdentityChanged(GameEvents.Event eventData)
    {
        // Refresh stats when someone's Steam identity changes
        RequestStats();
    }

    void OnGameStateLobby(GameEvents.Event eventData)
    {
        StartCoroutine(CR_UpdateStats());
    }
    void OnPhotonPlayerPropertiesChanged(object[] playerAndUpdatedProps)
    {
        ExitGames.Client.Photon.Hashtable table = (ExitGames.Client.Photon.Hashtable)playerAndUpdatedProps[1];
        if(table.ContainsKey(ConstantVars.PlayerSteamXPBackup_Key))
        {
            if (GameEvents.instance != null)
                GameEvents.instance.Trigger("OnPlayerXPChanged");
        }
    }
}
