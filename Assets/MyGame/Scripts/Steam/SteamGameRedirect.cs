﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamGameRedirect : MonoBehaviour {

    [HideInInspector]
    public static string TargetPhotonServer = string.Empty;

    public MenuControlNEW MainMenuControl;
    public MenuSwitcher PlayMenuSwitcher;

    IEnumerator m_joiningCR = null;

    private void Start()
    {
        if (!string.IsNullOrEmpty(TargetPhotonServer))
        {
            JoinPhotonServer(TargetPhotonServer);
            TargetPhotonServer = string.Empty;
        }
    }

    IEnumerator CR_WaitForPhoton(string serverName)
    {
        yield return null;

        MainMenuControl.ChangeMenu("PlayMenu");
        PlayMenuSwitcher.ChangeMenu("Connection_Menu");

        float elapsed = 0f;
        const float Timeout = 5f;
        while (elapsed < Timeout && !PhotonNetwork.connectedAndReady)
        {
            elapsed += Time.deltaTime;
            yield return null;
        }

        if (PhotonNetwork.connectedAndReady)
            PhotonNetwork.JoinRoom(serverName);
        else
        {
            MessagePopupControl.instance.ShowPopup("Failed to establish connection.", "Error");
            PlayMenuSwitcher.GoBack();
        }

        m_joiningCR = null;
    }

    public void JoinPhotonServer(string serverName)
    {
        if(m_joiningCR != null)
        {
            StopCoroutine(m_joiningCR);
            m_joiningCR = null;
        }

        Utility.InitializePhotonPlayerProps(PhotonNetwork.player);

        m_joiningCR = CR_WaitForPhoton(serverName);
        StartCoroutine(m_joiningCR);
    }
}
