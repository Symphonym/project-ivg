﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class SteamInviteControl : MonoBehaviour {

    public static SteamInviteControl instance;

    public float LobbyTimeout = 20f;

    protected Callback<GameLobbyJoinRequested_t> m_steamLobbyJoinRequest;
    protected Callback<GameRichPresenceJoinRequested_t> m_steamRichPresenceJoinRequest;
    protected Callback<LobbyDataUpdate_t> m_steamLobbyDataUpdate;
    private CallResult<LobbyCreated_t> m_steamLobbyCreated;


    CSteamID m_lobbyId = CSteamID.Nil;
    bool m_requestingToJoin = false;

    float m_timeoutCounter = 0;

    private void Awake()
    {
        instance = this;
        DontDestroyOnLoad(gameObject);
        m_timeoutCounter = LobbyTimeout;
    }

    private void Start()
    {
        StartCoroutine(CR_CheckTimeout());

        // Parse command line invite arguments, inviting from friend list is not yet handled

        if (!SteamManager.Initialized)
            return;

        string[] cmdArgs = System.Environment.GetCommandLineArgs();
        for(int i = 0; i < cmdArgs.Length; i++)
        {
            if(cmdArgs[i] == "+connect_lobby" && (i + 1) < cmdArgs.Length)
            {
                try
                {
                    ulong lobbyId = System.Convert.ToUInt64(cmdArgs[i + 1]);
                    m_requestingToJoin = true;

                    // Begin by requesting the meta-data (Photon Server name) of the lobby we want to join
                    SteamMatchmaking.RequestLobbyData(new CSteamID(lobbyId));
                }
                catch { };
                break;
            }
            else if(cmdArgs[i] == "+connect_photon" && (i + 1) < cmdArgs.Length)
                JoinPhotonServer(cmdArgs[i + 1]);
        }

        // TODO: Handle CMD arguments that gets added when an invite is accepted while the game isn't running
    }

    private void OnEnable()
    {
        if (SteamManager.Initialized)
        {
            m_steamLobbyJoinRequest = Callback<GameLobbyJoinRequested_t>.Create(OnSteamRequestJoinLobby);
            m_steamRichPresenceJoinRequest = Callback<GameRichPresenceJoinRequested_t>.Create(OnSteamRichPresenceRequestJoinLobby);
            m_steamLobbyDataUpdate = Callback<LobbyDataUpdate_t>.Create(OnSteamLobbyDataUpdate);
            m_steamLobbyCreated = CallResult<LobbyCreated_t>.Create(OnCreatedSteamLobby);
        }
    }

    IEnumerator CR_CheckTimeout()
    {
        while(true)
        {
            yield return new WaitForSeconds(1f);

            // The timeout ticks down whenever the overlay isn't activated
            if (m_lobbyId.IsValid() && !SteamUtils.IsOverlayEnabled())
            {
                m_timeoutCounter -= 1f;

                // If the timeout value since the Invite dialog was opened passes, the lobby is destroyed
                if (m_timeoutCounter <= 0f)
                    LeaveSteamLobby();
            }
        }
    }

    public void OpenInviteDialog()
    {
        if (!SteamManager.Initialized || !PhotonNetwork.inRoom)
            return;

        m_timeoutCounter = LobbyTimeout;

        if (!m_lobbyId.IsValid())
            CreateSteamLobby();
        else
            SteamFriends.ActivateGameOverlayInviteDialog(m_lobbyId);
    }

    void JoinPhotonServer(string photonServerName)
    {
        SteamGameRedirect redirecter = GameObject.FindObjectOfType<SteamGameRedirect>();

        // We are not at the main menu since the redirecter is not present
        if (redirecter == null)
        {
            SteamGameRedirect.TargetPhotonServer = photonServerName;

            if (PhotonNetwork.inRoom)
                PhotonNetwork.LeaveRoom();
        }

        // Steam game redirecter is present, so we are at the main menu
        else
            redirecter.JoinPhotonServer(photonServerName);
    }

    // Update is called once per frame
    void CreateSteamLobby()
    {
        if (!SteamManager.Initialized)
            return;

        // Make sure we're not already in a lobby
        LeaveSteamLobby();

        if (!PhotonNetwork.inRoom)
            return;

        // Create a new Steam lobby
        SteamAPICall_t handle = SteamMatchmaking.CreateLobby(ELobbyType.k_ELobbyTypeFriendsOnly, PhotonNetwork.room.MaxPlayers);
        m_steamLobbyCreated.Set(handle);
    }

    void LeaveSteamLobby()
    {
        if (SteamManager.Initialized && m_lobbyId.IsValid())
            SteamMatchmaking.LeaveLobby(m_lobbyId);

        m_lobbyId = CSteamID.Nil;
    }

    void OnJoinedRoom()
    {
        if (SteamManager.Initialized && PhotonNetwork.inRoom)
        {
            SteamFriends.ClearRichPresence();

            // Format example
            // "Playing on a public server (Europe)"
            string statusString = "Playing on a " + (PhotonNetwork.room.IsVisible ? "public" : "private") + " server";
            statusString += " (" + Utility.PhotonRegionToString(PhotonConnectionControl.instance.GetCurrentCloudRegion()) + ")";

            SteamFriends.SetRichPresence("status", statusString);

            if (PhotonNetwork.room.IsVisible)
            {
                string cmdConnectString = "+connect_photon ";

                string formatedServerName = PhotonNetwork.room.Name.Replace(@"\", @"\\"); // Replace \ to \\
                formatedServerName = formatedServerName.Replace("\"", "\\\""); // Replace " to \"
                cmdConnectString += "\"" + formatedServerName + "\""; // Add surrounding quotes
                
                // The command line option is as follows
                // +connect_photon "photonServerName"
                SteamFriends.SetRichPresence("connect", cmdConnectString);
            }
        }
    }
    void OnLeftRoom()
    {
        LeaveSteamLobby();

        if (SteamManager.Initialized)
            SteamFriends.ClearRichPresence();

        Utility.InitializePhotonPlayerProps(PhotonNetwork.player);
    }

    private void OnSteamRequestJoinLobby(GameLobbyJoinRequested_t pCallback)
    {
        m_requestingToJoin = true;

        // Begin by requesting the meta-data (Photon Server name) of the lobby we want to join
        SteamMatchmaking.RequestLobbyData(pCallback.m_steamIDLobby);
    }
    private void OnSteamRichPresenceRequestJoinLobby(GameRichPresenceJoinRequested_t pCallback)
    {
        // Strip the connect argument
        string unformatedString = pCallback.m_rgchConnect.Replace("+connect_photon ", string.Empty);

        // Remove the surrounding quotes 
        unformatedString = unformatedString.Substring(1, unformatedString.Length - 2);

        unformatedString = unformatedString.Replace("\\\"", "\""); // Replace \" to "
        unformatedString = unformatedString.Replace(@"\\", @"\"); // Replace \\ to \
        JoinPhotonServer(unformatedString);
    }
    private void OnSteamLobbyDataUpdate(LobbyDataUpdate_t pCallback)
    {
        if (pCallback.m_bSuccess != 0 && m_requestingToJoin)
        {
            m_requestingToJoin = false;

            string photonServerName = SteamMatchmaking.GetLobbyData(new CSteamID(pCallback.m_ulSteamIDLobby), ConstantVars.SteamLobby_PUNServer_Key);
            JoinPhotonServer(photonServerName);
        }

        // Steam lobby doesn't exist, so we make sure to reset request status as well
        else if (m_requestingToJoin)
            m_requestingToJoin = false;
    }
    private void OnCreatedSteamLobby(LobbyCreated_t pCallback, bool bIOFailure)
    {
        if (pCallback.m_eResult != EResult.k_EResultOK || bIOFailure)
        {
            MessagePopupControl.instance.ShowPopup("An error occurred while creating syncing with Steam. Please try again.", "Steam Error");
            m_lobbyId = CSteamID.Nil;
        }
        else
        {
            m_lobbyId = new CSteamID(pCallback.m_ulSteamIDLobby);

            if (PhotonNetwork.inRoom)
            {
                // Store name of Photon (PUN) server in Steam lobby data
                SteamMatchmaking.SetLobbyData(m_lobbyId, ConstantVars.SteamLobby_PUNServer_Key, PhotonNetwork.room.Name);

                // Immediately bring up Invite dialog when lobby is created
                SteamFriends.ActivateGameOverlayInviteDialog(m_lobbyId);
            }
            else
                LeaveSteamLobby();
        }

    }


}
