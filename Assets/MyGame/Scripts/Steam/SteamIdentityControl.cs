﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Steamworks;

public class SteamIdentityControl : MonoBehaviour {

    public static SteamIdentityControl instance;

    public float VerificationInterval = 10f;

    protected Callback<ValidateAuthTicketResponse_t> m_steamValidateAuthTicketResponse;

    class RemoteSteamClient
    {
        public CSteamID verifiedSteamId = CSteamID.Nil;
        public CSteamID allegedSteamId = CSteamID.Nil;
    }
    class TicketData
    {
        public HAuthTicket authTicket;
        public float sendTimeStamp;
    }

    // Mapping Photon player ID's to their Steam Id
    Dictionary<int, RemoteSteamClient> m_remoteClients = new Dictionary<int, RemoteSteamClient>();

    // Keeping track of the tickets we've sent
    Dictionary<int, TicketData> m_sentTickets = new Dictionary<int, TicketData>();

    // Use this for initialization
    void Awake ()
    {
        instance = this;

        DontDestroyOnLoad(gameObject);
        PhotonNetwork.OnEventCall += OnPhotonEvent;
	}

    private void Start()
    {
        StartCoroutine(CR_VerifyIdentities());
    }

    private void OnEnable()
    {
        if (SteamManager.Initialized)
            m_steamValidateAuthTicketResponse = Callback<ValidateAuthTicketResponse_t>.Create(OnSteamValidateAuthTicketResponse);
    }

    IEnumerator CR_VerifyIdentities()
    {
        // Continuously check if we have a verified Steam Id for each Photon
        // Player. Otherwise request their auth ticket.
        while(true)
        {
            yield return new WaitForSeconds(VerificationInterval);

            if (!PhotonNetwork.inRoom)
                continue;

            foreach(PhotonPlayer player in PhotonNetwork.otherPlayers)
            {
                if(!m_remoteClients.ContainsKey(player.ID))
                {
                    RaiseEventOptions opt = RaiseEventOptions.Default;
                    opt.Receivers = ReceiverGroup.Others;
                    opt.TargetActors = new int[1] { player.ID };
                    opt.Encrypt = false;
                    PhotonNetwork.RaiseEvent(ConstantVars.PhotonEventCode_RequestAuthTicket, null, true, opt);
                }
            }
        }
    }

    /// <summary>
    /// Retrieves the Steam Id associated with a Photon Player. If the Steam Id
    /// of the Photon Player has not been verified it will return an invalid Steam Id.
    /// </summary>
    /// <param name="player"></param>
    /// <returns>Steam Id of the player, CSteamID.Nil the player's Steam Id has not been verified</returns>
    public CSteamID GetPhotonPlayerSteamId(PhotonPlayer player)
    {
        // Use backup ID if we don't have Steam access
        if (!SteamManager.Initialized)
        {
            string strId = Utility.GetPhotonPlayerProp<string>(player, ConstantVars.PlayerSteamIDBackup_Key, "0");
            try
            {
                ulong id = System.Convert.ToUInt64(strId);
                return new CSteamID(id);
            }
            catch { };

            return CSteamID.Nil;
        }

        // If we want the Steam Id for our own player, just return our own Steam Id
        if (player.IsLocal)
            return SteamUser.GetSteamID();

        RemoteSteamClient remoteClient;

        if (m_remoteClients.TryGetValue(player.ID, out remoteClient))
        {
            if (remoteClient.verifiedSteamId.IsValid())
                return remoteClient.verifiedSteamId;
            else
                return CSteamID.Nil;
        }

        return CSteamID.Nil;
    }
    public PhotonPlayer GetPhotonPlayerFromSteamID(CSteamID steamId)
    {
        if (!SteamManager.Initialized)
            return null;

        // Return local player if the Steam Id is ours
        if (steamId == SteamUser.GetSteamID())
            return PhotonNetwork.player;

        foreach(KeyValuePair<int, RemoteSteamClient> pair in m_remoteClients)
        {
            if(pair.Value.verifiedSteamId ==  steamId)
                return PhotonPlayer.Find(pair.Key);
        }

        return null;
    }

    void SendTicket(PhotonPlayer target)
    {
        if (!SteamManager.Initialized || !PhotonNetwork.inRoom || target == null)
            return;

        if (m_sentTickets.ContainsKey(target.ID))
        {
            // We only allow one ticket per verification interval to be sent
            if (Time.time - m_sentTickets[target.ID].sendTimeStamp <= VerificationInterval)
                return;

            // Cancel any ticket we've previously sent to this player
            CancelTicket(target);
        }

        byte[] authTicket = new byte[1024];
        uint ticketLength = 0;

        // Store the auth ticket handle so we can cancel it later
        // Also keep a timestamp to avoid sending tickets too often
        TicketData ticket = new TicketData();
        ticket.authTicket = SteamUser.GetAuthSessionTicket(authTicket, 1024, out ticketLength);
        ticket.sendTimeStamp = Time.time;
        m_sentTickets[target.ID] = ticket;

        // Package the ticket data and the Steam Id we want to be verified as
        object[] data = new object[3];
        data[0] = SteamUser.GetSteamID().m_SteamID.ToString();
        data[1] = authTicket;
        data[2] = (int)ticketLength;

        RaiseEventOptions opt = RaiseEventOptions.Default;
        opt.Receivers = ReceiverGroup.Others;
        opt.TargetActors = new int[1] { target.ID };
        opt.Encrypt = true;
        PhotonNetwork.RaiseEvent(ConstantVars.PhotonEventCode_SendAuthTicket, data, true, opt);
    }

    // Cancel the auth ticket (if any) we sent to the specified player
    void CancelTicket(int photonPlayerId)
    {
        if (photonPlayerId < 0 || !SteamManager.Initialized)
            return;

        TicketData ticket;

        if (m_sentTickets.TryGetValue(photonPlayerId, out ticket))
        {
            SteamUser.CancelAuthTicket(ticket.authTicket);
            m_sentTickets.Remove(photonPlayerId);
        }
    }
    void CancelTicket(PhotonPlayer photonPlayer)
    {
        if (photonPlayer == null)
            return;

        CancelTicket(photonPlayer.ID);
    }

    void EndAuthSession(int photonPlayerId)
    {
        if (photonPlayerId < 0 || !SteamManager.Initialized)
            return;

        RemoteSteamClient remoteClient;

        if (m_remoteClients.TryGetValue(photonPlayerId, out remoteClient))
        {
            if (remoteClient.allegedSteamId.IsValid())
                SteamUser.EndAuthSession(remoteClient.allegedSteamId);
            m_remoteClients.Remove(photonPlayerId);
        }
    }
    void EndAuthSession(PhotonPlayer photonPlayer)
    {
        if (photonPlayer == null)
            return;

        EndAuthSession(photonPlayer.ID);
    }

    void OnSteamValidateAuthTicketResponse(ValidateAuthTicketResponse_t pCallback)
    {
        PhotonPlayer correspondingPhotonPlayer = null;
        RemoteSteamClient remoteClient = null;
        foreach (KeyValuePair<int, RemoteSteamClient> pair in m_remoteClients)
        {
            // We use "allegedSteamId" here so we also find PhotonPlayer's that are
            // only partially verified (passed BeginAuthSession)
            if (pair.Value.allegedSteamId == pCallback.m_SteamID)
            {
                remoteClient = pair.Value;
                correspondingPhotonPlayer = PhotonPlayer.Find(pair.Key);
                break;
            }
        }

        // If authentication fail we end the auth session
        if (pCallback.m_eAuthSessionResponse != EAuthSessionResponse.k_EAuthSessionResponseOK)
            EndAuthSession(correspondingPhotonPlayer);

        // Authentication completed successfully
        else
        {
            if (correspondingPhotonPlayer == null || remoteClient == null)
                return;

            // The Steam Id of this PhotonPlayer is now verified
            remoteClient.verifiedSteamId = pCallback.m_SteamID;

            // The Master Client (host) lets everyone know if a player with special tags has been
            // identified by sending out a chat message
            string tags = Utility.GetPlayerTags(correspondingPhotonPlayer);

            if (GameUIControl.instance != null && tags.Length > 0 && PhotonNetwork.isMasterClient)
                GameUIControl.instance.Chat.SendChatMessage(
                    Utility.ParseChatPlayerNameNoTags(correspondingPhotonPlayer) + " has been verified as " + tags, ChatMenuInput.ChatMessageTypes.HostAnnouncement);
        }

        // We trigger a game event to notify that the Steam Id of a
        // PhotonPlayer might have updated.
        if (GameEvents.instance != null)
            GameEvents.instance.Trigger("OnSteamIdentityChanged");
    }

    void OnLeftRoom()
    {
        if (!SteamManager.Initialized)
            return;
        
        // End our session with all the clients we verified
        List<int> remoteClientIds = new List<int>(m_remoteClients.Keys);
        foreach (int playerId in remoteClientIds)
            EndAuthSession(playerId);

        // Cancel all tickets we've sent
        List<int> sentTicketIds = new List<int>(m_sentTickets.Keys);
        foreach (int playerID in sentTicketIds)
            CancelTicket(playerID);
    }
    void OnJoinedRoom()
    {
        // Send auth tickets to all players in the game to verify them
        foreach(PhotonPlayer player in PhotonNetwork.otherPlayers)
            SendTicket(player);
    }

    void OnPhotonPlayerConnected(PhotonPlayer newPlayer)
    {
        // We immediately send a ticket to any new player to let them know of our identity
        SendTicket(newPlayer);
    }

    void OnPhotonPlayerDisconnected(PhotonPlayer otherPlayer)
    {
        // When a player disconnects, cancel the ticket we sent to them
        CancelTicket(otherPlayer);

        // Also end the auth session we have with the player
        EndAuthSession(otherPlayer);
    }

    private void OnPhotonEvent(byte eventcode, object content, int senderid)
    {
        if (eventcode == ConstantVars.PhotonEventCode_SendAuthTicket && SteamManager.Initialized)
        {
            object[] data = (object[])content;

            try
            {
                CSteamID allegedSenderSteamId = new CSteamID(System.Convert.ToUInt64((string)data[0]));
                byte[] authTicket = (byte[])data[1];
                int ticketSize = (int)data[2];

                // A person claiming to be us, ignore.
                if(allegedSenderSteamId == SteamUser.GetSteamID())
                    return;
                
                // If we already have a verification record for this client we end it
                if (m_remoteClients.ContainsKey(senderid))
                    EndAuthSession(senderid);

                // Attempt to verify the ticket
                EBeginAuthSessionResult result = SteamUser.BeginAuthSession(authTicket, ticketSize, allegedSenderSteamId);

                if (result == EBeginAuthSessionResult.k_EBeginAuthSessionResultOK)
                {

                    // If the ticket passes preliminary inspection we store a preliminary
                    // association between the sending PhotonPlayer and the alleged Steam Id
                    RemoteSteamClient remoteClient = new RemoteSteamClient();
                    remoteClient.allegedSteamId = allegedSenderSteamId;

                    m_remoteClients[senderid] = remoteClient;
                }
            }
            catch { };
        }

        // A remote client requests us to send our ticket so they can verify us
        else if(eventcode == ConstantVars.PhotonEventCode_RequestAuthTicket)
            SendTicket(PhotonPlayer.Find(senderid));
    }
}
