﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class ForceIntoDance : MonoBehaviour {

    Animator m_anim;

	// Use this for initialization
	void Start () {
        m_anim = GetComponent<Animator>();
        m_anim.SetInteger("WeaponType_int", 0);
        m_anim.SetInteger("Animation_int", 4);
        m_anim.SetFloat("Speed_f", 0f);
	}

}
