﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class IsItVisible : MonoBehaviour {

    Renderer m_renderer;

	// Use this for initialization
	void Start () {
        m_renderer = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(m_renderer.isVisible);
	}
}
